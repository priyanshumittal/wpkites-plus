=== WPKites Plus ===

Contributors: spicethemes
Requires at least: 4.5
Tested up to: 6.7.1
Stable tag: 1.5.3
Requires PHP: 5.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enhance WPKites WordPress Themes functionality.

== Description ==

WPKites WordPress Theme is lightweight, elegant, fully responsive and translation-ready theme that allows you to create stunning blogs and websites. The theme is well suited for companies, law firms,ecommerce, finance, agency, travel, photography, recipes, design, arts, personal and any other creative websites and blogs. Theme is developed using Bootstrap 4 framework. It comes with a predesigned home page, good looking header designs and number of content sections that you can easily customize. It also has lots of customization options (banner, services, testimonial, etc) that will help you create a beautiful, unique website in no time. WPKites is  compatible with popular plugins like WPML, Polylang, Woo-commerce  and Conact Form 7. WPKites theme comes with various popular locales.(DEMO: https://wpkites-pro.spicethemes.com) 


== Changelog ==

@Version 1.5.3
* Updated freemius directory.

@Version 1.5.2
* Updated font-awesome library and freemius directory.

@Version 1.5.1
* Added Rank Math, Seo Yoast and NavXT Breadcrumbs Feature.
* Updated the strings.

@Version 1.5
* Updated freemius directory.

@Version 1.4.2
* Added Load Google Font Localy  feature.
* Fixed Portfolio Custom post type translation issue.

@Version 1.4.1
* Fixed the portfolio post meta link issue .

@Version 1.4
* Removed Demo Importer plugin and added this feature into starter sites demo import feature.
* Fixed the post meta issue.

@Version 1.3
* Moved breadcrumb section code in individual pages.
* Fixed Typography & color settings and related post meta issues.
* Updated Breadcrumb section settings.
* Added starter sites demo import feature.

@Version 1.2
* Added Sidebar Layout settings in customizer for blog/archive, single post, and page.
* Added Meta setting for managing Sidebar Layout in individual posts/pages and WooCommerce pages.

@Version 1.1.2
* Added Content Width & Sidebar Width Settings.
* Fixed sticky header issues.

@Version 1.1.1
* Added freemius snippet for dashboard opt-in utility.
* Moved plugin option page and WPKites Plus hooks page in WPKites Plus Panel menu.

@Version 1.1
* Added freemius directory.

@Version 1.0
* Added banner image setting for the shop page. 
* Added One Click Demo Import feature.
* Fixed preloader, portfolio, escaping and some style issues.
* Removed unnecessary code.

@Version 0.2
* Updated some strings and added translatable comments.

@Version 0.1
* Initial Release.

== External resources ==

Owl Carousel: 
Copyright: (c) David Deutsch
License: MIT license
Source: https://cdnjs.com/libraries/OwlCarousel2/2.2.1

Alpha color picker Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT License
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-alpha-color-picker

Repeater Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT license
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-repeater

Custom control - Image Radio Button Custom Control:
Copyright: Anthony Hortin
License: GNU General Public License v2 or later
Source: https://github.com/maddisondesigns/customizer-custom-controls

== Images ==

* Image used in Slider, License CC0 Public Domain
1. https://pxhere.com/en/photo/1044386
2. https://pxhere.com/en/photo/102242
3. https://pxhere.com/en/photo/8873

* Image used in CTA 2, License CC0 Public Domain
https://pxhere.com/en/photo/938790

* Image used in Project, License CC0 Public Domain
1. https://pxhere.com/en/photo/1177788
2. https://pxhere.com/en/photo/1397984
3. https://pxhere.com/en/photo/229
4. https://pxhere.com/en/photo/1372631
5. https://pxhere.com/en/photo/1450143
6. https://pxhere.com/en/photo/1087339
7. https://pxhere.com/en/photo/892699
8. https://pxhere.com/en/photo/910919
9. https://pxhere.com/en/photo/1575121
10. https://pxhere.com/en/photo/1457957
11. https://pxhere.com/en/photo/1576793
12. https://pxhere.com/en/photo/939748
13. https://pxhere.com/en/photo/1559329
14. https://pxhere.com/en/photo/1206989
15. https://pxhere.com/en/photo/136941
16. https://pxhere.com/en/photo/98984
17. https://pxhere.com/en/photo/42491
18. https://pxhere.com/en/photo/892285

* Image used in Funfact, License CC0 Public Domain
https://pxhere.com/en/photo/1548825

* Image used in Testimonial, License CC0 Public Domain
1. https://pxhere.com/en/photo/52971
2. https://pxhere.com/en/photo/1587727
3. https://pxhere.com/en/photo/1432871

* Image used in Team, License CC0 Public Domain
1. https://pxhere.com/en/photo/1421179
2. https://pxhere.com/en/photo/1432871
3. https://pxhere.com/en/photo/642877
4. https://pxhere.com/en/photo/1596737
5. https://pxhere.com/en/photo/99184
6. https://pxhere.com/en/photo/1576793
7. https://pxhere.com/en/photo/816420

* Images on /images folder
Copyright (C) 2025, spicethemes and available as [GPLv2](https://www.gnu.org/licenses/gpl-2.0.html)