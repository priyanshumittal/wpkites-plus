<?php
/**
 * Template Name: Shortcode Page
 *
 * @package wpkites
 */
get_header();

wpkites_breadcrumbs();

?>
    <header class="entry-header">
		<?php the_post();?>
    </header>
        <div class="entry-content" id="content">
		  <?php the_content(); ?>
        </div>  
<?php       
get_footer();?>