<?php
/**
 * Template Name: Full Width Page
 *
 * @package wpkites
 */
get_header();

wpkites_breadcrumbs(); 

?>
<section class="section-space blog page" id="content">
	<div class="container<?php echo esc_html(wpkites_container());?>">
	   	<div class="row">
	     	<div class="col-lg-12 col-md-12 col-sm-12">
            	<?php
             	while ( have_posts()):
            	the_post();
             		include(WPKITESP_PLUGIN_DIR.'/inc/template-parts/content-page.php');
             		if ( comments_open() || get_comments_number() ) :
					comments_template();
					endif;
             	endwhile;
             	?>
			</div>
		</div>
	</div>
</section>
<?php get_footer();?>