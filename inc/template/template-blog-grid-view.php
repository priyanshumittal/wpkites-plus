<?php
/**
 * Template Name: Blog grid view
 */
get_header();

wpkites_breadcrumbs();

$blog_temp_type = get_theme_mod('wpkites_blog_content', 'excerpt');
?>
<section class="page-section-space blog bg-default grid-view <?php
if ($blog_temp_type == 'content') {
    echo 'blog-grid-view-full';
}
?>" id="content">
    <div class="container<?php echo esc_html(wpkites_blog_post_container());?>">
        <?php
        if (get_theme_mod('post_nav_style_setting', 'pagination') == 'pagination') {
            ?>
            <div class="row">
                <?php
                if (get_query_var('paged')) {
                    $paged = get_query_var('paged');
                } elseif (get_query_var('page')) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $args = array('post_type' => 'post', 'paged' => $paged);
                $loop = new WP_Query($args);
                if ($loop->have_posts()):
                    while ($loop->have_posts()): $loop->the_post();
                        ?>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <?php include(WPKITESP_PLUGIN_DIR.'/inc/template-parts/content-blog-template.php'); ?>
                        </div>	
                        <?php
                    endwhile;
                else:
                    get_template_part('template-parts/content','none');
                endif;
                ?>
            </div>
            <?php
            // pagination function
            echo '<div class="center">';
            $obj = new wpkites_plus_pagination();
            $obj->wpkites_plus_page($loop);
            echo '</div>';
        } else {
            echo do_shortcode('[ajax_posts]');
        }
        ?>
    </div>
</section>
<?php get_footer(); ?>