<?php
/**
 * Template Name: Blog switcher
 */
get_header();

wpkites_breadcrumbs();

$blog_temp_type = get_theme_mod('wpkites_blog_content', 'excerpt');
?>
<section class="page-section-space blog blog-switcher bg-default" id="content">
    <div class="container<?php echo esc_html(wpkites_blog_post_container());?>">
        <div class="row">				
            <div class="col-lg-12 col-md-12 col-sm-12">
                <?php
                if (get_theme_mod('post_nav_style_setting', 'pagination') == 'pagination') {
                    if (get_query_var('paged')) {
                        $paged = get_query_var('paged');
                    } elseif (get_query_var('page')) {
                        $paged = get_query_var('page');
                    } else {
                        $paged = 1;
                    }
                    $blog_i=0;
                    $args = array('post_type' => 'post', 'paged' => $paged);
                    $loop = new WP_Query($args);
                    if ($loop->have_posts()):
                        while ($loop->have_posts()): $loop->the_post();
                            include(WPKITESP_PLUGIN_DIR.'/inc/template-parts/ajax-switcher-content.php');
                            $blog_i++;
                        endwhile;
                    else:
                        get_template_part('template-parts/content','none');
                    endif;
                    // pagination function
                    echo '<div class="center">';
                    $obj = new wpkites_plus_pagination();
                    $obj->wpkites_plus_page($loop);
                    echo '</div>';
                } else {
                    echo do_shortcode('[ajax_posts]');
                }
                ?>
            </div>	
        </div>
    </div>
</section>
<?php get_footer(); ?>