<?php
/**
 *  Template Name: Blog Masonry 3 Column
 */
get_header();

wpkites_breadcrumbs();

$exc_length = get_theme_mod('wpkites_blog_content', 'excerpt'); ?>
<section class="page-section-space blog bg-default blog-masonry <?php
if ($exc_length != 'excerpt') {
    echo 'threemasonary-full-width';
}
?>" id="content">
    <div class="container<?php echo esc_html(wpkites_blog_post_container());?>">
        <?php
        if (get_theme_mod('post_nav_style_setting', 'pagination') == "pagination") {
            ?>
            <div class="grid">
                <?php
                if (get_query_var('paged')) {
                    $paged = get_query_var('paged');
                } elseif (get_query_var('page')) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $args = array('post_type' => 'post', 'paged' => $paged);
                $loop = new WP_Query($args);
                if ($loop->have_posts()):
                    while ($loop->have_posts()): $loop->the_post();
                        ?>
                        <div class="grid-item col-md-4">
                            <?php include(WPKITESP_PLUGIN_DIR.'/inc/template-parts/content-blog-template.php'); ?>
                        </div>
                        <?php
                    endwhile;
                else:
                    get_template_part('template-parts/content','none');
                endif;
                ?>
            </div>
            <!-- pagination function -->
           <?php
                echo '<div class="center">';
                $obj = new wpkites_plus_pagination();
                $obj->wpkites_plus_page($loop);
                echo '</div>';
                
        }
        if (get_theme_mod('post_nav_style_setting', 'pagination') != "pagination") {
            echo do_shortcode('[ajax_posts]');
        }
        ?>
    </div>
</section>
<?php get_footer(); ?>