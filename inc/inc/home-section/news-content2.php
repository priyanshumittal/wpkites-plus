<?php
$latest_news_section_enable = get_theme_mod('latest_news_section_enable', true);
if ($latest_news_section_enable != false) {
    ?>
    <!-- Latest News section -->
    <section class="section-space blog bg-default-color">
        <div class="wpkites-newz container">
            <?php
            $home_news_section_title = get_theme_mod('home_news_section_title', __('All News & Articles', 'wpkites-plus'));
            $home_news_section_discription = get_theme_mod('home_news_section_discription', __('Our Blogs', 'wpkites-plus'));
            $home_meta_section_settings = get_theme_mod('home_meta_section_settings', true);
            if (($home_news_section_title) || ($home_news_section_discription) != '') {
                ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="section-header">                            
                            <?php if($home_news_section_title){?><h2 class="section-title"><?php echo $home_news_section_title; ?></h2><?php }
                            if ($home_news_section_discription){?><h5 class="section-subtitle"><?php echo $home_news_section_discription; ?></h5>
                            <?php } ?>
                            <div class="separator"><i class="fa fa-crosshairs"></i></div>
                        </div>
                    </div>                      
                </div>
                <!-- /Section Title -->
            <?php } ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 list-view">
                    <?php
                    $no_of_post = get_theme_mod('wpkites_homeblog_counts', 4);
                    $args = array('post_type' => 'post', 'post__not_in' => get_option("sticky_posts"), 'posts_per_page' => $no_of_post);
                    query_posts($args);
                    if (query_posts($args)) {
                        while (have_posts()):the_post(); {?>
                                <article class="post media">	
                                    <?php if (has_post_thumbnail()) { ?>
                                        <figure class="post-thumbnail">
                                            <?php $defalt_arg = array('class' => "img-fluid"); ?>
                                            <?php the_post_thumbnail('', $defalt_arg); ?>                                            
                                        </figure>	
                                    <?php } ?>
                                    <div class="post-content <?php if(!has_post_thumbnail()){ echo 'remove-images';} else{echo 'media-body';}?>">
                                        <?php
                                        if ($home_meta_section_settings == true) {?> 
                                                <div class="entry-date <?php if(!has_post_thumbnail()){ echo 'remove-image';}?>">
                                                    <a href="<?php echo esc_url(home_url()).'/'.esc_html(date('Y/m', strtotime(get_the_date()))); ?>">
                                                    <?php echo esc_html(get_the_date()); ?>
                                                    </a>
                                                </div>                                      

                                            <div class="entry-meta">
                                                <i class="fa fa-user"></i><span class="author postauthor"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
                                                    <?php echo get_the_author(); ?></a></span>
                                                

                                                <?php
                                                $cat_list = get_the_category_list();
                                                if (!empty($cat_list)) {?>
                                                    <i class="fa fa-folder-open"></i><span class="cat-links postcat"><?php the_category(', '); ?></span>
                                                <?php } 
                                                
                                                $commt = get_comments_number();
                                                if (!empty($commt)) {
                                                    ?>
                                                    <i class="fa fa-comment-o"></i><span class="cat-links"><a href="<?php the_permalink();?>"><?php echo get_comments_number();?></a></span>
                                                <?php } ?>

                                            </div>  
                                        <?php } ?>

                                        <header class="entry-header">
                                            <h4 class="entry-title">
                                                <a class="home-blog-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                            </h4>
                                        </header>	

                                        <div class="entry-content">
                                            <?php the_excerpt(); 
                                            $blog_button = get_theme_mod('home_news_button_title', __('Read More', 'wpkites-plus'));
                                                if (!empty($blog_button)) {?>
                                                <p>
                                                    <a href="<?php the_permalink(); ?>" class="more-link"><?php echo $blog_button; ?>
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </p>
                                            <?php } ?>
                                        </div>	
                                    </div>			
                                </article>
                                <!--</div>-->
                                <?php
                            }
                        endwhile;
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>