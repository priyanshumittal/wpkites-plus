<div class="site-info text-center layout-1">
    <div class="container">
        <?php $foot_section_1 = get_theme_mod('footer_bar_sec1','custom_text');
              switch ( $foot_section_1 )
                { 
                    case 'none':
                    break;

                    case 'footer_menu':
                    echo wpkites_plus_footer_bar_menu();
                    break;

                    case 'custom_text':
                    echo get_theme_mod('footer_copyright','<p class="copyright-section"><span>' . __( 'Proudly powered by','wpkites'). ' ' . '<a href="https://wordpress.org"> WordPress</a>' . ' | ' . __('Theme','wpkites') . ': <a href="https://spicethemes.com/wpkites-wordpress-theme" rel="nofollow"> WPKites </a>' . __('by','wpkites') . ' ' . '<a href="https://spicethemes.com" rel="nofollow">Spicethemes</a></span></p>');
                    break;

                    case 'widget':
                    wpkites_plus_footer_widget_area('footer-bar-1');
                    break;
                }
        ?> 
    </div>
<!--</div>-->