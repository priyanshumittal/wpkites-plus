<?php 
   $shop_button = '<ul class="nav navbar-nav mr-auto">%3$s';
   //Hence This condition only work when woocommerce plugin will be activate
   if ( class_exists( 'WooCommerce' ) ) {
    if((get_theme_mod('header_logo_placing','left')=='six')|| (get_theme_mod('header_logo_placing','left')=='seven'))
    { 
      if(get_theme_mod('after_menu_btn_new_tabl',false)==true) { $wpkites_target="_blank";}
      else { $wpkites_target="_self"; }
      if(!empty(get_theme_mod('after_menu_btn_txt')) && (get_theme_mod('after_menu_multiple_option')=='menu_btn')):
        $shop_button .= '<li class="nav-item wpkites-header-button main-header-btn hw"><div class="btn-box"><a target='.$wpkites_target.' class="theme-btn btn-style-one wpkites_header_btn" href='.get_theme_mod('after_menu_btn_link','#').'><span class="txt">'.get_theme_mod('after_menu_btn_txt').'</span></a></div>';
      endif;
      if(!empty(get_theme_mod('after_menu_html')) && (get_theme_mod('after_menu_multiple_option')=='html')):
        $shop_button .= '<li class="nav-item menu-html menu-item 1">'.get_theme_mod('after_menu_html'); 
      endif;
      if(get_theme_mod('after_menu_multiple_option')=='top_menu_widget'):
          ob_start();
        $sidebar = wpkites_plus_footer_widget_area( 'menu-widget-area' );
        $sidebar = ob_get_contents();
      $shop_button .= '<li class="nav-item">'.$sidebar.'</li>';
      ob_end_clean();
      endif;
      //$shop_button .= '</ul> <div class="header-module">';
    }
    else{
    if(get_theme_mod('after_menu_btn_new_tabl',false)==true) { $wpkites_target="_blank";}
        else { $wpkites_target="_self"; }
        if(!empty(get_theme_mod('after_menu_btn_txt')) && (get_theme_mod('after_menu_multiple_option')=='menu_btn')):
          $shop_button .= '<li class="nav-item main-header-btn "><div class="btn-box"><a target='.$wpkites_target.' class="theme-btn wpkites_header_btn btn-style-one wpkites_header_btn" href='.get_theme_mod('after_menu_btn_link','#').'><span class="txt">'.get_theme_mod('after_menu_btn_txt').'</span></a></div>';
        endif;
        if(!empty(get_theme_mod('after_menu_html')) && (get_theme_mod('after_menu_multiple_option')=='html')):
          $shop_button .= '<li class="nav-item html menu-item 2">'.get_theme_mod('after_menu_html'); 
        endif;
         if(get_theme_mod('after_menu_multiple_option')=='top_menu_widget'):
          //$shop_button .= '<li class="nav-item html menu-item">'.wpkites_footer_widget_area('menu-widget-area'); 
            ob_start();
          $sidebar = wpkites_plus_footer_widget_area( 'menu-widget-area' );
          $sidebar = ob_get_contents();
        $shop_button .= '<li class="nav-item">'.$sidebar.'</li>';
        ob_end_clean();
        endif;
        
   }
   $shop_button .= '<li class="nav-item"> <div class="header-module">';
   if(get_theme_mod('search_effect_style_setting','toogle')=='toogle' && get_theme_mod('search_btn_enable',true)==true)
      {
        $shop_button .='<div class="nav-search nav-light-search wrap">
                        <div class="search-box-outer">
                       <div class="dropdown">
                      <a href="#" title="'.__('Search','wpkites-plus').'" class="search-icon condition has-submenu" aria-haspopup="true" aria-expanded="false">
                   <i class="fa fa-search"></i>
                 <span class="sub-arrow"></span></a>
                  <ul class="dropdown-menu pull-right search-panel"  role="group" aria-hidden="true" aria-expanded="false" >
                             <li class="panel-outer">
                             <div class="form-container">
                                <form role="search" method="get" class="search-form" action="'.esc_url( home_url( '/' )).'">
                                 <label>
                                  <input type="search" class="search-field" placeholder="'.__('Search …','wpkites-plus').'" value="" name="s" autocomplete="off">
                                 </label>
                                 <input type="submit" class="search-submit" value="'.__('Search','wpkites-plus').'">
                                </form>                   
                               </div>
                             </li>
                           </ul>
                         </div>
                       </div>
                     </div>';
          } 
   elseif(get_theme_mod('search_effect_style_setting','popup_light')=='popup_light' && get_theme_mod('search_btn_enable',true)==true || get_theme_mod('search_effect_style_setting','popup_dark')=='popup_dark'  && get_theme_mod('search_btn_enable',true)==true)
          {
            $shop_button .=' <div class="nav-search nav-light-search wrap">
                   <div class="search-box-outer">
                   <div class="dropdown">
                <a href="#searchbar_fullscreen" title="'.__('Search','wpkites-plus').'" class="search-iconaria-haspopup=" true"="" aria-expanded="false">
                  <i class="fa fa-search"></i>
                </a>
                        </div>
                      </div>
                    </div>';
          } 
   if(get_theme_mod('cart_btn_enable',false)==true){
   $shop_button .='<div class="cart-header ">';
      global $woocommerce; 
//      $woo_itm = ($woocommerce->cart->cart_contents_count > 1) ? "" : "";
      $link = function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : $woocommerce->cart->get_cart_url();
      $shop_button .= '<a class="cart-icon" href="'.$link.'" >';
      
      if ($woocommerce->cart->cart_contents_count == 0){
          $shop_button .= '<i class="fa fa-shopping-cart" aria-hidden="true"></i>';
        }
        else
        {
          $shop_button .= '<i class="fa fa-cart-plus" aria-hidden="true"></i>';
        }
           
        $shop_button .= '</a>';
         /* translators: %d: count for number of products in cart */
        $shop_button .= '<a class="cart-total" href="'.$link.'" >'.sprintf(_n('%d item', $woocommerce->cart->cart_contents_count, 'wpkites-plus'), $woocommerce->cart->cart_contents_count) . '</a>';
    }
    }
    //Else condition will run if woocommerce plugin is deactived
    else
    {
    if((get_theme_mod('header_logo_placing','left')=='six')|| (get_theme_mod('header_logo_placing','left')=='seven'))
      {
        $hw='hw';
        $html='menu-html';
      }
      else
      {
        $hw='';
        $html='html';
      }
      if(get_theme_mod('after_menu_btn_new_tabl',false)==true) { $wpkites_target="_blank";}
      else { $wpkites_target="_self"; }
      if(!empty(get_theme_mod('after_menu_btn_txt')) && (get_theme_mod('after_menu_multiple_option')=='menu_btn')):
        $shop_button .= '<li class="nav-item menu-item main-header-btn '.$hw.' 3"><div class="btn-box"><a target='.$wpkites_target.' class="theme-btn btn-style-one wpkites_header_btn" href='.get_theme_mod('after_menu_btn_link','#').'><span class="txt">'.get_theme_mod('after_menu_btn_txt').'</span></a></div>';
      endif;
      if(!empty(get_theme_mod('after_menu_html')) && (get_theme_mod('after_menu_multiple_option')=='html')):
        $shop_button .= '<li class="nav-item '.$html.'  menu-item 3">'.get_theme_mod('after_menu_html'); 
      endif;
       if(get_theme_mod('after_menu_multiple_option')=='top_menu_widget'):
        //$shop_button .= '<li class="nav-item html menu-item">'.wpkites_footer_widget_area('menu-widget-area'); 
          ob_start();
        $sidebar = wpkites_plus_footer_widget_area( 'menu-widget-area' );
        $sidebar = ob_get_contents();
      $shop_button .= '<li class="nav-item">'.$sidebar.'</li>';
      ob_end_clean();
      endif;
      
      if(get_theme_mod('search_effect_style_setting','toogle')=='toogle'  && get_theme_mod('search_btn_enable',true)==true)
      {
   
    $shop_button .= '<li class="nav-item"><div class="header-module">
         <div class="nav-search nav-light-search wrap">
                           <div class="search-box-outer">
                            <div class="dropdown">
                  <a href="#" title="'.__('Search','wpkites-plus').'" class="search-icon condition has-submenu" aria-haspopup="true" aria-expanded="false">
               <i class="fa fa-search"></i>
             <span class="sub-arrow"></span></a>
              <ul class="dropdown-menu pull-right search-panel"  role="group" aria-hidden="true" aria-expanded="false">
                             <li class="panel-outer">
                             <div class="form-container">
                                <form role="search" method="get" class="search-form" action="'.esc_url( home_url( '/' )).'">
                                 <label>
                                  <input type="search" class="search-field" placeholder="'.__('Search …','wpkites-plus').'" value="" name="s" autocomplete="off">
                                 </label>
                                 <input type="submit" class="search-submit" value="'.__('Search','wpkites-plus').'">
                                </form>                   
                               </div>
                             </li>
                           </ul>
                       </div>
                     </div>
                   </div>
        
      </div>';
    }
    elseif(get_theme_mod('search_effect_style_setting','popup_light')=='popup_light' && get_theme_mod('search_btn_enable',true)==true || get_theme_mod('search_effect_style_setting','popup_dark')=='popup_dark' && get_theme_mod('search_btn_enable',true)==true)
          {
            $shop_button .= '<li class="nav-item"><div class="header-module">
                 <div class="nav-search nav-light-search wrap">
                   <div class="search-box-outer">
                   <div class="dropdown">
                <a href="#searchbar_fullscreen" title="'.__('Search','wpkites-plus').'" class="search-iconaria-haspopup=" true"="" aria-expanded="false">
                  <i class="fa fa-search"></i>
                </a>
                        </div>
                      </div>
                    </div>
      </div>';
          }
    }
   $shop_button .= '</ul>';
   
   
   $menu_class='';
    wp_nav_menu( array
             (
             'theme_location'=> 'wpkites-primary', 
             'menu_class'    => 'nav navbar-nav mr-auto '.$menu_class.'',
            'items_wrap'  => $shop_button,
             'fallback_cb'   => 'wpkites_fallback_page_menu',
             'walker'        => new WPKites_Nav_Walker()
             ));        