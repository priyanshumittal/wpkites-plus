jQuery( document ).ready(function($) {
	// Change the width of logo
	wp.customize('wpkites_logo_length', function(control) {
		control.bind(function( controlValue ) {
			$('.custom-logo').css('max-width', '500px');
			$('.custom-logo').css('width', controlValue + 'px');
			$('.custom-logo').css('height', 'auto');
		});
	});

	// Change the border radius
	wp.customize('after_menu_btn_border', function(control) {
		control.bind(function( borderRadius ) {
		$('.wpkites_header_btn').css('border-radius', borderRadius + 'px');
			
		});
	});

	// Change the container width
	wp.customize('container_width_pattern', function(control) {
		control.bind(function( containerWidth ) {
		$('body .container.container_default').css('max-width', containerWidth + 'px');
		});
	});

	// Change content width
	wp.customize('content_width', function(control) {
		
		control.bind(function( contentWidth ) {
			$('body .page-section-space .row .col-lg-8, body .page-section-space .row .col-md-7, body.woocommerce-page .section-space .row .col-lg-8, body.woocommerce-page .section-space .row .col-md-8, body.woocommerce-page .page-section-space .row .col-lg-8, body.woocommerce-page .page-section-space .row .col-md-8').css('max-width', contentWidth + '%');
			$('body .page-section-space .row .col-lg-8, body .page-section-space .row .col-md-7, body.woocommerce-page .page-section-space .row .col-lg-8, body.woocommerce-page .page-section-space .row .col-md-8').css('flex', contentWidth + '%');
		});

	});

	// Change sidebar width
	wp.customize('sidebar_width', function(control) {
		
		control.bind(function( sidebarWidth ) {
			$('body .page-section-space .row .col-lg-4, body .page-section-space .row .col-md-5, body.woocommerce-page .section-space .row .col-lg-4, body.woocommerce-page .section-space .row .col-md-4, body.woocommerce-page .page-section-space .row .col-lg-4, body.woocommerce-page .page-section-space .row .col-md-4').css('max-width', sidebarWidth + '%');
			$('body .page-section-space .row .col-lg-4, body .page-section-space .row .col-md-5, body.woocommerce-page .section-space .row .col-lg-4, body.woocommerce-page .section-space .row .col-md-4, body.woocommerce-page .page-section-space .row .col-lg-4, body.woocommerce-page .page-section-space .row .col-md-4').css('flex', sidebarWidth + '%');
		});

	});
	

	// Change Slider container width
		wp.customize('container_slider_width', function(control) {
		control.bind(function( slideWidth ) {
		$('body .container.slider-caption').css('max-width', slideWidth + 'px');
		});
	});

	//Change CTA 1 Container width
	wp.customize('container_cta1_width', function(control) {
		control.bind(function( cta1Width ) {
		$('body .wpkites-cta1-container').css('max-width', cta1Width + 'px');
		});
	});

	// Change Service container width
	wp.customize('container_service_width', function(control) {
		control.bind(function( servicesWidth ) {
		$('body .wpkites-service-container.container').css('max-width', servicesWidth + 'px');
		});
	});

	// Change Funfact container width
	wp.customize('container_fun_fact_width', function(control) {
		control.bind(function( funWidth ) {
		$('body .wpkites-fun-container.container').css('max-width', funWidth + 'px');
		});
	});

	// Change Portfolio container width
	wp.customize('container_portfolio_width', function(control) {
		control.bind(function( portWidth ) {
		$('body .wpkites-portfolio-container.container').css('max-width', portWidth + 'px');
		});
	});

	//Change Homepage Newz Container width
	wp.customize('container_about_width', function(control) {
		control.bind(function( aboutWidth ) {
		$('body .wpkites-about-container').css('max-width', aboutWidth + 'px');
		});
	});

	// Change Testi container width
	wp.customize('container_testimonial_width', function(control) {
		control.bind(function( testiWidth ) {
		$('body .wpkites-tesi-container.container').css('max-width', testiWidth + 'px');
		});
	});

	//Change Homepage Newz Container width 
	wp.customize('container_home_blog_width', function(control) {
		control.bind(function( newzWidth ) {
		$('body .wpkites-newz.container').css('max-width', newzWidth + 'px');
		});
	});

	//Change Homepage Newz Container width
	wp.customize('container_cta2_width', function(control) {
		control.bind(function( cta2Width ) {
		$('body .wpkites-cta2-container').css('max-width', cta2Width + 'px');
		});
	});

	//Change Team Container width
	wp.customize('container_team_width', function(control) {
		control.bind(function( teamWidth ) {
		$('body .wpkites-team-container.container').css('max-width', teamWidth + 'px');
		});
	});

	//Change shop Container width
	wp.customize('container_shop_width', function(control) {
		control.bind(function( shopWidth ) {
		$('body .wpkites-shop-container.container').css('max-width', shopWidth + 'px');
		});
	});

	//Change client & partners Container width
	wp.customize('container_clients_width', function(control) {
		control.bind(function( clientWidth ) {
		$('body .wpkites-client-container.container').css('max-width', clientWidth + 'px');
		});
	});

});