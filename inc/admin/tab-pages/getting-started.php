<?php
/**
 * Getting started template
 */
$wpkites_plus_name = wp_get_theme();?>
<div id="getting_started" class="wpkites-plus-tab-pane active">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1 class="wpkites-plus-info-title text-center"><?php 
				/* translators: %s: theme name */
				printf( esc_html__('%s Theme Configuration','wpkites-plus'), $wpkites_plus_name );?>
				</h1>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-12">			
			    <div class="wpkites-plus-page">
			    	<div class="mockup">
			    		<img src="<?php echo esc_url(WPKITESP_PLUGIN_URL).'/inc/admin/assets/img/mockup-pro.png';?>"  width="100%">
			    	</div>				
				</div>	
			</div>	

		</div>

		<div class="row" style="margin-top: 20px;">			

			<div class="col-md-6">
				<div class="wpkites-plus-page">
					<div class="wpkites-plus-page-top"><?php esc_html_e( 'Links to Customizer Settings', 'wpkites-plus' ); ?></div>
					<div class="wpkites-plus-page-content">
						<ul class="wpkites-plus-page-list-flex">
							<li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=title_tagline' ) ); ?>" target="_blank"><?php esc_html_e('Site Logo','wpkites-plus'); ?></a>
							</li>
							<li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wpkites_theme_panel' ) ); ?>" target="_blank"><?php esc_html_e('Blog Options','wpkites-plus'); ?></a>
							</li>
							 <li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=nav_menus' ) ); ?>" target="_blank"><?php esc_html_e('Menus','wpkites-plus'); ?></a>
							</li>
							 <li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=theme_style' ) ); ?>" target="_blank"><?php esc_html_e('Layout & Color Scheme','wpkites-plus'); ?></a>
							</li>
							 <li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=widgets' ) ); ?>" target="_blank"><?php esc_html_e('Widgets','wpkites-plus'); ?></a>
							</li>
							 <li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wpkites_general_settings' ) ); ?>" target="_blank"><?php esc_html_e('General Settings','wpkites-plus'); ?></a><?php esc_html_e(' ( Preloader, After Menu, Header Presets, Sticky Header, Container settings, Post Navigation Styles, Scroll to Top settings ) ','wpkites-plus' ); ?>
							</li>
                            <li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=section_settings' ) ); ?>" target="_blank"><?php esc_html_e('Homepage Sections','wpkites-plus'); ?></a>
							</li>
                            <li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wpkites_template_settings' ) ); ?>" target="_blank"><?php esc_html_e('Page Template Settings','wpkites-plus'); ?></a>
							</li>
			
							<li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wpkites_typography_setting' ) ); ?>" target="_blank"><?php esc_html_e('Typography','wpkites-plus'); ?></a>
							</li>
							<li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'themes.php?page=WPKites_Plus_Hooks_Settings' ) ); ?>" target="_blank"><?php esc_html_e('Hooks','wpkites-plus'); ?></a>
							</li>
							
							<li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=frontpage_layout' ) ); ?>" target="_blank"><?php esc_html_e('Sections Order Manager','wpkites-plus'); ?></a>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-md-6"> 
				<div class="wpkites-plus-page">
					<div class="wpkites-plus-page-top"><?php esc_html_e( 'Useful Links', 'wpkites-plus' ); ?></div>
					<div class="wpkites-plus-page-content">
						<ul class="wpkites-plus-page-list-flex">
							<li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url('https://wpkites-pro.spicethemes.com/'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf( esc_html__('%s Plus Demo','wpkites-plus'), $wpkites_plus_name );?>
								</a>
							</li>

							<li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wpkites-plus'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf( esc_html__('%s Plus Details','wpkites-plus'), $wpkites_plus_name );?>
								</a>
							</li>
							
							<li class="">
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url('https://support.spicethemes.com/index.php?p=/categories/wpkites-pro'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf( esc_html__('%s Plus Support','wpkites-plus'), $wpkites_plus_name );?>
								</a>
							</li>
							
						    <li class=""> 
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url('https://wordpress.org/support/theme/wpkites/reviews/#new-post'); ?>" target="_blank"><?php echo esc_html__('Your feedback is valuable to us','wpkites-plus'); ?></a>
							</li>
							
							<li class=""> 
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url('https://helpdoc.spicethemes.com/category/wpkites-plus/'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf( esc_html__('%s Plus Documentation','wpkites-plus'), $wpkites_plus_name );?>
								</a>
							</li>
							 
							<li> 
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wpkites-free-vs-plus/'); ?>" target="_blank"><?php echo esc_html__('Free vs Plus','wpkites-plus'); ?></a>
							</li> 

							<li> 
								<a class="wpkites-plus-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wpkites-plus-changelog/'); ?>" target="_blank"><?php echo esc_html__('Changelog','wpkites-plus'); ?></a>
							</li> 
						</ul>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>	