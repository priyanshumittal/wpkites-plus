<?php
/**
 * shortcode
 */
$customizer_url = admin_url() . 'customize.php' ;
?>

<div id="shortcode_articles" class="wpkites-plus-tab-pane active" style="display: none;">
	<div class="container-fluid">			
		<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
			<h3 class="wpkites-plus-info-title text-center">Shortcodes</h3>
			<div class="col-md-4" style="padding-left: 50px;">

				<h4><?php echo esc_html('CTA 1 Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_cta1]'; ?></b></h4><br>
				
				<h4><?php echo esc_html('Service Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_service]'; ?></b></h4><br>

				<h4><?php echo esc_html('Portfolio Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_portfolio]'; ?></b></h4><br>

				<h4><?php echo esc_html('CTA 2 Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_cta2]'; ?></b></h4><br>			
				
				<h4><?php echo esc_html('About Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_about]'; ?></b></h4><br>			
				
				<h4><?php echo esc_html('Team Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_team]'; ?></b></h4><br>

				<h4><?php echo esc_html('Funfact Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_funfact]'; ?></b></h4><br>

				<h4><?php echo esc_html('Testimonial Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_testimonial]'; ?></b></h4><br>

				<h4><?php echo esc_html('Client Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_client]'; ?></b></h4><br>

				<h4><?php echo esc_html('Contact Section Shortcode','wpkites-plus'); ?></h4>
				<h4><b><?php echo '[wpkites_contact]'; ?></b></h4><br>		
			
			</div>
			<div class="col-lg-8">
				<img src="<?php echo esc_url(WPKITESP_PLUGIN_URL).'/inc/admin/assets/img/shortcode-image.png';?>"  width="100%">
			</div>
		</div>
		<div class="row" style="margin: 20px;">
			<div style="background-color: #d4edda; padding:10px;">
					<h4><a href="https://helpdoc.spicethemes.com/wpkites-plus/how-to-add-shortcodes-in-wpkites-plus/" target="_blank"><?php echo esc_html__('Click Here','wpkites-plus');?></a> <?php echo esc_html('To know about the attributes with the shortcodes.', 'wpkites-plus'); ?> </h4>
			</div>
		</div>
	</div>
</div>	