<?php
// Register and load the widget
function wpkites_header_topbar_info_widget() {
    register_widget('wpkites_header_topbar_info_widget');
}

add_action('widgets_init', 'wpkites_header_topbar_info_widget');

// Creating the widget
class wpkites_header_topbar_info_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'wpkites_header_topbar_info_widget', // Base ID
                esc_html__('WPKites: Header info widget', 'wpkites-plus' ), // Widget Name
                array(
                    'classname' => 'wpkites_header_topbar_info_widget',
                    'phone_number' => esc_html__('Topbar header info widget.', 'wpkites-plus' ),
                ),
                array(
                    'width' => 600,
                )
        );
    }

    public function widget($args, $instance) {

        //echo $args['before_widget']; 
        echo $args['before_widget'];
        ?>
        <ul class="head-contact-info">
            <li class="phone">
                <?php if (!empty($instance['wpkites_phone_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['wpkites_phone_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa fa-phone"></i>
                <?php } ?>	
                <?php
                if (!empty($instance['phone_number'])) {
                    echo esc_html($instance['phone_number']);
                } else {
                    echo esc_html($instance['phone_number']);
                }
                ?>
            </li>
            <li class="envelope">
                <?php if (!empty($instance['wpkites_email_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['wpkites_email_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa fa-envelope"></i>
                <?php } ?>	
                <a href="mailto:abc@example.com"> <?php
                    if (!empty($instance['wpkites_email_id'])) {
                        echo esc_html($instance['wpkites_email_id']);
                    }
                    ?></a>
            </li>
        </ul>
        <?php
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance) {

        if (isset($instance['wpkites_phone_icon'])) {
            $wpkites_phone_icon = $instance['wpkites_phone_icon'];
        } else {
            $wpkites_phone_icon = 'fa-solid fa-phone';
        }

        if (isset($instance['phone_number'])) {
            $phone_number = $instance['phone_number'];
        } else {
            $phone_number = '+99 999-999-9999';
        }

        if (isset($instance['wpkites_email_icon'])) {
            $wpkites_email_icon = $instance['wpkites_email_icon'];
        } else {
            $wpkites_email_icon = 'fa-solid fa-envelope';
        }

        if (isset($instance['wpkites_email_id'])) {
            $wpkites_email_id = $instance['wpkites_email_id'];
        } else {
            $wpkites_email_id = 'abc@example.com';
        }

        // Widget admin form
        ?>

        <label for="<?php echo esc_attr($this->get_field_id('wpkites_phone_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'wpkites-plus' ); echo ''; ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('wpkites_phone_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('wpkites_phone_icon')); ?>" type="text" value="<?php
        if ($wpkites_phone_icon)
            echo esc_attr($wpkites_phone_icon);
        else
            echo esc_attr('fa-solid fa-phone', 'wpkites-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'wpkites-plus' ); echo ' ';  ?><a href="<?php echo esc_url('http://fortawesome.github.io/Font-Awesome/icons/'); ?>" target="_blank" ><?php echo 'fa-icon'; ?></a></span>
        <br><br>

        <label for="<?php echo esc_attr($this->get_field_id('phone_number')); ?>"><?php esc_html_e('Phone', 'wpkites-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('phone_number')); ?>" name="<?php echo esc_attr($this->get_field_name('phone_number')); ?>" type="text" value="<?php
        if ($phone_number)
            echo esc_attr($phone_number);
        else
            echo '+99 999-999-9999';
        ?>" /><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('wpkites_email_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'wpkites-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('wpkites_email_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('wpkites_email_icon')); ?>" type="text" value="<?php
        if ($wpkites_email_icon)
            echo esc_attr($wpkites_email_icon);
        else
            echo 'fa-solid fa-phone';
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'wpkites-plus' ); echo ' '; ?><a href="<?php echo esc_url('http://fortawesome.github.io/Font-Awesome/icons/'); ?>" target="_blank" ><?php echo 'fa-icon'; ?></a></span><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('wpkites_email_id')); ?>"><?php esc_html_e('Email', 'wpkites-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('wpkites_email_id')); ?>" name="<?php echo esc_attr($this->get_field_name('wpkites_email_id')); ?>" type="text" value="<?php
        if ($wpkites_email_id)
            echo esc_attr($wpkites_email_id);
        else
            echo 'abc@example.com';
        ?>" /><br><br>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {

        $instance = array();
        $instance['wpkites_phone_icon'] = (!empty($new_instance['wpkites_phone_icon']) ) ? wpkites_sanitize_text($new_instance['wpkites_phone_icon']) : '';
        $instance['phone_number'] = (!empty($new_instance['phone_number']) ) ? wpkites_sanitize_text($new_instance['phone_number']) : '';
        $instance['wpkites_email_icon'] = (!empty($new_instance['wpkites_email_icon']) ) ? wpkites_sanitize_text($new_instance['wpkites_email_icon']) : '';
        $instance['wpkites_email_id'] = (!empty($new_instance['wpkites_email_id']) ) ? $new_instance['wpkites_email_id'] : '';
       
        return $instance;
    }

}