<?php
/************* Home portfolio custom post type ************************/		
function wpkites_plus_project_type()
		{	register_post_type( 'wpkites_portfolio',
				array(
				'labels' => array(
				'name' => __('Projects', 'wpkites-plus'),
				'add_new' => __('Add New', 'wpkites-plus'),
                'add_new_item' => __('Add New Project','wpkites-plus'),
				'edit_item' => __('Add New','wpkites-plus'),
				'new_item' => __('New Link','wpkites-plus'),
				'all_items' => __('All Projects','wpkites-plus'),
				'view_item' => __('View Link','wpkites-plus'),
				'search_items' => __('Search Links','wpkites-plus'),
				'not_found' =>  __('No Links found','wpkites-plus'),
				'not_found_in_trash' => __('No Links found in Trash','wpkites-plus'), 
				
			),
				'supports' => array('title','thumbnail','editor','excerpt'),
				'show_in_nav_menus' => false,
				'public' => true,
				'rewrite' => array('slug' => 'wpkites_portfolio'),
				'menu_position' => 11,
				'public' => true,
				'menu_icon' => WPKITESP_PLUGIN_URL . '/inc/images/portfolio.png',
			)
	);
}
add_action( 'init', 'wpkites_plus_project_type' );
?>