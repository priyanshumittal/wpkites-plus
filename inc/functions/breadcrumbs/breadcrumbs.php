<?php
// theme sub header breadcrumb functions
if (!function_exists('wpkites_breadcrumbs')):

    function wpkites_breadcrumbs() {
        $classes = get_body_class();
        global $post;
        $breadcrumb='';
        $hide_show_banner = get_theme_mod('banner_enable',true);
        $breadcrumb_overlay=get_theme_mod('breadcrumb_overlay_section_color','rgba(0,0,0,0.6)');
        $wpkites_breadcrumb_type = get_theme_mod('wpkites_breadcrumb_type','yoast');
        if($hide_show_banner == true)
        {
            if (is_404()){?>
                <section class="page-title-section" <?php if ( !empty(get_theme_mod('error_background_img')) ){ ?> style="background-image: url('<?php echo get_theme_mod('error_background_img');?>'); background-color: #17212c;"  <?php } ?>>   
            <?php
            }
            elseif (is_search()){?>
                <section class="page-title-section s" <?php if ( !empty(get_theme_mod('search_background_img')) ){ ?> style="background-image: url('<?php echo get_theme_mod('search_background_img');?>'); background-color: #17212c;"  <?php } ?>>           
                <?php
            }
            elseif (in_array('date',$classes)) {?>
                <section class="page-title-section" <?php if ( !empty(get_theme_mod('date_background_img')) ){ ?> style="background: url('<?php echo get_theme_mod('date_background_img');?>'); background-color: #17212c;"  <?php } ?>>     
            <?php 
            }
            elseif (in_array('author',$classes)) {?>
                <section class="page-title-section" <?php if ( !empty(get_theme_mod('author_background_img')) ){ ?> style="background-image: url('<?php echo get_theme_mod('author_background_img');?>'); background-color: #17212c;"  <?php } ?>>     
            <?php 
            }
            elseif (in_array('woocommerce', $classes)) {?>
                <section class="page-title-section" <?php if( !empty(get_theme_mod('shop_breadcrumb_back_img')) ){ ?> style="background-image: url('<?php echo get_theme_mod('shop_breadcrumb_back_img');?>'); background-color: #17212c;"  <?php } ?>>     
            <?php 
            }
            else{?>
                <section class="page-title-section" <?php if( get_header_image() ){ ?> style="background-image: url('<?php header_image(); ?>'); background-color: #17212c;"  <?php } ?>>      
                <?php
            }?>            
            </style>
               <div class="breadcrumb-overlay" <?php if(get_theme_mod('breadcrumb_image_overlay',true)==true){ echo 'style="background-color:'.$breadcrumb_overlay.'"';}?> >     
                    <div class="container">
                        <div class="row">
                            <?php 
                            $wpkites_breadcrumbs_choice =get_post_meta(get_the_ID(),'wpkites_show_breadcrumb', true );
                            if(get_theme_mod('breadcrumb_position','page_header')=='page_header'  && get_theme_mod('breadcrumb_setting_enable',true)==true || $wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable' ):
                                $breadcrumb_col='col-lg-6 col-md-6 col-sm-12';
                            else:
                                $breadcrumb_col='col-lg-12 col-md-12 col-sm-12';
                            endif;
                            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                            $breadcrumb_enable_disable  = get_theme_mod('breadcrumb_setting_enable',true);
							  if($wpkites_breadcrumbs_choice == ''){
							 if($wpkites_breadcrumb_type == 'yoast') {
                                if($breadcrumb_enable_disable == true ){
                                    if ( function_exists('yoast_breadcrumb') ) {
                                        $wpseo_titles=get_option('wpseo_titles');
                                        if($wpseo_titles['breadcrumbs-enable']==true) {
                                           $breadcrumbs = yoast_breadcrumb("","",false);
                                           $breadcrumb='<ul class="page-breadcrumb">
                                                            <li>'.wp_kses_post($breadcrumbs).'</li>
                                                        </ul>';
                                        }   
                                    }
                                }
                              }
							}
                            elseif($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable') {
							  if($wpkites_breadcrumb_type == 'yoast') {
                                if ( function_exists('yoast_breadcrumb') ) {
                                    $wpseo_titles=get_option('wpseo_titles');
                                    if($wpseo_titles['breadcrumbs-enable']==true){
                                        $breadcrumbs = yoast_breadcrumb("","",false);
                                        $breadcrumb='<ul class="page-breadcrumb">
                                                        <li>'.wp_kses_post($breadcrumbs).'</li>
                                                    </ul>';
                                    }   
                                }
							  }	
                            }
                            if(get_theme_mod('breadcrumb_alignment','parallel')=='parallel') { 
                                if(get_theme_mod('breadcrumb_position','page_header')=='page_header'):
                                    echo '<div class="col-lg-6 col-md-6 col-sm-12 parallel">';
                                    do_action('wpkites_plus_breadcrumbs_page_title_hook');
                                    echo '</div>';
                                endif;
                                if($breadcrumb){echo '<div class="'.$breadcrumb_col.' text-right parallel">'.$breadcrumb.'</div>';}
                                elseif(($wpkites_breadcrumb_type == 'rankmath')&&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) {
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							    echo '<div class="'.$breadcrumb_col.' text-right parallel">'; rank_math_the_breadcrumbs();	echo '</div>';
							    }}
						        elseif(($wpkites_breadcrumb_type == 'navxt')&&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) { 
								if( function_exists( 'bcn_display' ) ){
								echo '<div class="'.$breadcrumb_col.' text-right parallel"><nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav></div>';;
                                }  
                               }								
                            }
                            elseif(get_theme_mod('breadcrumb_alignment','parallel')=='parallelr') {
                                if($breadcrumb){echo '<div class="'.$breadcrumb_col.' text-left parallel">'.$breadcrumb.'</div>';}
							 elseif(($wpkites_breadcrumb_type == 'rankmath')&&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) {  
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							    echo '<div class="'.$breadcrumb_col.' text-left parallel">'; rank_math_the_breadcrumbs();	echo '</div>';
							    }}
						        elseif(($wpkites_breadcrumb_type == 'navxt')&&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) {   if( function_exists( 'bcn_display' ) ){
								echo '<div class="'.$breadcrumb_col.' text-left parallel"><nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav></div>';;
                                }  
                               }	
                                if(get_theme_mod('breadcrumb_position','page_header')=='page_header'):
                                    if((($breadcrumb)||($wpkites_breadcrumb_type == 'rankmath')||($wpkites_breadcrumb_type == 'navxt'))&&($breadcrumb_enable_disable == true )) {
                                        $col='col-lg-6 col-md-6 col-sm-12';
									   if(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_disable'))	
									   {  $col='col-lg-12 col-md-12 col-sm-12'; }
                                    }
                                    else { 
                                        $col='col-lg-12 col-md-12 col-sm-12';
                                    }              
                                    echo '<div class="'.$col.' text-right parallel">';
                                    do_action('wpkites_plus_breadcrumbs_page_title_hook');
                                    echo '</div>';
                                endif;
                            }
                            elseif(get_theme_mod('breadcrumb_alignment','parallel')=='centered') { 
                                echo '<div class="col-lg-12 col-md-12 col-sm-12 text-center">';
                                if(get_theme_mod('breadcrumb_position','page_header')=='page_header'):
                                    do_action('wpkites_plus_breadcrumbs_page_title_hook');
                                endif;
                                if($breadcrumb){echo $breadcrumb;}
								elseif(($wpkites_breadcrumb_type == 'rankmath')&&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) { 
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							        rank_math_the_breadcrumbs();	
							    }}
						        elseif(($wpkites_breadcrumb_type == 'navxt')&&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) {  if( function_exists( 'bcn_display' ) ){
								echo '<nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav></div>';;
                                }  
                               }
                                echo'</div>';
                            }
                            elseif(get_theme_mod('breadcrumb_alignment','parallel')=='left') { 
                                echo '<div class="col-lg-12 col-md-12 col-sm-12 text-left">';
                                if(get_theme_mod('breadcrumb_position','page_header')=='page_header'):                       
                                    do_action('wpkites_plus_breadcrumbs_page_title_hook');
                                endif;
                                if($breadcrumb){echo $breadcrumb;}
								elseif(($wpkites_breadcrumb_type == 'rankmath')&&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) { 
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							        rank_math_the_breadcrumbs();	
							    }}
						        elseif(($wpkites_breadcrumb_type == 'navxt') &&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) {  if( function_exists( 'bcn_display' ) ){
								echo '<nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav></div>';;
                                }  
                               }
                                echo'</div>';
                            }
                            elseif(get_theme_mod('breadcrumb_alignment','parallel')=='right'){ 
                                echo '<div class="col-lg-12 col-md-12 col-sm-12 text-right">';
                                if(get_theme_mod('breadcrumb_position','page_header')=='page_header'):
                                    do_action('wpkites_plus_breadcrumbs_page_title_hook');
                                endif;
                                if($breadcrumb){echo $breadcrumb;}
								elseif(($wpkites_breadcrumb_type == 'rankmath') &&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) { 
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							        rank_math_the_breadcrumbs();	
							    }}
						        elseif(($wpkites_breadcrumb_type == 'navxt')&&(($wpkites_breadcrumbs_choice == 'wpkites_breadcrumbs_enable')||($wpkites_breadcrumbs_choice == ''))&&($breadcrumb_enable_disable == true ) ) {  if( function_exists( 'bcn_display' ) ){
								echo '<nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav></div>';;
                                }  
                               }
                                echo'</div>';
                            }?>
                        </div>
                    </div>
                </div>
            </section>
            <div class="page-seperate"></div>
        <?php } else { ?><div class="page-seperate"></div><?php
        }
    }
endif;
?>