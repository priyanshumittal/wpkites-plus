/**
 *  Exported wpkites_filter_hooks
 *
 */

/**
 * Filter hooks
 */
function wpkites_filter_hooks() {

	var search_val = '';

	if ( typeof jQuery( '#wpkites_search_hooks' ) !== 'undefined' ) {

		if ( typeof jQuery( '#wpkites_search_hooks' ).val() !== 'undefined' ) {

			search_val = jQuery( '#wpkites_search_hooks' ).val().toUpperCase();

			if ( typeof search_val !== 'undefined' ) {

				jQuery( '#WPKites_Plus_Hooks_Settings th' ).each(
					function () {

						if ( jQuery( this ).text().toUpperCase().indexOf( search_val ) > -1 ) {
							jQuery( this ).parent().removeClass( 'hooks-none' );
						} else {
							jQuery( this ).parent().addClass( 'hooks-none' );
						}
					}
				);

			}

		}

	}
}