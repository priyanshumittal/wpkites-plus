<?php

//About Section
$wp_customize->add_section('home_about_page_section', array(
    'title' => esc_html__('About Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
    'priority' => 13,
));

// Enable about section
$wp_customize->add_setting('about_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_plus_sanitize_checkbox',
    ));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'about_section_enable',
                array(
            'label' => esc_html__('Enable/Disable About Section', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'home_about_page_section',
                )
));

//About section content
if ( class_exists( 'WPKites_Plus_Page_Editor' ) ) {
$about_image = WPKITESP_PLUGIN_URL.'/inc/images/about.jpg';
$default = '<div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <figure class="about-thumbnail">    
                    <img src="'.esc_url($about_image).'" alt="'.esc_attr__('About','wpkites-plus').'" class="img-fluid">
                </figure>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
              <div class="about-block">
                <div class="section-header">
                    <p class="section-subtitle">'. esc_html__('Who We Are','wpkites-plus').'</p>
                    <h2 class="about-section-title">'. esc_html__('The promotion that your company needs','wpkites-plus').'</h2>
                </div>

                <div class="entry-content">
                    <p>'. esc_html__('Greater great set seasons was morning creepeth a replenish fisher night to. She to fourth does. cattle also be days given can itself are  you good. It green night male.','wpkites-plus').'</p>
                    <div class="about-area">
                        <div class="media">
                            <div class="about-icon">
                                <i class="fa fa-clipboard"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="title">'. esc_html__('Certified Company','wpkites-plus').'</h4>
                                <p>'. esc_html__('Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate.','wpkites-plus').'</p>
                            </div>
                        </div>
                    </div>
                    <div class="about-area">
                        <div class="media">
                            <div class="about-icon">
                                <i class="fa fa-cog"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="title">'. esc_html__('Experienced Employee','wpkites-plus').'</h4>
                                <p>'. esc_html__('Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate.','wpkites-plus').'</p>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>';

$wp_customize->add_setting(
    'about_section_content', array(
        'default'           => $default,
        'sanitize_callback' => 'wp_kses_post',
        'transport'         => $selective_refresh,
    )
);

$wp_customize->add_control(
    new WPKites_Plus_Page_Editor(
        $wp_customize, 'about_section_content', array(
            'label'    => esc_html__( 'About Content', 'innofit-plus' ),
            'section'  => 'home_about_page_section',
            'priority' => 10,
            'needsync' => true,
            'active_callback' => 'wpkites_plus_abt_callback'
        )
    )
);
}

/**
 * Add selective refresh for Front page section section controls.
 */

$wp_customize->selective_refresh->add_partial( 'about_section_content', array(
        'selector'            => '.about-section .section-subtitle',
        'settings'            => 'about_section_content',
        
) );
?>