<?php

//Client Section

$wp_customize->add_section('home_client_section', array(
    'title' => esc_html__('Clients Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
    'priority' => 23,
));

// Enable client section
$wp_customize->add_setting('client_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_sanitize_checkbox'
    ));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'client_section_enable',
                array(
            'label' => esc_html__('Enable/Disable Client Section', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'home_client_section',
                )
));

// Enable Background Color
$wp_customize->add_setting( 'enable_clt_bg_color', array(
    'default' => false,
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control('enable_clt_bg_color', array(
    'label'    => __('Enable Client Section Background Color', 'wpkites-plus' ),
    'section'  => 'home_client_section',
    'type' => 'checkbox',
) );
    

//Client Background Overlay Color
$wp_customize->add_setting('clt_bg_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => '#000000',
));

$wp_customize->add_control(new WPKites_Plus_Customize_Alpha_Color_Control($wp_customize, 'clt_bg_color', array(
            'label' => esc_html__('Client Section Background Color', 'wpkites-plus'),
            'palette' => true,
            'active_callback' => 'wpkites_plus_sponsors_callback',
            'section' => 'home_client_section')
));

if (class_exists('WPKites_Plus_Repeater')) {
    $wp_customize->add_setting(
            'wpkites_clients_content', array(
            )
    );

    $wp_customize->add_control(
            new WPKites_Plus_Repeater(
                    $wp_customize, 'wpkites_clients_content', array(
                'label' => esc_html__('Client Content', 'wpkites-plus'),
                'section' => 'home_client_section',
                'add_field_label' => esc_html__('Add new client', 'wpkites-plus'),
                'item_name' => esc_html__('Client', 'wpkites-plus'),
                'customizer_repeater_image_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'active_callback' => 'wpkites_plus_sponsors_callback'
                    )
            )
    );
}

$wp_customize->add_setting('client_items',
        array(
            'default' => 5,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_sanitize_number_range',
        )
);
$wp_customize->add_control('client_items',
        array(
            'label' => esc_html__('Number of Clients', 'wpkites-plus'),
            'section' => 'home_client_section',
            'type' => 'number',
            'input_attrs' => array('min' => 1, 'max' => 5, 'step' => 1, 'style' => 'width: 100%;'),
            'active_callback' => 'wpkites_plus_sponsors_callback'
        )
);
//Navigation Type
$wp_customize->add_setting('client_nav_style', array('default' => 'navigation'));
$wp_customize->add_control('client_nav_style', array(
    'label' => __('Navigation Style', 'wpkites-plus'),
    'section' => 'home_client_section',
    'type' => 'radio',
    'choices' => array(
        'bullets' => __('Bullets', 'wpkites-plus'),
        'navigation' => __('Navigation', 'wpkites-plus'),
        'both' => __('Both', 'wpkites-plus'),
    ),
    'active_callback' => 'wpkites_plus_sponsors_callback'
));

// animation speed
$wp_customize->add_setting('client_animation_speed', array('default' => 3000));
$wp_customize->add_control('client_animation_speed',
        array(
            'label' => __('Animation Speed', 'wpkites-plus'),
            'section' => 'home_client_section',
            'type' => 'select',
            'choices' => array(
                2000 => '2.0',
                3000 => '3.0',
                4000 => '4.0',
                5000 => '5.0',
                6000 => '6.0',
            ),
            'active_callback' => 'wpkites_plus_sponsors_callback'
));

// smooth speed
$wp_customize->add_setting('client_smooth_speed', array('default' => 1000));
$wp_customize->add_control('client_smooth_speed',
        array(
            'label' => __('Smooth Speed', 'wpkites-plus'),
            'section' => 'home_client_section',
            'type' => 'select',
            'active_callback' => 'wpkites_plus_sponsors_callback',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0')
));


$wp_customize->selective_refresh->add_partial('clt_bg_color', array(
    'selector' => '.sponsors.bg-default-color-3',
    'settings' => 'clt_bg_color',
    'render_callback' => 'clt_bg_color_render_callback'
));

$wp_customize->selective_refresh->add_partial('wpkites_clients_content', array(
    'selector' => '.sponsors #clients-carousel',
    'settings' => 'wpkites_clients_content',
    'render_callback' => 'wpkites_clients_content_render_callback'
));

function clt_bg_color_render_callback() {
    return get_theme_mod('clt_bg_color');
}

function wpkites_clients_content_render_callback() {
    return get_theme_mod('wpkites_clients_content');
}
?>