<?php

$wp_customize->add_section('services_section', array(
    'title' => __('Services Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
    'priority' => 11,
));


//Service Section

$wp_customize->add_setting('home_service_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_sanitize_checkbox'
));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'home_service_section_enabled',
                array(
            'label' => __('Enable/Disable Services Section', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'services_section',
                )
));

//Service section title
$wp_customize->add_setting('home_service_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Services We Provide', 'wpkites-plus'),
    'sanitize_callback' => 'wpkites_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_service_section_title', array(
    'label' => __('Title', 'wpkites-plus'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_service_callback'
));

// Service section description
$wp_customize->add_setting('home_service_section_discription', array(
    'capability' => 'edit_theme_options',
    'default' => __('Our Best Services', 'wpkites-plus'),
    'transport' => $selective_refresh,
));


$wp_customize->add_control('home_service_section_discription', array(
    'label' => __('Sub Title', 'wpkites-plus'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_service_callback'
));

//Style Design
$wp_customize->add_setting('home_serive_design_layout', array('default' => 1));
$wp_customize->add_control('home_serive_design_layout',
        array(
            'label' => __('Design Style', 'wpkites-plus'),
            'section' => 'services_section',
            'type' => 'select',
            'choices' => array(
                1 => __('Design', 'wpkites-plus').' 1',
                2 => __('Design', 'wpkites-plus').' 2',
                3 => __('Design', 'wpkites-plus').' 3',
                4 => __('Design', 'wpkites-plus'.' 4')
            ),
            'active_callback' => 'wpkites_plus_service_callback'
));

$wp_customize->add_setting('home_service_col',
        array(
            'default' => 3,
            'sanitize_callback' => 'wpkites_plus_sanitize_select'
        )
);

$wp_customize->add_control('home_service_col',
        array(
            'label' => esc_html__('Column Layout', 'wpkites-plus'),
            'section' => 'services_section',
            'type' => 'select',
            'active_callback' => 'wpkites_plus_service_callback',
            'choices' => array(
                1 => '1 ' . esc_html__('Column', 'wpkites-plus'),
                2 => '2 ' . esc_html__('Column', 'wpkites-plus'),
                3 => '3 ' . esc_html__('Column', 'wpkites-plus'),
                4 => '4 ' . esc_html__('Column', 'wpkites-plus'),
            )
        )
);


if (class_exists('WPKites_Plus_Repeater')) {
    $wp_customize->add_setting('wpkites_service_content', array());

    $wp_customize->add_control(new WPKites_Plus_Repeater($wp_customize, 'wpkites_service_content', array(
                'label' => esc_html__('Service Content', 'wpkites-plus'),
                'section' => 'services_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Service', 'wpkites-plus'),
                'item_name' => esc_html__('Service', 'wpkites-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'active_callback' => 'wpkites_plus_service_callback'
    )));
}

$wp_customize->selective_refresh->add_partial('home_service_section_title', array(
    'selector' => '.services .section-title, .services2 .section-title, .services3 .section-title, .services4 .section-title',
    'settings' => 'home_service_section_title',
    'render_callback' => 'wpkites_plus_home_service_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_service_section_discription', array(
    'selector' => '.services .section-subtitle, .services2 .section-subtitle, .services3 .section-subtitle, .services4 .section-subtitle',
    'settings' => 'home_service_section_discription',
    'render_callback' => 'wpkites_plus_home_service_section_discription_render_callback'
));

$wp_customize->selective_refresh->add_partial('service_viewmore_btn_text', array(
    'selector' => '.services .view-more-services',
    'settings' => 'service_viewmore_btn_text',
    'render_callback' => 'wpkites_plus_service_viewmore_btn_text_render_callback'
));

function wpkites_plus_home_service_section_title_render_callback() {
    return get_theme_mod('home_service_section_title');
}

function wpkites_plus_home_service_section_discription_render_callback() {
    return get_theme_mod('home_service_section_discription');
}

function wpkites_plus_service_viewmore_btn_text_render_callback() {
    return get_theme_mod('service_viewmore_btn_text');
}