<?php

// wpkites slider content data
if (!function_exists('wpkites_plus_slider_default_customize_register')) :
    add_action('customize_register', 'wpkites_plus_slider_default_customize_register');

    function wpkites_plus_slider_default_customize_register($wp_customize) {

        //wpkites lite slider data
        if (get_theme_mod('home_slider_subtitle') != '' || get_theme_mod('home_slider_title') != '' || get_theme_mod('home_slider_discription') != '' || get_theme_mod('home_slider_image') != '') {

            $home_slider_subtitle = get_theme_mod('home_slider_subtitle');
            $home_slider_title = get_theme_mod('home_slider_title');
            $home_slider_discription = get_theme_mod('home_slider_discription');
            $home_slider_btn_target = get_theme_mod('home_slider_btn_target');
            $home_slider_btn_txt = get_theme_mod('home_slider_btn_txt');
            $home_slider_btn_link = get_theme_mod('home_slider_btn_link');
            $home_slider_image = get_theme_mod('home_slider_image');


            if ($home_slider_btn_target == 1) {

                $home_slider_btn_target = true;
            }

            $wpkites_plus_slider_content_control = $wp_customize->get_setting('wpkites_plus_slider_content');
            if (!empty($wpkites_plus_slider_content_control)) {
                $wpkites_plus_slider_content_control->default = json_encode(array(
                    array(
                        'title' => !empty($home_slider_title) ? $home_slider_title : __('We Provide Quality Business','wpkites-plus') . '<br>' . __('Solution','wpkites-plus'),
                        'text' => !empty($home_slider_discription) ? $home_slider_discription : __('Welcome to WPKites','wpkites-plus'),
                        'abtsliderbutton_text' => !empty($home_slider_btn_txt) ? $home_slider_btn_txt : __('Learn More','wpkites-plus'),
                        'abtsliderlink' => !empty($home_slider_btn_link) ? $home_slider_btn_link : '#',
                        'image_url' => !empty($home_slider_image) ? $home_slider_image : WPKITESP_PLUGIN_URL . '/inc/images/slider/slide-1.jpg',
                        'abtslider_open_new_tab' => !empty($home_slider_btn_target) ? $home_slider_btn_target : false,
                        'id' => 'customizer_repeater_56d7ea7f40b50',
                    ),
                ));
            }
        } else {
            //wpkites slider data
            $wpkites_plus_slider_content_control = $wp_customize->get_setting('wpkites_plus_slider_content');
            if (!empty($wpkites_plus_slider_content_control)) {
                $wpkites_plus_slider_content_control->default = json_encode(array(
                    array(
                        'title' => __('We provide solutions to','wpkites-plus') . '<br><span>' . __('grow your business', 'wpkites-plus') . '</span>',
                        'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.',
                        'abtsliderbutton_text' => __('Learn More', 'wpkites-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/slider/slide-1.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'wpkites-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b96',
                    ),
                    array(
                        'title' => __('We create stunning') . '<br><span>' . __('WordPress themes','wpkites-plus') . '</span>',
                        'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.',
                        'abtsliderbutton_text' => __('Learn More', 'wpkites-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/slider/slide-2.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'wpkites-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b97',
                    ),
                    array(
                        'title' => __('We provide solutions to','wpkites-plus') . '<br><span>' . __('grow your business', 'wpkites-plus') . '</span>',
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'wpkites-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'wpkites-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/slider/slide-3.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'wpkites-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b98',
                    ),
                ));
            }
        }
    }

endif;

// wpkites default service data
if (!function_exists('wpkites_service_default_customize_register')) :
    function wpkites_service_default_customize_register($wp_customize) {
        $wpkites_service_content_control = $wp_customize->get_setting('wpkites_service_content');
        if (!empty($wpkites_service_content_control)) {
            $wpkites_service_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-headphones',
                    'title' => esc_html__('Unlimited Support', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'icon_value' => 'fa-mobile',
                    'title' => esc_html__('Pixel Perfect Design', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'icon_value' => 'fa-cogs',
                    'title' => esc_html__('Powerful Options', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
                array(
                    'icon_value' => 'fa-desktop',
                    'title' => esc_html__('Powerful Options', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_52d7ea8f40b86',
                ),
                array(
                    'icon_value' => 'fa-headphones',
                    'title' => esc_html__('Powerful Options', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_53d7ea9f40b86',
                ),
                array(
                    'icon_value' => 'fa-desktop',
                    'title' => esc_html__('Powerful Options', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_59d7ea4f40b86',
                ),
            ));
        }
    }
    add_action('customize_register', 'wpkites_service_default_customize_register');
endif;

// wpkites default Contact deatail
if (!function_exists('wpkites_plus_contact_detail_default_customize_register')) :
    function wpkites_plus_contact_detail_default_customize_register($wp_customize) {
        $wpkites_plus_contact_detail_content_control = $wp_customize->get_setting('wpkites_plus_contact_detail_content');
        if (!empty($wpkites_plus_contact_detail_content_control)) {
            $wpkites_plus_contact_detail_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-solid fa-location-dot',
                    'title' => 'Address',
                    'text' => '17504 Carlton Cuevas Rd, Gulfport, MS, 39503',
                    'id' => 'customizer_repeater_56d7ea7f40b60',
                ),
                array(
                    'icon_value' => 'fa-solid fa-mobile-screen',
                    'title' => 'Phone',
                    'text' => '(007) 123 456 7890<br>(007) 444 333 6678',
                    'id' => 'customizer_repeater_56d7ea7f40b61',
                ),
                array(
                    'icon_value' => 'fa-solid fa-envelope',
                    'title' => 'Email',
                    'text' => 'info@honeypress.com <br>support@honeypress.com',
                    'id' => 'customizer_repeater_56d7ea7f40b62',
                ),        
            ));
        }
    }
    add_action('customize_register', 'wpkites_plus_contact_detail_default_customize_register');
endif;

// busicare default Contact data
if (!function_exists('wpkites_plus_social_links_default_customize_register')) :

    function wpkites_plus_social_links_default_customize_register($wp_customize) {

        $wpkites_social_links_control = $wp_customize->get_setting('wpkites_social_links');
        if (!empty($wpkites_social_links_control)) {
            $wpkites_social_links_control->default = json_encode(array(
                        array(
                        'icon_value' => 'fa-brands fa-facebook-f',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b76',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-x-twitter',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b77',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-linkedin',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b78',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-instagram',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
                        array(
                        'icon_value' => 'fa fa-youtube-play',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
        ));
        }
    }

    add_action('customize_register', 'wpkites_plus_social_links_default_customize_register');

endif;

// wpkites default Funfact data
if (!function_exists('wpkites_funfact_default_customize_register')) :

    function wpkites_funfact_default_customize_register($wp_customize) {

        $wpkites_funfact_content_control = $wp_customize->get_setting('wpkites_funfact_content');
        if (!empty($wpkites_funfact_content_control)) {
            $wpkites_funfact_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-pie-chart',
                    'title' => '2531',
                    'text' => esc_html__('Registered Members', 'wpkites-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'icon_value' => 'fa-regular fa-face-smile',
                    'title' => '3564',
                    'text' => esc_html__('Happy Customer', 'wpkites-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'icon_value' => 'fa-shield',
                    'title' => '231',
                    'text' => esc_html__('Win Awards', 'wpkites-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
                array(
                    'icon_value' => 'fa-solid fa-hand-peace',
                    'title' => '1070',
                    'text' => esc_html__('Satisfied Customer', 'wpkites-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b87',
                ),
            ));
        }
    }

    add_action('customize_register', 'wpkites_funfact_default_customize_register');

endif;

// wpkites default Contact data
if (!function_exists('wpkites_contact_default_customize_register')) :

    function wpkites_contact_default_customize_register($wp_customize) {

        $wpkites_plus_contact_content_control = $wp_customize->get_setting('wpkites_plus_contact_content');
        if (!empty($wpkites_plus_contact_content_control)) {
            $wpkites_plus_contact_content_control->default = json_encode(array(
                array(
                'icon_value' => 'fa-solid fa-location-dot',
                'title' => 'Address',
                'text' => '17504 Carlton Cuevas Rd, Gulfport, MS, 39503',
                'id' => 'customizer_repeater_56d7ea7f40b60',
            ),
            array(
                'icon_value' => 'fa-solid fa-mobile-screen',
                'title' => 'Phone',
                'text' => '(007) 123 456 7890<br>(007) 444 333 6678',
                'id' => 'customizer_repeater_56d7ea7f40b61',
            ),
            array(
                'icon_value' => 'fa-solid fa-envelope',
                'title' => 'Email',
                'text' => 'info@example.com <br>support@example.com',
                'id' => 'customizer_repeater_56d7ea7f40b62',
            ),   
            ));
        }
    }

    add_action('customize_register', 'wpkites_contact_default_customize_register');

endif;

// wpkites default Contact data
if (!function_exists('wpkites_social_links_default_customize_register')) :

    function wpkites_social_links_default_customize_register($wp_customize) {

        $wpkites_social_links_control = $wp_customize->get_setting('wpkites_social_links');
        if (!empty($wpkites_social_links_control)) {
            $wpkites_social_links_control->default = json_encode(array(
                array(
                'icon_value' => 'fa-brands fa-facebook-f',
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b76',
                ),
                array(
                'icon_value' => 'fa-brands fa-x-twitter',
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b77',
                ),
                array(
                'icon_value' => 'fa-brands fa-linkedin',
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b78',
                ),
                array(
                'icon_value' => 'fa-brands fa-instagram',
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b80',
                ),
                array(
                'icon_value' => 'fa-brands fa-youtube',
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b80',
                ),
            ));
        }
    }

    add_action('customize_register', 'wpkites_social_links_default_customize_register');

endif;

// WPKites Testimonial content data
if (!function_exists('wpkites_testimonial_default_customize_register')) :
    add_action('customize_register', 'wpkites_testimonial_default_customize_register');

    function wpkites_testimonial_default_customize_register($wp_customize) {

        //wpkites default testimonial data.
        $wpkites_testimonial_content_control = $wp_customize->get_setting('wpkites_testimonial_content');
        if (!empty($wpkites_testimonial_content_control)) {
            $wpkites_testimonial_content_control->default = json_encode(array(
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => __('Amanda Smith', 'wpkites-plus'),
                    'designation' => __('Developer', 'wpkites-plus'),
                    'home_testimonial_star' => '4.5',
                    'link' => '#',
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_77d7ea7f40b96',
                    'home_slider_caption' => 'customizer_repeater_star_4.5',
                ),
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => __('Travis Cullan', 'wpkites-plus'),
                    'designation' => __('Team Leader', 'wpkites-plus'),
                    'home_testimonial_star' => '5',
                    'link' => '#',
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/user/user2.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_88d7ea7f40b97',
                    'home_slider_caption' => 'customizer_repeater_star_5',
                ),
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => __('Victoria Wills', 'wpkites-plus'),
                    'designation' => __('Volunteer', 'wpkites-plus'),
                    'home_testimonial_star' => '3.5',
                    'link' => '#',
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/user/user3.jpg',
                    'id' => 'customizer_repeater_11d7ea7f40b98',
                    'open_new_tab' => 'no',
                    'home_slider_caption' => 'customizer_repeater_star_3.5',
                ),
            ));
        }
    }

endif;


// WPKites Team content data
if (!function_exists('wpkites_team_default_customize_register')) :
    add_action('customize_register', 'wpkites_team_default_customize_register');

    function wpkites_team_default_customize_register($wp_customize) {
        //wpkites default team data.
        $wpkites_team_content_control = $wp_customize->get_setting('wpkites_team_content');
        if (!empty($wpkites_team_content_control)) {
            $wpkites_team_content_control->default = json_encode(array(
                array(
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/team/team-1.jpg',
                    'image_url2' => WPKITESP_PLUGIN_URL . '/inc/images/portfolio/project-20.jpg',
                    'membername' => 'Danial Wilson',
                    'designation' => esc_html__('Senior Manager', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_26d7ea7f40c56',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-37fb908374e06',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-47fb9144530fc',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9750e1e09',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-67fb0150e1e256',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/team/team-2.jpg',
                    'image_url2' => WPKITESP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Amanda Smith',
                    'designation' => esc_html__('Founder & CEO', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d1ea2f40c66',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9133a7772',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9160rt683',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916zzooc9',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916qqwwc784',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/team/team-3.jpg',
                    'image_url2' => WPKITESP_PLUGIN_URL . '/inc/images/portfolio/project-11.jpg',
                    'membername' => 'Victoria Wills',
                    'designation' => esc_html__('Web Master', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c76',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb917e4c69e',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb91830825c',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e8',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/team/team-4.jpg',
                    'image_url2' => WPKITESP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Travis Marcus',
                    'designation' => esc_html__('UI Developer', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c86',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb925cedcb2',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb92615f030',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
            ));
        }
    }

endif;

//Client section
if (!function_exists('wpkites_client_default_customize_register')) :
    add_action('customize_register', 'wpkites_client_default_customize_register');

    function wpkites_client_default_customize_register($wp_customize) {
        //wpkites default client data.
        $wpkites_client_content_control = $wp_customize->get_setting('wpkites_clients_content');
        if (!empty($wpkites_client_content_control)) {
            $wpkites_client_content_control->default = json_encode(array(
                array(
                    'link' => '#',
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/sponsors/client-1.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b96',
                ),
                array(
                    'link' => '#',
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/sponsors/client-2.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b97',
                ),
                array(
                    'link' => '#',
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/sponsors/client-3.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b98',
                ),
                array(
                    'link' => '#',
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/sponsors/client-4.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b99',
                ),
            ));
        }
    }

endif;