<?php

//Team Section
$wp_customize->add_section('wpkites_team_section', array(
    'title' => __('Team Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
    'priority' => 15,
));

$wp_customize->add_setting('team_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_sanitize_checkbox'
));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'team_section_enable',
                array(
            'label' => __('Enable/Disable Team Section', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'wpkites_team_section',
                )
));

// Team section title
$wp_customize->add_setting('home_team_section_title', array(
    'default' => __('Our Team Is Very Expert', 'wpkites-plus'),
    'sanitize_callback' => 'wpkites_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_team_section_title', array(
    'label' => __('Title', 'wpkites-plus'),
    'section' => 'wpkites_team_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_team_callback'
));

//Team section discription
$wp_customize->add_setting('home_team_section_discription', array(
    'default' => __('Our Expert Team Members', 'wpkites-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_team_section_discription', array(
    'label' => __('Sub Title', 'wpkites-plus'),
    'section' => 'wpkites_team_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_team_callback'
));

//Style Design
$wp_customize->add_setting('home_team_design_layout', array('default' => 1));
$wp_customize->add_control('home_team_design_layout',
        array(
            'label' => __('Design Style', 'wpkites-plus'),
            'section' => 'wpkites_team_section',
            'type' => 'select',
            'choices' => array(
                1 => __('Design', 'wpkites-plus').' 1',
                2 => __('Design', 'wpkites-plus').' 2',
                3 => __('Design', 'wpkites-plus').' 3',
                4 => __('Design', 'wpkites-plus').' 4'
            ),
            'active_callback' => 'wpkites_plus_team_callback',
));


if (class_exists('WPKites_Plus_Repeater')) {
    $wp_customize->add_setting(
            'wpkites_team_content', array(
            )
    );

    $wp_customize->add_control(
            new WPKites_Plus_Repeater(
                    $wp_customize, 'wpkites_team_content', array(
                'label' => esc_html__('Team Content', 'wpkites-plus'),
                'section' => 'wpkites_team_section',
                'priority' => 15,
                'add_field_label' => esc_html__('Add new Team Member', 'wpkites-plus'),
                'item_name' => esc_html__('Team Member', 'wpkites-plus'),
                'customizer_repeater_member_name_control' => true,
                //'customizer_repeater_text_control' => true,
                'customizer_repeater_designation_control' => true,
                'customizer_repeater_image_control' => true,
                'customizer_repeater_image_control2' => true,
                //'customizer_repeater_link_control' => true,
                // 'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_repeater_control' => true,
                'active_callback' => 'wpkites_plus_team_callback'
                    )
            )
    );
}

//Navigation Type
$wp_customize->add_setting('team_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('team_nav_style', array(
    'label' => __('Navigation Style', 'wpkites-plus'),
    'section' => 'wpkites_team_section',
    'type' => 'radio',
    'priority' => 17,
    'choices' => array(
        'bullets' => __('Bullets', 'wpkites-plus'),
        'navigation' => __('Navigation', 'wpkites-plus'),
        'both' => __('Both', 'wpkites-plus'),
    ),
    'active_callback' => 'wpkites_plus_team_callback'
));

// animation speed
$wp_customize->add_setting('team_animation_speed', array('default' => 3000));
$wp_customize->add_control('team_animation_speed',
        array(
            'label' => __('Animation Speed', 'wpkites-plus'),
            'section' => 'wpkites_team_section',
            'type' => 'select',
            'priority' => 17,
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'wpkites_plus_team_callback'
));

// smooth speed
$wp_customize->add_setting('team_smooth_speed', array('default' => 1000));
$wp_customize->add_control('team_smooth_speed',
        array(
            'label' => __('Smooth Speed', 'wpkites-plus'),
            'section' => 'wpkites_team_section',
            'type' => 'select',
            'priority' => 17,
            'active_callback' => 'wpkites_plus_team_callback',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0')
));

/**
 * Add selective refresh for Front page team section controls.
 */
$wp_customize->selective_refresh->add_partial('home_team_section_title', array(
    'selector' => '.section-space.team .section-title',
    'settings' => 'home_team_section_title',
    'render_callback' => 'wpkites_plus_home_team_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_team_section_discription', array(
    'selector' => '.section-space.team .section-subtitle',
    'settings' => 'home_team_section_discription',
    'render_callback' => 'wpkites_plus_home_team_section_discription_render_callback',
));

function wpkites_plus_home_team_section_title_render_callback() {
    return get_theme_mod('home_team_section_title');
}

function wpkites_plus_home_team_section_discription_render_callback() {
    return get_theme_mod('home_team_section_discription');
}

?>