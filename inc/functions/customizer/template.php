<?php

/**
 * Template Options Customizer
 *
 * @package wpkites
 */
function wpkites_plus_template_customizer($wp_customize) {
    
    $wp_customize->add_panel('wpkites_template_settings',
            array(
                'priority' => 920,
                'capability' => 'edit_theme_options',
                'title' => __('Page Settings', 'wpkites-plus')
            )
    );

    // Add section to manage Portfolio page settings
    $wp_customize->add_section(
            'porfolio_page_section',
            array(
                'title' => __('Portfolio Page Settings', 'wpkites-plus'),
                'panel' => 'wpkites_template_settings',
                'priority' => 600,
            )
    );

    
    // Portfolio page title
    $wp_customize->add_setting(
            'porfolio_page_title', array(
        'default' => __('Our Portfolio', 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'porfolio_page_title',
            array(
                'type' => 'text',
                'label' => __('Title', 'wpkites-plus'),
                'section' => 'porfolio_page_section',
            )
    );

    // Portfolio page subtitle
    $wp_customize->add_setting(
            'porfolio_page_subtitle', array(
        'default' => __('Our Recent Works', 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'porfolio_page_subtitle',
            array(
                'type' => 'text',
                'label' => __('Sub Title', 'wpkites-plus'),
                'section' => 'porfolio_page_section',
            )
    );
    
    // Add section to manage Portfolio category page settings
    $wp_customize->add_section(
            'porfolio_category_page_section',
            array(
                'title' => __('Portfolio Category Page Settings', 'wpkites-plus'),
                'panel' => 'wpkites_template_settings',
                'priority' => 700,
            )
    );
    
    // Portfolio category page title
    $wp_customize->add_setting(
            'porfolio_category_page_title', array(
        'default' => __('Portfolio Category Title', 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control(
            'porfolio_category_page_title',
            array(
                'type' => 'text',
                'label' => __('Title', 'wpkites-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );

     // Portfolio category page subtitle
    $wp_customize->add_setting(
            'porfolio_category_page_desc', array(
        'default' => 'Morbi facilisis, ipsum ut ullamcorper venenatis, nisi diam placerat turpis, at auctor nisi magna cursus arcu.',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control(
            'porfolio_category_page_desc',
            array(
                'type' => 'text',
                'label' => __('Description', 'wpkites-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    
    // Number of portfolio column layout
    $wp_customize->add_setting('portfolio_cat_column_layout', array(
        'default' => 4,
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('portfolio_cat_column_layout', array(
        'type' => 'select',
        'label' => __('Column Layout', 'wpkites-plus'),
        'section' => 'porfolio_category_page_section',
        'choices' => array(
                6 => '2 ' . __('Column', 'wpkites-plus'),
                4 => '3 ' . __('Column', 'wpkites-plus'),
                3 => '4 ' . __('Column', 'wpkites-plus'),
            )
    ));

    // Add section to manage Contact page settings
    $wp_customize->add_section(
            'contact_page_section',
            array(
                'title' => __('Contact Page Settings', 'wpkites-plus'),
                'panel' => 'wpkites_template_settings',
                'priority' => 800,
            )
    );

    // Contact form title
    $wp_customize->add_setting(
            'contact_cf7_title', array(
        'default' => __("Send Us A Message", 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_cf7_title',
            array(
                'type' => 'text',
                'label' => __('Contact Form Title', 'wpkites-plus'),
                'section' => 'contact_page_section',
            )
    );

    // Contact form subtitle
    $wp_customize->add_setting(
            'contact_cf7_subtitle', array(
        'default' => __("Let's work together", 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_cf7_subtitle',
            array(
                'type' => 'text',
                'label' => __('Contact Form Sub Title', 'wpkites-plus'),
                'section' => 'contact_page_section',
            )
    );

    // Contact Detail Title
    $wp_customize->add_setting(
            'contact_dt_title', array(
        'default' => __("Contact Details", 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_dt_title',
            array(
                'type' => 'text',
                'label' => __('Contact Detail Title', 'wpkites-plus'),
                'section' => 'contact_page_section',
            )
    );

    // Contact Detail subTitle
    $wp_customize->add_setting(
            'contact_dt_subtitle', array(
        'default' => __("Our offices", 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_dt_subtitle',
            array(
                'type' => 'text',
                'label' => __('Contact Detail Sub Title', 'wpkites-plus'),
                'section' => 'contact_page_section',
            )
    );

    // Contact Detail Description
    $wp_customize->add_setting(
            'contact_dt_description', array(
        'default' => __("It would be great to hear from you! If you got any questions, please do not hesitate to send us a message. We are looking forward to hearing from you! We reply within 24 hours!", 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_dt_description',
            array(
                'type' => 'textarea',
                'label' => __('Contact Detail Description', 'wpkites-plus'),
                'section' => 'contact_page_section',
            )
    );

    if (class_exists('WPKites_Plus_Repeater')) {
        $wp_customize->add_setting('wpkites_plus_contact_content', array());

        $wp_customize->add_control(new WPKites_Plus_Repeater($wp_customize, 'wpkites_plus_contact_content', array(
                    'label' => esc_html__('Contact Details', 'wpkites-plus'),
                    'section' => 'contact_page_section',
                    'priority' => 10,
                    'add_field_label' => esc_html__('Add new Contact Info', 'wpkites-plus'),
                    'item_name' => esc_html__('Contact', 'wpkites-plus'),
                    'customizer_repeater_icon_control' => true,
                    'customizer_repeater_title_control' => true,
                    'customizer_repeater_text_control' => true,
        )));
    }
    if (class_exists('wpkites_plus_Repeater')) {
        $wp_customize->add_setting('wpkites_social_links', array());

        $wp_customize->add_control(new wpkites_plus_Repeater($wp_customize, 'wpkites_social_links', array(
                    'label' => esc_html__('Social Links', 'busicare-plus'),
                    'section' => 'contact_page_section',
                    'priority' => 10,
                    'add_field_label' => esc_html__('Add new Social Link', 'busicare-plus'),
                    'item_name' => esc_html__('Social Link', 'busicare-plus'),
                    'customizer_repeater_icon_control' => true,
                    'customizer_repeater_checkbox_control'=> true,
                    'customizer_repeater_link_control' => true,
        )));
    }
    // google map shortcode
    $wp_customize->add_setting('contact_google_map_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('contact_google_map_shortcode', array(
        'label' => __('Google Map Shortcode', 'wpkites-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    // Contact form 7 shortcode
    $wp_customize->add_setting('contact_form_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('contact_form_shortcode', array(
        'label' => esc_html__('Contact Form Shortcode', 'wpkites-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    // Contact form Client Section    
    $wp_customize->selective_refresh->add_partial('contact_cf7_title', array(
        'selector' => '.contact-form h2',
        'settings' => 'contact_cf7_title',
        'render_callback' => 'contact_cf7_title_render_callback'
    ));
    $wp_customize->selective_refresh->add_partial('contact_cf7_subtitle', array(
        'selector' => '.contact-form h5',
        'settings' => 'contact_cf7_subtitle',
        'render_callback' => 'contact_cf7_subtitle_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('contact_dt_title', array(
        'selector' => '.contact-section .contact-info h2,.contact-info.conatct-page4 h2.section-title',
        'settings' => 'contact_dt_title',
        'render_callback' => 'contact_dt_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('contact_dt_subtitle', array(
        'selector' => '.contact-section .contact-info h5.subtitle,.contact-info.conatct-page4 h5.section-subtitle',
        'settings' => 'contact_dt_subtitle',
        'render_callback' => 'contact_dt_subtitle_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('contact_dt_description', array(
        'selector' => '.contact-section p.details',
        'settings' => 'contact_dt_description',
        'render_callback' => 'contact_dt_description_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_title', array(
        'selector' => '.portfolio-cat-page h2.section-title',
        'settings' => 'porfolio_category_page_title',
        'render_callback' => 'porfolio_category_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_desc', array(
        'selector' => '.portfolio-cat-page h5.section-subtitle ',
        'settings' => 'porfolio_category_page_desc',
        'render_callback' => 'porfolio_category_page_desc_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_title', array(
        'selector' => '.portfolio-page h2.section-title',
        'settings' => 'porfolio_page_title',
        'render_callback' => 'porfolio_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_subtitle', array(
        'selector' => '.portfolio-page h5.section-subtitle',
        'settings' => 'porfolio_page_subtitle',
        'render_callback' => 'porfolio_page_subtitle_render_callback'
    ));

    function contact_cf7_title_render_callback() {
        return get_theme_mod('contact_cf7_title');
    }
    function contact_cf7_subtitle_render_callback() {
        return get_theme_mod('contact_cf7_subtitle');
    }
    function contact_dt_title_render_callback() {
        return get_theme_mod('contact_dt_title');
    }
    function contact_dt_subtitle_render_callback() {
        return get_theme_mod('contact_dt_subtitle');
    }
    function contact_dt_description_render_callback() {
        return get_theme_mod('contact_dt_description');
    }

    function contact_info_title_render_callback() {
        return get_theme_mod('contact_info_title');
    }

    function porfolio_category_page_title_render_callback() {
        return get_theme_mod('porfolio_category_page_title');
    }

    function porfolio_category_page_desc_render_callback() {
        return get_theme_mod('porfolio_category_page_desc');
    }

    function porfolio_page_title_render_callback() {
        return get_theme_mod('porfolio_page_title');
    }

    function porfolio_page_subtitle_render_callback() {
        return get_theme_mod('porfolio_page_subtitle');
    }

}

add_action('customize_register', 'wpkites_plus_template_customizer');
?>