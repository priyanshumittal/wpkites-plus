<?php

/* Testimonial Section */
$wp_customize->add_section('testimonial_section', array(
    'title' => __('Testimonials Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
    'priority' => 22,
));

// Enable testimonial section
$wp_customize->add_setting('testimonial_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_sanitize_checkbox'
));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'testimonial_section_enable',
                array(
            'label' => __('Enable/Disable Testimonial Section', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'testimonial_section',
                )
));


// testimonial section title
$wp_customize->add_setting('home_testimonial_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('What Our Client Says', 'wpkites-plus'),
    'sanitize_callback' => 'wpkites_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_testimonial_section_title', array(
    'label' => __('Title', 'wpkites-plus'),
    'section' => 'testimonial_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_testimonial_callback'
));

//testimonials & partners section discription
$wp_customize->add_setting('home_testimonial_section_discription', array(
    'default' => __('Our Testimonial', 'wpkites-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_testimonial_section_discription', array(
    'label' => __('Sub Title', 'wpkites-plus'),
    'section' => 'testimonial_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_testimonial_callback'
));


//Style Design
$wp_customize->add_setting('home_testimonial_design_layout', array('default' => 1));
$wp_customize->add_control('home_testimonial_design_layout',
        array(
            'label' => __('Design Style', 'wpkites-plus'),
            'active_callback' => 'wpkites_plus_testimonial_callback',
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                1 => __('Design', 'wpkites-plus').' 1',
                2 => __('Design', 'wpkites-plus').' 2',
                3 => __('Design', 'wpkites-plus').' 3',
                4 => __('Design', 'wpkites-plus').' 4'
            )
));

//Slide Item
$wp_customize->add_setting('home_testimonial_slide_item', array('default' => 1));
$wp_customize->add_control('home_testimonial_slide_item',
        array(
            'label' => __('Slide Item', 'wpkites-plus'),
            'active_callback' => 'wpkites_plus_testimonial_callback',
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                1 => __('One', 'wpkites-plus'),
                2 => __('Two', 'wpkites-plus'),
                3 => __('Three', 'wpkites-plus'),
            )
));

if (class_exists('WPKites_Plus_Repeater')) {
    $wp_customize->add_setting('wpkites_testimonial_content', array(
    ));

    $wp_customize->add_control(new WPKites_Plus_Repeater($wp_customize, 'wpkites_testimonial_content', array(
                'label' => esc_html__('Testimonial Content', 'wpkites-plus'),
                'section' => 'testimonial_section',
                'add_field_label' => esc_html__('Add new Testimonial', 'wpkites-plus'),
                'item_name' => esc_html__('Testimonial', 'wpkites-plus'),
//                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'customizer_repeater_user_name_control' => true,
                'customizer_repeater_designation_control' => true,
                'active_callback' => 'wpkites_plus_testimonial_callback'
    )));
}

//Testimonial Background Image
$wp_customize->add_setting('testimonial_callout_background', array(
    'default' => WPKITESP_PLUGIN_URL.'/inc/images/bg/img.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'testimonial_callout_background', array(
            'label' => __('Background Image', 'wpkites-plus'),
            'section' => 'testimonial_section',
            'settings' => 'testimonial_callout_background',
            'active_callback' => function($control) {
        return (
                wpkites_plus_testimonial_design_layout_callback($control) &&
                wpkites_plus_testimonial_callback($control)
                );
    },
        )));

// Image overlay
$wp_customize->add_setting('testimonial_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('testimonial_image_overlay', array(
    'label' => __('Enable testimonial image overlay', 'wpkites-plus'),
    'section' => 'testimonial_section',
    'type' => 'checkbox',
    'active_callback' => function($control) {
        return (
                wpkites_plus_testimonial_design_layout_callback($control) &&
                wpkites_plus_testimonial_callback($control)
                );
    },
));


//Testimonial Background Overlay Color
$wp_customize->add_setting('testimonial_overlay_section_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'rgba(0,0,0,0.8)',
));

$wp_customize->add_control(new WPKites_Plus_Customize_Alpha_Color_Control($wp_customize, 'testimonial_overlay_section_color', array(
            'label' => __('Testimonial image overlay color', 'wpkites-plus'),
            'palette' => true,
//            'active_callback' => 'wpkites_testimonial_callback',
            'active_callback' => function($control) {
                return (
                        wpkites_plus_testimonial_design_layout_callback($control) &&
                        wpkites_plus_testimonial_callback($control)
                        );
            },
            'section' => 'testimonial_section')
));

//Navigation Type
$wp_customize->add_setting('testimonial_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('testimonial_nav_style', array(
    'label' => __('Navigation Style', 'wpkites-plus'),
    'section' => 'testimonial_section',
    'type' => 'radio',
    'priority' => 17,
    'choices' => array(
        'bullets' => __('Bullets', 'wpkites-plus'),
        'navigation' => __('Navigation', 'wpkites-plus'),
        'both' => __('Both', 'wpkites-plus'),
    ),
    'active_callback' => 'wpkites_plus_testimonial_callback'
));

// animation speed
$wp_customize->add_setting('testimonial_animation_speed', array('default' => 3000));
$wp_customize->add_control('testimonial_animation_speed',
        array(
            'label' => __('Animation Speed', 'wpkites-plus'),
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'wpkites_plus_testimonial_callback'
));

// smooth speed
$wp_customize->add_setting('testimonial_smooth_speed', array('default' => 1000));
$wp_customize->add_control('testimonial_smooth_speed',
        array(
            'label' => __('Smooth Speed', 'wpkites-plus'),
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0'),
            'active_callback' => 'wpkites_plus_testimonial_callback'
));


/**
 * Add selective refresh for Front page testimonial section controls.
 */
$wp_customize->selective_refresh->add_partial('home_testimonial_section_title', array(
    'selector' => '.section-space.testimonial .section-title',
    'settings' => 'home_testimonial_section_title',
    'render_callback' => 'wpkites_plus_home_testimonial_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_testimonial_section_discription', array(
    'selector' => '.testimonial .section-header h5',
    'settings' => 'home_testimonial_section_discription',
    'render_callback' => 'wpkites_plus_home_testimonial_section_discription_render_callback',
));

function wpkites_plus_home_testimonial_section_title_render_callback() {
    return get_theme_mod('home_testimonial_section_title');
}

function wpkites_plus_home_testimonial_section_discription_render_callback() {
    return get_theme_mod('home_testimonial_section_discription');
}