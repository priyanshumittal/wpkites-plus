<?php

$wp_customize->add_section('contact_section', array(
    'title' => esc_html__('Contact Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
//    'priority' => 2,
));


//Contact Section
$wp_customize->add_setting('home_contact_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_sanitize_checkbox'
));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'home_contact_section_enabled',
                array(
            'label' => esc_html__('Enable Contact on homepage', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'contact_section',
                )
));

//Contact section title
$wp_customize->add_setting('home_contact_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Stay in touch with us', 'wpkites-plus'),
    'sanitize_callback' => 'wpkites_home_page_sanitize_text',
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_contact_section_title', array(
    'label' => esc_html__('Title', 'wpkites-plus'),
    'section' => 'contact_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_contact_callback'
));

//Background Overlay Color
$wp_customize->add_setting('contact_bg_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => '#ffffff',
));

$wp_customize->add_control(new WPKites_Plus_Customize_Alpha_Color_Control($wp_customize, 'contact_bg_color', array(
            'label' => esc_html__('Background Color', 'wpkites-plus'),
            'palette' => true,
            'active_callback' => 'wpkites_plus_contact_callback',
            'section' => 'contact_section')
));


if (class_exists('WPKites_Plus_Repeater')) {
    $wp_customize->add_setting('wpkites_plus_contact_detail_content', array());
    $wp_customize->add_control(new WPKites_Plus_Repeater($wp_customize, 'wpkites_plus_contact_detail_content', array(
                'label' => esc_html__('Contact content', 'wpkites-plus'),
                'section' => 'contact_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Contact', 'wpkites-plus'),
                'item_name' => esc_html__('Contact', 'wpkites-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_image_control' => true,
                'active_callback' => 'wpkites_plus_contact_callback'
    )));
}

$wp_customize->selective_refresh->add_partial('home_contact_section_title', array(
    'selector' => '.section-space.contact-detail h2.section-title',
    'settings' => 'home_contact_section_title',
    'render_callback' => 'home_contact_section_title_render_callback'
));

function home_contact_section_title_render_callback() {
    return get_theme_mod('home_contact_section_title');
}