<?php

/**
 * General Settings Customizer
 *
 * @package wpkites
 */
function wpkites_plus_general_settings_customizer($wp_customize) {

    $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';

    class WPKites_Plus_Header_Logo_Customize_Control_Radio_Image extends WP_Customize_Control {

        /**
         * The type of customize control being rendered.
         *
         * @since 1.1.24
         * @var   string
         */
        public $type = 'radio-image';

        /**
         * Displays the control content.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function render_content() {
            /* If no choices are provided, bail. */
            if (empty($this->choices)) {
                return;
            }
            ?>
            <?php if (!empty($this->label)) : ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif; ?>
            <?php if (!empty($this->description)) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>
            <div id="<?php echo esc_attr("input_{$this->id}"); ?>">
                <?php foreach ($this->choices as $value => $args) : ?>
                    <input type="radio" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr("_customize-radio-{$this->id}"); ?>" id="<?php echo esc_attr("{$this->id}-{$value}"); ?>" <?php $this->link(); ?> <?php checked($this->value(), $value); ?> />
                    <label for="<?php echo esc_attr("{$this->id}-{$value}"); ?>" class="<?php echo esc_attr("{$this->id}-{$value}"); ?>">
                        <?php if (!empty($args['label'])) : ?>
                            <span class="screen-reader-text"><?php echo esc_html($args['label']); ?></span>
                        <?php endif; ?>
                        <img class="wp-ui-highlight" src="<?php echo esc_url(sprintf($args['url'], WPKITESP_PLUGIN_URL, get_stylesheet_directory_uri())); ?>"
                        <?php
                        if (!empty($args['label'])) :
                            echo 'alt="' . esc_attr($args['label']) . '"';
                        endif;
                        ?>
                             />
                    </label>
            <?php endforeach; ?>
            </div><!-- .image -->
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#<?php echo esc_attr("input_{$this->id}"); ?>').buttonset();


                if(jQuery("#_customize-input-after_menu_multiple_option").val()=='menu_btn')
                {
                    jQuery("#customize-control-after_menu_btn_txt").show();
                    jQuery("#customize-control-after_menu_btn_link").show();
                    jQuery("#customize-control-after_menu_btn_new_tabl").show();
                    jQuery("#customize-control-after_menu_btn_border").show();
                    jQuery("#customize-control-after_menu_html").hide();  
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                }
                else if(jQuery("#_customize-input-after_menu_multiple_option").val()=='html')
                {
                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                    jQuery("#customize-control-after_menu_html").show(); 
                }
                else if(jQuery("#_customize-input-after_menu_multiple_option").val()=='top_menu_widget')
                {

                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_html").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").show();
                }
                else
                {
                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_html").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                }
                
                //Js For Homepage Slider Variation
                if(jQuery("#_customize-input-slide_variation").val()=='slide')
                {
                    jQuery("#customize-control-slide_video_upload").hide();
                    jQuery("#customize-control-slide_video_url").hide();
                }
                else
                {
                    jQuery("#customize-control-slide_video_upload").show();
                    jQuery("#customize-control-slide_video_url").show();
                }

                //Js For banner height

                if(jQuery("#_customize-input-banner_height_option").val()=='desktop')
                {
                    jQuery("#customize-control-banner_height_desktop").show();
                    jQuery("#customize-control-banner_height_ipad").hide();
                    jQuery("#customize-control-banner_height_mobile").hide();
                }
                else if(jQuery("#_customize-input-banner_height_option").val()=='ipad')
                {
                    jQuery("#customize-control-banner_height_desktop").hide();
                    jQuery("#customize-control-banner_height_ipad").show();
                    jQuery("#customize-control-banner_height_mobile").hide(); 
                }
                else if(jQuery("#_customize-input-banner_height_option").val()=='mobile')
                {
                    jQuery("#customize-control-banner_height_desktop").hide();
                    jQuery("#customize-control-banner_height_ipad").hide();
                    jQuery("#customize-control-banner_height_mobile").show();
                }
                else
                {
                    jQuery("#customize-control-banner_height_desktop").show();
                    jQuery("#customize-control-banner_height_ipad").hide();
                    jQuery("#customize-control-banner_height_mobile").hide();
                }


                });
            </script>
            <?php
        }
           
        /**
         * Loads the jQuery UI Button script and hooks our custom styles in.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function enqueue() {
            wp_enqueue_script('jquery-ui-button');
            add_action('customize_controls_print_styles', array($this, 'print_styles'));
        }

        /**
         * Outputs custom styles to give the selected image a visible border.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function print_styles() {
            ?>
            <style type="text/css" id="hybrid-customize-radio-image-css">
                .customize-control-radio-image .ui-buttonset {
                    text-align: center;
                }
                .footer_widget_area_placing-3, .footer_widget_area_placing-4, .footer_widget_area_placing-6
                {
                    max-width: 32% !important;
                }
                .footer_widget_area_placing-3 .wp-ui-highlight, .footer_widget_area_placing-4 .wp-ui-highlight, .footer_widget_area_placing-6 .wp-ui-highlight {
                    background-color: #E6E6E6 !important;
                }
                .customize-control-radio-image label {
                    display: inline-block;
                    max-width: 49%;
                    padding: 3px;
                    font-size: inherit;
                    line-height: inherit;
                    height: auto;
                    cursor: pointer;
                    border-width: 0;
                    -webkit-appearance: none;
                    -webkit-border-radius: 0;
                    border-radius: 0;
                    white-space: nowrap;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    color: inherit;
                    background: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    vertical-align: inherit;
                }
                .customize-control-radio-image label:first-of-type {
                    float: left;
                }
                .customize-control-radio-image label:hover {
                    background: none;
                    border-color: inherit;
                    color: inherit;
                }
                .customize-control-radio-image label:active {
                    background: none;
                    border-color: inherit;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    -webkit-transform: none;
                    -ms-transform: none;
                    transform: none;
                }
                .customize-control-radio-image img { border: 1px solid transparent; }
                .customize-control-radio-image .ui-state-active img {
                    border-color: #5B9DD9;
                    -webkit-box-shadow: 0 0 3px rgba(0,115,170,.8);
                    box-shadow: 0 0 3px rgba(0,115,170,.8);
                }
            </style>
            <?php
        }

    }

    $wp_customize->add_panel('wpkites_general_settings',
            array(
                'priority' => 124,
                'capability' => 'edit_theme_options',
                'title' => __('General Settings', 'wpkites-plus')
            )
    );

          // Preloader
    $wp_customize->add_section(
        'preloader_section',
        array(
            'title' =>esc_html__('Preloader','wpkites-plus'),
            'panel'  => 'wpkites_general_settings',
            'priority'   => 1,
            
            )
    );

     $wp_customize->add_setting('preloader_enable',
        array(
            'default' => false,
            'sanitize_callback' => 'wpkites_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize, 'preloader_enable',
        array(
            'label'    => esc_html__( 'Enable/Disable Preloader', 'wpkites-plus' ),
            'section'  => 'preloader_section',
            'type'     => 'toggle',
            'priority' => 1,
        )
    ));

    if ( class_exists( 'WPKites_Plus_Header_Logo_Customize_Control_Radio_Image' ) ) {
            $wp_customize->add_setting(
                'preloader_style', array(
                    'default'           => 1,
                )
            );

            $wp_customize->add_control(
                new WPKites_Plus_Header_Logo_Customize_Control_Radio_Image(
                    $wp_customize, 'preloader_style', array(
                        'label'    => esc_html__('Preloader Style', 'wpkites-plus' ),
                        'description' => esc_html__('You can change the color skin of only first and second Preloader styles', 'wpkites-plus' ),
                        'priority' => 2,
                        'section' => 'preloader_section',
                        'choices' => array(
                            1 => array(
                                'url' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/loader/preloader1.png',
                            ),
                            2 => array(
                                'url' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/loader/preloader2.png',
                            ),
                            3 => array(
                                'url' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/loader/preloader3.png',

                            ),
                            4 => array(
                                'url' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/loader/preloader4.png',
                                
                            ),
                            5 => array(
                                'url' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/loader/preloader5.png',
                                
                            ),
                            6 => array(
                                'url' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/loader/preloader6.png',
                                
                            ),
                        ),
                    )
                )
            );
        }





    // After Menu
    $wp_customize->add_section(
        'after_menu_setting_section',
        array(
            'title' =>esc_html__('After Menu','wpkites-plus'),
            'panel'  => 'wpkites_general_settings',
            'priority'   => 3,
            )
    );

    //Dropdown button or html option
    $wp_customize->add_setting(
    'after_menu_multiple_option',
    array(
        'default'           =>  'none',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'wpkites_plus_sanitize_select',
    ));
    $wp_customize->add_control('after_menu_multiple_option', array(
        'label' => esc_html__('After Menu','wpkites-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_multiple_option',
        'type'    =>  'select',
        'choices' =>  array(
            'none'      =>  esc_html__('None', 'wpkites-plus'),
            'menu_btn'  => esc_html__('Button', 'wpkites-plus'),
            'html'      => esc_html__('HTML', 'wpkites-plus'),
            'top_menu_widget' => esc_html__('Widget', 'wpkites-plus'),
            )
    ));

    //After Menu Button Text
    $wp_customize->add_setting(
    'after_menu_btn_txt',
    array(
        'default'           =>  '',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'wpkites_plus_home_page_sanitize_text',
    ));
    $wp_customize->add_control('after_menu_btn_txt', array(
        'label' => esc_html__('Button Text','wpkites-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_txt',
        'type' => 'text',
    ));

    //After Menu Button Text
    $wp_customize->add_setting(
    'after_menu_btn_link',
    array(
        'default'           =>  '#',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'esc_url_raw',
    ));
    $wp_customize->add_control('after_menu_btn_link', array(
        'label' => esc_html__('Button Link','wpkites-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_link',
        'type' => 'text',
    ));

    //Open in new tab
    $wp_customize->add_setting(
    'after_menu_btn_new_tabl',
    array(
        'default'           =>  false,
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'wpkites_sanitize_checkbox',
    ) );
    
    $wp_customize->add_control('after_menu_btn_new_tabl', array(
        'label' => esc_html__('Open link in a new tab','wpkites-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_new_tabl',
        'type'    =>  'checkbox'
    )); 

    //Border Radius
    $wp_customize->add_setting( 'after_menu_btn_border',
            array(
                'default' => 0,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'after_menu_btn_border',
            array(
                'label' => esc_html__( 'Button Border Radius', 'wpkites-plus' ),
                'section' => 'after_menu_setting_section',
                'input_attrs' => array(
                    'min' => 0,
                    'max' => 30,
                    'step' => 1,),)
        ));

    //After Menu HTML section
    $wp_customize->add_setting('after_menu_html', 
        array(
        'default'=> '',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback'=> 'wpkites_plus_home_page_sanitize_text',
        )
    );

    $wp_customize->add_control('after_menu_html', 
        array(
            'label'=> __('HTML','wpkites-plus'),
            'section'=> 'after_menu_setting_section',
            'type'=> 'textarea',
        )
    );


    class WP_After_Menu_Widget_Customize_Control extends WP_Customize_Control {
        public $type = 'new_menu';
        /**
        * Render the control's content.
        */
        public function render_content() {
        ?>
         <h3><?php _e('To add widgets, Go to Widgets >> After Menu Widget Area','wpkites-plus');?></h3>
        <?php
        }
    }
    $wp_customize->add_setting(
        'after_menu_widget_area_section',
        array(
            'capability'     => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_home_page_sanitize_text',
        )   
    );
    $wp_customize->add_control( new WP_After_Menu_Widget_Customize_Control( $wp_customize, 'after_menu_widget_area_section', array( 
            'section' => 'after_menu_setting_section',
            'setting' => 'after_menu_widget_area_section',
        ))
    );

    //Enable/Disable Cart Icon
    $wp_customize->add_setting('cart_btn_enable',
        array(
            'default' => false,
            'sanitize_callback' => 'wpkites_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize, 'cart_btn_enable',
        array(
            'label'    => esc_html__( 'Enable/Disable Cart Icon', 'wpkites-plus' ),
            'section'  => 'after_menu_setting_section',
            'type'     => 'toggle',
        )
    ));
    
    // Header Preset Section
    $wp_customize->add_section(
            'header_preset_setting_section',
            array(
                'title' => __('Header Presets', 'wpkites-plus'),
                'panel' => 'wpkites_general_settings',
                'priority' => 99,
            )
    );
    if (class_exists('WPKites_Plus_Header_Logo_Customize_Control_Radio_Image')) {
        $wp_customize->add_setting(
                'header_logo_placing', array(
            'default' => 'left',
                )
        );
        $wp_customize->add_control(new WPKites_Plus_Header_Logo_Customize_Control_Radio_Image(
                        $wp_customize, 'header_logo_placing', array(
                    'label' => esc_html__('Header layout with logo placing', 'wpkites-plus'),
                    'priority' => 6,
                    'section' => 'header_preset_setting_section',
                    'choices' => array(
                        'left' => array(
                            'url' => trailingslashit(WPKITESP_PLUGIN_URL) . 'inc/images/header-preset/container-right.png',
                        ),
                        'right' => array(
                            'url' => trailingslashit(WPKITESP_PLUGIN_URL) . 'inc/images/header-preset/container-left.png',
                        ),
                        'center' => array(
                            'url' => trailingslashit(WPKITESP_PLUGIN_URL) . 'inc/images/header-preset/center.png',
                        ),
                        'full' => array(
                            'url' => trailingslashit(WPKITESP_PLUGIN_URL) . 'inc/images/header-preset/full-left.png',
                        ),
                         'five' => array(
                            'url' => trailingslashit(WPKITESP_PLUGIN_URL) . 'inc/images/header-preset/5.png',
                            
                        ),
                        'six' => array(
                            'url' => trailingslashit(WPKITESP_PLUGIN_URL) . 'inc/images/header-preset/6.png',
                            
                        ),
                        'seven' => array(
                            'url' => trailingslashit(WPKITESP_PLUGIN_URL) . 'inc/images/header-preset/7.png',
                            
                        ),
                    ),
                    )
                )
        );
    }

    //Menu Header Info First
    $wp_customize->add_setting(
            'menu_header_icon1', array(
        'default' => __('fa-map-marker', 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_icon1',
            array(
                'type' => 'text',
                'label' => __('Icon First', 'wpkites-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wpkites_plus_menu_header_info_callback',
            )
            );

    $wp_customize->add_setting(
            'menu_header_text1', array(
        'default' => __('1010 Avenue of the Moon<br> New York, NY 10018 US.', 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_text1',
            array(
                'type' => 'textarea',
                'label' => __('Text First', 'wpkites-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wpkites_plus_menu_header_info_callback',
            )
    );



    //Menu Header Info Second
    $wp_customize->add_setting(
            'menu_header_icon2', array(
        'default' => 'fa-clock-o',
        'sanitize_callback' => 'sanitize_text_field',

    ));

    $wp_customize->add_control(
            'menu_header_icon2',
            array(
                'type' => 'text',
                'label' => __('Icon Second', 'wpkites-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wpkites_plus_menu_header_info_callback',
            )
        
    );

    $wp_customize->add_setting(
            'menu_header_text2', array(
        'default' => __('Mon - Sat 8.00 - 18.00<br>Sunday CLOSED', 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_text2',
            array(
                'type' => 'textarea',
                'label' => __('Text Second', 'wpkites-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wpkites_plus_menu_header_info_callback',
            )
    );

    //Menu Header Info Third
    $wp_customize->add_setting(
            'menu_header_icon3', array(
        'default' => 'fa-phone',
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_icon3',
            array(
                'type' => 'text',
                'label' => __('Icon Third', 'wpkites-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wpkites_plus_menu_header_info_callback',
            )
    );

    $wp_customize->add_setting(
            'menu_header_text3', array(
        'default' => __('Mobile: 9876543210,<br> Phone: +234567891', 'wpkites-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_text3',
            array(
                'type' => 'textarea',
                'label' => __('Text Third', 'wpkites-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wpkites_plus_menu_header_info_callback',
            )
        
    );

    // Sticky Header 
    $wp_customize->add_section(
        'sticky_header_section',
        array(
            'title' =>__('Sticky Header','wpkites-plus'),
            'panel'  => 'wpkites_general_settings',
            'priority'   => 99,
            
            )
    );

     $wp_customize->add_setting('sticky_header_enable',
        array(
            'default' => false,
            'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize, 'sticky_header_enable',
        array(
            'label'    => __( 'Enable/Disable Sticky Header', 'wpkites-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'toggle',
            'priority'              => 1,
        )
    ));

    //Differet logo for sticky header
    $wp_customize->add_setting('sticky_header_logo',
        array(
            'default' => false,
            'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize, 'sticky_header_logo',
        array(
            'label'    => __( 'Different Logo for Sticky Header', 'wpkites-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'toggle',
            'active_callback' => 'wpkites_plus_sticky_header_callback',
            'priority'  => 2,
        )
    ));

    // Stick Header logo for Desktop
    $wp_customize->add_setting( 'sticky_header_logo_desktop', array(
              'sanitize_callback' => 'esc_url_raw',
            ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sticky_header_logo_desktop', array(
              'label'    => __( 'Logo for desktop view', 'wpkites-plus' ),
              'section'  => 'sticky_header_section',
              'settings' => 'sticky_header_logo_desktop',
              //'active_callback' => 'wpkites_plus_sticky_header_logo_callback',
              //'active_callback' => 'wpkites_plus_sticky_header_callback',
              'active_callback' => function($control) {
                return (
                        wpkites_plus_sticky_header_logo_callback($control) &&
                        wpkites_plus_sticky_header_callback($control)
                        );
            },
              'priority'    => 3,
            ) ) );

    // Stick Header logo for Mobile
    $wp_customize->add_setting( 'sticky_header_logo_mbl', array(
              'sanitize_callback' => 'esc_url_raw',
            ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sticky_header_logo_mbl', array(
              'label'    => __( 'Logo for mobile view', 'wpkites-plus' ),
              'section'  => 'sticky_header_section',
              'settings' => 'sticky_header_logo_mbl',
              'active_callback' => function($control) {
                return (
                        wpkites_plus_sticky_header_logo_callback($control) &&
                        wpkites_plus_sticky_header_callback($control)
                        );
            },
              'priority'    => 4,
            ) ) );

    //Sticky Header Animation Effect
    $wp_customize->add_setting( 'sticky_header_animation', array( 'default' => '') );
    $wp_customize->add_control( 'sticky_header_animation', 
        array(
            'label'    => __( 'Animation Effect', 'wpkites-plus' ),
            'description' => esc_html__( 'Note: Shrink Effect will not work with Header Presets Five, Six and Seven.', 'wpkites-plus' ),        
            'section'  => 'sticky_header_section',
            'type'     => 'select',
             'active_callback' => 'wpkites_plus_sticky_header_callback',
            'choices'=>array(
                ''=>__('None', 'wpkites-plus'),
                'slide'=>__('Slide', 'wpkites-plus'),
                'fade'=>__('Fade', 'wpkites-plus'),
                'shrink'=>__('Shrink', 'wpkites-plus'),
                )
    ));

    //Sticky Header Enable
    $wp_customize->add_setting( 'sticky_header_device_enable', array( 'default' => 'desktop') );
    $wp_customize->add_control( 'sticky_header_device_enable', 
        array(
            'label'    => __( 'Enable', 'wpkites-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'select',
             'active_callback' => 'wpkites_plus_sticky_header_callback',
            'choices'=>array(
                'desktop'=>__('Desktop', 'wpkites-plus'),
                'mobile'=>__('Mobile', 'wpkites-plus'),
                'both'=>__('Desktop + Mobile', 'wpkites-plus')
                )
    ));
        
        //Sticky Header Opacity
    $wp_customize->add_setting( 'sticky_header_device_enable', array( 'default' => 'desktop') );
    $wp_customize->add_control( 'sticky_header_device_enable', 
        array(
            'label'    => __( 'Enable', 'wpkites-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'select',
            'active_callback' => 'wpkites_plus_sticky_header_callback',
            'choices'=>array(
                'desktop'=>__('Desktop', 'wpkites-plus'),
                'mobile'=>__('Mobile', 'wpkites-plus'),
                'both'=>__('Desktop + Mobile', 'wpkites-plus')
                )
    ));

    $wp_customize->add_setting('sticky_header_opacity',
        array(
            'default' => 1.0,
            'capability'     => 'edit_theme_options',
            ));

    $wp_customize->add_control(new WPKites_Plus_Opacity_Control( $wp_customize, 'sticky_header_opacity',
        array(
            'label'    => __( 'Sticky Opacity', 'wpkites-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'slider',
            'active_callback' => 'wpkites_plus_sticky_header_callback',
            'min'   => 0.1,
            'max'   => 1.0,
        )
    ));
        
        //Sticky Header Height
    $wp_customize->add_setting('sticky_header_height',
        array(
            'default' => 0,
            'capability'     => 'edit_theme_options',
            )
    );

    $wp_customize->add_control(new WPKites_Plus_Slider_Control( $wp_customize, 'sticky_header_height',
        array(
            'label'    => __( 'Sticky Height', 'wpkites-plus' ),
            'description'    => esc_html__( 'Note: Sticky Height will not work with shrink effect', 'wpkites-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'slider',
            'active_callback' => 'wpkites_plus_sticky_header_callback',
            'min'   => 0,
            'max'   => 50,
        )
    ));
    
    // Search Effect settings
    $wp_customize->add_section(
            'search_effect_setting_section',
            array(
                'title' => __('Search Settings', 'wpkites-plus'),
                'panel' => 'wpkites_general_settings',
                'priority' => 100,
            )
    );
    //Enable/Disable Search Effect
    $wp_customize->add_setting('search_btn_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'search_btn_enable',
                    array(
                'label' => __('Enable/Disable Search Icon', 'wpkites-plus'),
                'section' => 'search_effect_setting_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));
    $wp_customize->add_setting('search_effect_style_setting',
            array(
                'default' => 'toogle',
                'sanitize_callback' => 'wpkites_sanitize_select'
            )
    );
    $wp_customize->add_control('search_effect_style_setting',
            array(
                'label' => esc_html__('Choose Search Effect', 'wpkites-plus'),
                'section' => 'search_effect_setting_section',
                'type' => 'radio',
                'active_callback' => 'search_icon_hide_show_callback',
                'choices' => array(
                    'toogle' => esc_html__('Toggle', 'wpkites-plus'),
                    'popup_light' => esc_html__('Pop up light', 'wpkites-plus'),
                    'popup_dark' => esc_html__('Pop up dark', 'wpkites-plus'),
                )
            )
    );

    // add section to manage breadcrumb settings
    $wp_customize->add_section(
            'breadcrumb_setting_section',
            array(
                'title' => __('Breadcrumb Settings', 'wpkites-plus'),
                'panel' => 'wpkites_general_settings',
                'priority' => 100,
            )
    );

    // enable/disable banner
    $wp_customize->add_setting('banner_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'banner_enable',
                    array(
                'label' => __('Enable/Disable Banner', 'wpkites-plus'),
                'section' => 'breadcrumb_setting_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));
    // enable/disable banner subtitle
    $wp_customize->add_setting('breadcrumb_subtitle_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'breadcrumb_subtitle_enable',
                    array(
                'label' => __('Enable/Disable Breadcrumbs Subtitle', 'wpkites-plus'),
                'section' => 'breadcrumb_setting_section',
                'type' => 'toggle',
                'priority' => 2,
                'active_callback' => 'wpkites_plus_breadcrumb_callback',
                    )
    ));
	
			//Dropdown button or html option
	$wp_customize->add_setting(
    'wpkites_breadcrumb_type',
    array(
        'default'           =>  'yoast',
		'capability'        =>  'edit_theme_options',
		'sanitize_callback' =>  'wpkites_sanitize_select',
    ));
	$wp_customize->add_control('wpkites_breadcrumb_type', array(
		'label' => esc_html__('Breadcrumb type','wpkites-plus'),
		'description' => esc_html__( 'If you use other than "default" one you will need to install and activate respective plugins Breadcrumb','wpkites-plus') . ' NavXT, Yoast SEO ' . __('and','wpkites-plus') . ' Rank Math SEO',
        'section' => 'breadcrumb_setting_section',
		'setting' => 'wpkites_breadcrumb_type',
		'type'    =>  'select',
		'priority' => 15,
	  'active_callback'   => 	'wpkites_plus_breadcrumb_callback',
		'choices' =>  array(
			'default' => __('Default(Blank)', 'wpkites-plus'),
            'yoast'  => 'Yoast SEO',
            'rankmath'  => 'Rank Math',
			'navxt'  => 'NavXT',
			)
	));
    // breadcrumb visibility
    $wp_customize->add_setting('breadcrumb_visibility',
        array(
            'default'           =>  'show',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_visibility', 
        array(
            'label'     => esc_html__('Visibility','wpkites-plus' ),
            'section'   => 'breadcrumb_setting_section',
            'setting'   => 'breadcrumb_visibility',
            'active_callback'   => 'wpkites_plus_breadcrumb_callback',
            'priority'  => 3,
            'type'      => 'select',
            'choices'   =>  
            array(
                'show'          =>  esc_html__('Show On All Devices', 'wpkites-plus' ),
                'hide_tablet'   =>  esc_html__('Hide On Tablet', 'wpkites-plus' ),
                'hide_mobile'   =>  esc_html__('Hide On Mobile', 'wpkites-plus' ),
                'hide_tab_mob'  =>  esc_html__('Hide On Tablet & Mobile', 'wpkites-plus' ),
                'hide_all'      =>  esc_html__('Hide On All Devices', 'wpkites-plus' ),
            )
        )
    );
    // Breadcrumb Banner Height
    $wp_customize->add_setting('banner_height_option',
        array(
            'default'           =>  'desktop',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control('banner_height_option', 
        array(
            'label'     => esc_html__('Banner Height','wpkites-plus' ),
            'section'   => 'breadcrumb_setting_section',
            'setting'   => 'banner_height_option',
            'active_callback'   => 'wpkites_plus_breadcrumb_callback',
            'priority'  => 4,
            'type'      => 'select',
            'choices'   =>  
            array(
                'desktop'      =>  esc_html__('Desktop', 'wpkites-plus' ),
                'ipad'         =>  esc_html__('Ipad', 'wpkites-plus' ),
                'mobile'      =>  esc_html__('Mobile', 'wpkites-plus' )
            )
        )
    );
    $wp_customize->add_setting( 'banner_height_desktop',
        array(
            'default'           => 304,
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'banner_height_desktop',
        array(
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'banner_height_desktop',
            'active_callback'   =>  'wpkites_plus_breadcrumb_callback',
            'priority'          =>  5,
            'input_attrs'   => 
                array(
                    'min'   =>  0,
                    'max'   =>  800,
                    'step'  =>  1
                )
        )
    ));
    $wp_customize->add_setting( 'banner_height_ipad',
        array(
            'default'           => 265,
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'banner_height_ipad',
        array(
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'banner_height_ipad',
            'active_callback'   =>  'wpkites_plus_breadcrumb_callback',
            'priority'          =>  5,
            'input_attrs'   => 
                array(
                    'min'   =>  0,
                    'max'   =>  800,
                    'step'  =>  1
                )
        )
    ));
    $wp_customize->add_setting( 'banner_height_mobile',
        array(
            'default'           => 285,
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'banner_height_mobile',
        array(
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'banner_height_mobile',
            'active_callback'   =>  'wpkites_plus_breadcrumb_callback',
            'priority'          =>  5,
            'input_attrs'   => 
                array(
                    'min'   =>  0,
                    'max'   =>  800,
                    'step'  =>  1
                )
        )
    ));
    // Heading for the page title 
    class WPKites_Plus_pagetitle_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Page Title', 'wpkites-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('breadcrumb_page_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_text'
        )
    );
    $wp_customize->add_control(new WPKites_Plus_pagetitle_Customize_Control($wp_customize, 'breadcrumb_page_title', 
        array(
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'breadcrumb_page_title',
            'active_callback'   =>  'wpkites_plus_breadcrumb_callback',
            'priority'          => 6,
        )
    ));
    // Enable/Disable page title
    $wp_customize->add_setting('enable_page_title',
        array(
            'default'           => true,
            'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize, 'enable_page_title',
        array(
            'label'             =>  esc_html__( 'Enable/Disable Page Title', 'wpkites-plus'),
            'section'           =>  'breadcrumb_setting_section',
            'type'              =>  'toggle',
            'active_callback'   =>  'wpkites_plus_breadcrumb_callback',
            'priority'          =>  7
        )
    ));
    // Breadcrumb Position
    $wp_customize->add_setting('breadcrumb_position', 
        array(
            'default'           => esc_html__('page_header','wpkites-plus' ),
            'sanitize_callback' => 'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_position', 
        array(      
            'label'     => esc_html__('Position', 'wpkites-plus' ),        
            'section'   => 'breadcrumb_setting_section',
            'type'      => 'radio',
            'active_callback'   => function($control) {
                                        return (
                                            wpkites_plus_breadcrumb_callback($control) &&
                                            wpkites_plus_page_title_callback($control)
                                        );
                                    },
            'priority'  => 8,
            'choices'   =>  
            array(
                'page_header'   => esc_html__('Page Header', 'wpkites-plus' ),
                'content_area'  => esc_html__('Content Area', 'wpkites-plus' )
            )
        )
    );
    /* Markup */
    $wp_customize->add_setting('breadcrumb_markup',
        array(
            'default'           =>  'h1',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_markup', 
        array(
            'label'     => esc_html__('Markup','wpkites-plus' ),
            'section'   => 'breadcrumb_setting_section',
            'setting'   => 'breadcrumb_markup',
            'active_callback'   => function($control) {
                                        return (
                                            wpkites_plus_breadcrumb_callback($control) &&
                                            wpkites_plus_page_title_callback($control)
                                        );
                                    },
            'priority'  => 9,
            'type'      => 'select',
            'choices'   =>  
            array(
                'h1'      =>  esc_html__('Heading 1', 'wpkites-plus' ),
                'h2'      =>  esc_html__('Heading 2', 'wpkites-plus' ),
                'h3'      =>  esc_html__('Heading 3', 'wpkites-plus' ),
                'h4'      =>  esc_html__('Heading 4', 'wpkites-plus' ),
                'h5'      =>  esc_html__('Heading 5', 'wpkites-plus' ),
                'h6'      =>  esc_html__('Heading 6', 'wpkites-plus' ),
                'span'    =>  esc_html__('Span', 'wpkites-plus' ),
                'p'       =>  esc_html__('Paragraph', 'wpkites-plus' ),
                'div'     =>  esc_html__('Div', 'wpkites-plus' ),
            )
        )
    );
    // Breadcrumb Alignment
    $wp_customize->add_setting( 'breadcrumb_alignment',
        array(
            'default'           => 'parallel',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control( new WPKites_Plus_Image_Radio_Button_Custom_Control( $wp_customize, 'breadcrumb_alignment',
        array(
            'label'     => esc_html__( 'Alignment', 'wpkites-plus'  ),
            'section'   => 'breadcrumb_setting_section',
            'active_callback'   => 'wpkites_plus_breadcrumb_callback',
            'priority'  => 10,
            'choices'   => 
            array(
                'parallel' => array('image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/breadcrumb/breadcrumb-right.png'),
                'parallelr' => array('image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/breadcrumb/breadcrumb-left.png'),
                'centered' => array('image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/breadcrumb/breadcrumb-center.png'),
                'left' => array('image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/breadcrumb/both-on-left.png'),
                'right' => array('image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/breadcrumb/both-on-right.png')   
            )
        )
    ));
    // Breadcrumb padding 
    class WPKites_Plus_Breadcusrmb_Padding_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php _e('Padding','wpkites-plus');?></h3>
        <?php
        }
    }
    $wp_customize->add_setting('breadcrumb_section_padding',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_sanitize_text'
        )   
    );
    $wp_customize->add_control( new WPKites_Plus_Breadcusrmb_Padding_Customize_Control($wp_customize, 'breadcrumb_section_padding', array( 
            'section'   => 'breadcrumb_setting_section',
            'setting'   => 'breadcrumb_section_padding',
            'active_callback'   =>  'wpkites_plus_breadcrumb_callback',
            'priority'  =>  11
        )
    ));
    /* Breadcrumb top padding */
    $wp_customize->add_setting( 'breadcrumb_top_padding',
        array(
            'default'           => 112,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'breadcrumb_top_padding',
        array(
            'label'             => esc_html__( 'Top', 'wpkites-plus'  ),
            'active_callback'   => 'wpkites_plus_breadcrumb_callback',
            'section'           => 'breadcrumb_setting_section',
            'type'              => 'number',
            'priority'          => 12,
            'input_attrs'       => 
                array( 
                    'min' => 0, 
                    'max' => 500, 
                    'step' => 1
                )
        )
    );
    /* Breadcrumb right padding */
    $wp_customize->add_setting( 'breadcrumb_right_padding',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'breadcrumb_right_padding',
        array(
            'label'             => esc_html__( 'Right', 'wpkites-plus'  ),
            'active_callback'   => 'wpkites_plus_breadcrumb_callback',
            'section'           => 'breadcrumb_setting_section',
            'type'              => 'number',
            'priority'          => 12,
            'input_attrs'       => 
                array( 
                    'min' => 0, 
                    'max' => 500, 
                    'step' => 1
                )
        )
    );
    /* Breadcrumb bottom padding */
    $wp_customize->add_setting( 'breadcrumb_bottom_padding',
        array(
            'default'           => 95,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'breadcrumb_bottom_padding',
        array(
            'label'             => esc_html__( 'Bottom', 'wpkites-plus'  ),
            'active_callback'   => 'wpkites_plus_breadcrumb_callback', 
            'section'           => 'breadcrumb_setting_section',
            'type'              => 'number',
            'priority'          => 12,
            'input_attrs'       => 
                array( 
                    'min' => 0, 
                    'max' => 500, 
                    'step' => 1
                )
        )
    );
    /* Breadcrumb left padding */
    $wp_customize->add_setting( 'breadcrumb_left_padding',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'breadcrumb_left_padding',
        array(
            'label'             => esc_html__( 'Left', 'wpkites-plus'  ),
            'active_callback'   => 'wpkites_plus_breadcrumb_callback',
            'section'           => 'breadcrumb_setting_section',
            'type'              => 'number',
            'priority'          => 12,
            'input_attrs'       => 
                array( 
                    'min' => 0, 
                    'max' => 500, 
                    'step' => 1
                )
        )
    );

    /* == Heading for the breadcrumb == */
    class WPKites_Plus_breadcrumbs_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3 style="background:#e5e5e5;padding:10px 12px;margin:0"><?php esc_html_e('Breadcrumbs', 'wpkites-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('bredcrumb_setting',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_text'
        )
    );
    $wp_customize->add_control(new WPKites_Plus_breadcrumbs_Customize_Control($wp_customize, 'bredcrumb_setting', 
        array(
                'section'           =>  'breadcrumb_setting_section',
                'setting'           =>  'bredcrumb_setting',
                'active_callback'   =>  'wpkites_plus_breadcrumb_callback',
                'priority'  => 13,
            )
    ));
    // enable/disable breadcrumb
     $wp_customize->add_setting('breadcrumb_setting_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'breadcrumb_setting_enable',
        array(
            'label' => __('Enable/Disable Breadcrumbs', 'wpkites-plus'),
            'section' => 'breadcrumb_setting_section',
            'type' => 'toggle',
            'active_callback' => 'wpkites_plus_breadcrumb_callback',
            'priority' => 14,
        )
    ));
    // Breadcrumb Style
    $wp_customize->add_setting('breadcrumb_style',
        array(
            'default'           =>  'text',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_style', 
        array(
            'label'     => esc_html__('Home item','wpkites-plus' ),
            'section'   => 'breadcrumb_setting_section',
            'setting'   => 'breadcrumb_style',
            'active_callback'   => 'wpkites_plus_breadcrumb_callback',
            'priority'  => 16,
            'type'      => 'select',
            'choices'   =>  
            array(
                'text'      =>  esc_html__('Text', 'wpkites-plus' ),
                'icon'      =>  esc_html__('Icon', 'wpkites-plus' ),
            )
        )
    );
    // breadcrumbs background image
    $wp_customize->add_setting('breadcrumb_image_enable',
        array(
            'default'           => true,
            'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize, 'breadcrumb_image_enable',
        array(
            'label'             =>  esc_html__( 'Enable/Disable Background Image', 'wpkites-plus'),
            'section'           =>  'breadcrumb_setting_section',
            'active_callback'   => 'wpkites_plus_breadcrumb_callback',
            'type'              =>  'toggle',
            'priority'          =>  17
        )
    ));
    // breadcrumb background image repeat
    $wp_customize->add_setting('breadcrumb_back_image_repeat',
        array(
            'default'           =>  'no-repeat',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_back_image_repeat', 
        array(
            'label'             =>  esc_html__('Background Image Repeat','wpkites-plus' ),
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'breadcrumb_back_image_repeat',
            'active_callback'   =>  function($control) {
                                        return (
                                            wpkites_plus_breadcrumb_callback($control) &&
                                            wpkites_plus_breadcrumb_bgimage_callback($control)
                                        );
                                    },
            'priority'          =>  18,
            'type'              =>  'select',
            'choices'           =>  array(
                'no-repeat'     =>  esc_html__('No-Repeat', 'wpkites-plus' ),
                'repeat'        =>  esc_html__('Repeat All', 'wpkites-plus'  ),
                'repeat-x'      =>  esc_html__('Repeat Horizontally', 'wpkites-plus' ),
                'repeat-y'      =>  esc_html__('Repeat Vertically', 'wpkites-plus' )
            )
        )
    );
    // breadcrumb background image position
    $wp_customize->add_setting('breadcrumb_back_image_position',
        array(
            'default'           =>  'top center',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'wpkites_plus_sanitize_text'
        )
    );
    $wp_customize->add_control('breadcrumb_back_image_position', 
        array(
            'label'             =>  esc_html__('Background Image Position','wpkites-plus' ),
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'breadcrumb_back_image_position',
            'active_callback'   =>  function($control) {
                                        return (
                                            wpkites_plus_breadcrumb_callback($control) &&
                                            wpkites_plus_breadcrumb_bgimage_callback($control)
                                        );
                                    },
            'priority'          =>  19,
            'type'              =>  'select',
            'choices'           =>  array(
                'left top'          =>  esc_html__('Left Top', 'wpkites-plus' ),
                'left center'       =>  esc_html__('Left Center', 'wpkites-plus' ),
                'left bottom'       =>  esc_html__('Left Bottom', 'wpkites-plus' ),
                'right top'         =>  esc_html__('Right Top', 'wpkites-plus' ),
                'right center'      =>  esc_html__('Right Center', 'wpkites-plus' ),
                'right bottom'      =>  esc_html__('Right Bottom', 'wpkites-plus' ),
                'top center'        =>  esc_html__('Center Top', 'wpkites-plus' ),
                'center center'     =>  esc_html__('Center Center', 'wpkites-plus' ),
                'center bottom'     =>  esc_html__('Center Bottom', 'wpkites-plus' ),
            )
        )
    );
    // breadcrumb background size
    $wp_customize->add_setting('breadcrumb_back_size',
        array(
            'default'           =>  'cover',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_back_size', 
        array(
            'label'             =>  esc_html__('Background Size','wpkites-plus' ),
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'breadcrumb_back_size',
            'active_callback'   =>  function($control) {
                                        return (
                                            wpkites_plus_breadcrumb_callback($control) &&
                                            wpkites_plus_breadcrumb_bgimage_callback($control)
                                        );
                                    },
            'priority'          =>  20,
            'type'              =>  'select',
            'choices'           =>  array(
                'cover'         =>  esc_html__('Cover', 'wpkites-plus' ),
                'contain'       =>  esc_html__('Contain', 'wpkites-plus' ),
                'auto'          =>  esc_html__('Auto', 'wpkites-plus' )
            )
        )
    );
    // breadcrumb background attachment
    $wp_customize->add_setting('breadcrumb_back_attachment',
        array(
            'default'           =>  'scroll',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'wpkites-plus_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_back_attachment', 
        array(
            'label'             =>  esc_html__('Background Attachment','wpkites-plus' ),
            'description'       =>  esc_html__('Note: Background Image Repeat and Background Image Position will not work with Background Attachment Fixed property', 'wpkites-plus'),
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'breadcrumb_back_attachment',
            'active_callback'   =>  function($control) {
                                        return (
                                            wpkites_plus_breadcrumb_callback($control) &&
                                            wpkites_plus_breadcrumb_bgimage_callback($control)
                                        );
                                    },
            'priority'          =>  21,
            'type'              =>  'select',
            'choices'           =>  array(
                'scroll'        =>  esc_html__('Scroll', 'wpkites-plus' ),
                'fixed'         =>  esc_html__('Fixed', 'wpkites-plus' )            )
        )
    );
    // Image overlay
    $wp_customize->add_setting('breadcrumb_image_overlay',
            array(
                'default' => true,
                'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'breadcrumb_image_overlay',
        array(
            'label' => __('Enable/Disable Banner Image Overlay', 'wpkites-plus'),
            'section' => 'breadcrumb_setting_section',
            'type' => 'toggle',
            'active_callback' => function($control) {
                                        return (
                                            wpkites_plus_breadcrumb_callback($control) &&
                                            wpkites_plus_breadcrumb_bgimage_callback($control)
                                        );
                                    },
            'priority' => 22,
        )
    ));
    //breadcrumb overlay color
    $wp_customize->add_setting( 'breadcrumb_overlay_section_color', 
        array(
            'sanitize_callback' => 'sanitize_text_field',
            'default' => 'rgba(0,0,0,0.6)',
        ) 
    );        
    $wp_customize->add_control(new WPKites_Plus_Customize_Alpha_Color_Control( $wp_customize,'breadcrumb_overlay_section_color', 
        array(
            'label'      => __('Image Overlay Color','wpkites-plus' ),
            'palette' => true,
            'active_callback' => function($control) {
                                            return (
                                                wpkites_plus_breadcrumb_callback($control) &&
                                                wpkites_plus_breadcrumb_bgimage_callback($control)
                                            );
                                        },
            'section' => 'breadcrumb_setting_section',
            'priority' => 23
        )
    ) );
    //Bg Image for 404 page
    $wp_customize->add_setting( 'error_background_img', 
        array(
              'sanitize_callback' => 'esc_url_raw',
        ) 
    );       
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'error_background_img', 
        array(
            'label'    => __( '404 Page', 'wpkites-plus' ),
            'section'  => 'breadcrumb_setting_section',
            'settings' => 'error_background_img',
            'active_callback' => function($control) {
                                        return (
                                            wpkites_plus_breadcrumb_callback($control) &&
                                            wpkites_plus_breadcrumb_bgimage_callback($control)
                                        );
                                    },
            'priority'  =>  24
        ) 
    ) );
    //Bg Image for search page
    $wp_customize->add_setting( 'search_background_img', 
        array(
              'sanitize_callback' => 'esc_url_raw',
        ) 
    );        
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'search_background_img', 
        array(
            'label'    => __( 'Search Page', 'wpkites-plus' ),
            'section'  => 'breadcrumb_setting_section',
            'settings' => 'search_background_img',
            'active_callback' => function($control) {
                                    return (
                                        wpkites_plus_breadcrumb_callback($control) &&
                                        wpkites_plus_breadcrumb_bgimage_callback($control)
                                    );
                                },
            'priority'  =>  25
        ) 
    ) );
    //Bg Image for date page
    $wp_customize->add_setting( 'date_background_img', 
        array(
              'sanitize_callback' => 'esc_url_raw',
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'date_background_img', 
        array(
            'label'    => __( 'Date Archive', 'wpkites-plus' ),
            'section'  => 'breadcrumb_setting_section',
            'settings' => 'date_background_img',
            'active_callback' => function($control) {
                                    return (
                                        wpkites_plus_breadcrumb_callback($control) &&
                                        wpkites_plus_breadcrumb_bgimage_callback($control)
                                    );
                                },
            'priority'  =>  26
    ) ) );
    //Bg Image for author page
    $wp_customize->add_setting( 'author_background_img', 
        array(
              'sanitize_callback' => 'esc_url_raw',
        ) 
    );        
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'author_background_img', 
        array(
              'label'    => __( 'Author Archive', 'wpkites-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'author_background_img',
              'active_callback' => function($control) {
                                    return (
                                        wpkites_plus_breadcrumb_callback($control) &&
                                        wpkites_plus_breadcrumb_bgimage_callback($control)
                                    );
                                },
            'priority'  =>  27
        ) 
    ) );
    // Shop breadcrumb background image
    $wp_customize->add_setting( 'shop_breadcrumb_back_img', 
        array(
            'sanitize_callback' => 'esc_url_raw'
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'shop_breadcrumb_back_img', 
        array(
            'label'             =>  esc_html__( 'Shop Page', 'wpkites-plus' ),
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'shop_breadcrumb_back_img',
            'active_callback' => function($control) {
                    return (
                        wpkites_plus_breadcrumb_callback($control) &&
                        wpkites_plus_breadcrumb_bgimage_callback($control)
                    );
                },
            'priority'  =>  28
        ) 
    ));

    /* ===================
    // Container Setting
    =================== */
    $wp_customize->add_section(
        'container_width_section',
        array(
            'title' =>__('Container Settings','wpkites-plus'),
            'panel'  => 'wpkites_general_settings',
            'priority'   => 102,
            
            )
    );
    
    //Container width
    $wp_customize->add_setting( 'container_width_pattern',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_width_pattern',
        array(
            'label'    => __( 'Container Width', 'wpkites-plus' ),
            'priority' => 1,
            'section' => 'container_width_section',
            'description'   =>  esc_html__( 'Note: Container Width will not work with stretched sidebar layout.', 'wpkites-plus'  ),
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));
    //Content width
    $wp_customize->add_setting( 'content_width',
        array(
            'default' => 66.6,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'content_width',
        array(
            'label'    => __( 'Content Width', 'wpkites-plus' ),
            'priority' => 2,
            'section' => 'container_width_section',
            'description'  =>  esc_html__( 'Note: Content Width will work only with sidebar layout.', 'wpkites-plus'),
            'input_attrs' => array(
                'min' => 0,
                'max' => 100,
                'step' => 1,),)
    ));
    //sidebar width
    $wp_customize->add_setting( 'sidebar_width',
        array(
            'default' => 33.3,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'sidebar_width',
        array(
            'label'    => __( 'Sidebar Width', 'wpkites-plus' ),
            'priority' => 3,
            'section' => 'container_width_section',
            'description'  =>  esc_html__( 'Note: Sidebar Width will work only with sidebar layout.', 'wpkites-plus'),
            'input_attrs' => array(
                'min' => 0,
                'max' => 100,
                'step' => 1,),)
    ));


    $wp_customize->add_setting( 'page_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'page_container_setting', 
        array(
            'label'    => __( 'Page Layout', 'wpkites-plus' ),
            'priority' => 4,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'wpkites-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'wpkites-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'wpkites-plus'),
                )
        ));
        $wp_customize->add_setting( 'post_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'post_container_setting', 
        array(
            'label'    => __( 'Blog Layout', 'wpkites-plus' ),
            'priority' => 5,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'wpkites-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'wpkites-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'wpkites-plus'),
                )
        ));
        $wp_customize->add_setting( 'single_post_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'single_post_container_setting', 
        array(
            'label'    => __( 'Single Post Layout', 'wpkites-plus' ),
            'priority' => 6,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'wpkites-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'wpkites-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'wpkites-plus'),
                )
        ));


class WP_Home_Container_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
     <h3><?php esc_attr_e('Homepage Sections Container Width','wpkites-plus'); ?></h3>
    <?php
    }
}
$wp_customize->add_setting(
    'business_temp_container_width',
    array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )   
);
$wp_customize->add_control( new WP_Home_Container_Customize_Control( $wp_customize, 'business_temp_container_width', array( 
        'section' => 'container_width_section',
        'setting' => 'business_temp_container_width',
    ))
);

        //Container Width For Slider Section
    $wp_customize->add_setting( 'container_slider_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_slider_width',
            array(
                'label'    => __( 'Slider', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));

    //Container Width For Callout  Section
    $wp_customize->add_setting( 'container_cta1_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_cta1_width',
            array(
                'label'    => __( 'CTA 1', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));


    //Container Width For Service Section
    $wp_customize->add_setting( 'container_service_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_service_width',
            array(
                'label'    => __( 'Service', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 
 
    
    //Container Width For Portfolio Section
    $wp_customize->add_setting( 'container_portfolio_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_portfolio_width',
            array(
                'label'    => __( 'Portfolio', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 

    //Container Width For Callout  Section
    $wp_customize->add_setting( 'container_cta2_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_cta2_width',
            array(
                'label'    => __( 'CTA 2', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));

    //Container Width For About  Section
    $wp_customize->add_setting( 'container_about_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_about_width',
            array(
                'label'    => __( 'About', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));
    
   //Container Width For TEAM Section
    $wp_customize->add_setting( 'container_team_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_team_width',
            array(
                'label'    => __( 'Team', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));   


    //Container Width For Blog Section
    $wp_customize->add_setting( 'container_home_blog_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_home_blog_width',
            array(
                'label'    => __( 'Blog', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 

    //Container Width For Fun&Fact Section
        $wp_customize->add_setting( 'container_fun_fact_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_fun_fact_width',
            array(
                'label'    => __( 'FunFact', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));  


    //Container Width For SHOP Section
    $wp_customize->add_setting( 'container_shop_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_shop_width',
            array(
                'label'    => __( 'Shop', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        )); 

    //Container Width For Testimonial Section
    $wp_customize->add_setting( 'container_testimonial_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_testimonial_width',
            array(
                'label'    => __( 'Testimonial', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));


    //Container Width For Client & Partners Section
    $wp_customize->add_setting( 'container_clients_width',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPKites_Slider_Custom_Control( $wp_customize, 'container_clients_width',
            array(
                'label'    => __( 'Clients', 'wpkites-plus' ),
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));
    

    /******************** Sidebar Layouts *******************************/
    $wp_customize->add_section('sidebar_layout_setting_section',
        array(
            'title' =>esc_html__('Sidebar Layout','wpkites-plus' ),
            'panel'  => 'wpkites_general_settings',
        )
    );
    // Blog/Archive sidebar layout
    $wp_customize->add_setting( 'blog_sidebar_layout',
        array(
            'default'           => 'right',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_sanitize_select'
        )
    );
    $wp_customize->add_control( new WPKites_Plus_Image_Radio_Button_Custom_Control( $wp_customize, 'blog_sidebar_layout',
        array(
            'label' => esc_html__( 'Blog/Archives', 'wpkites-plus'  ),
            'section' => 'sidebar_layout_setting_section',
            'priority'  => 1,
            'choices' => array(
                'right' => array( 
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/right.jpg',
                ),
                'left' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/left.jpg',
                ),
                'full' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/full.jpg',
                ),
                'stretched' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/stretched.jpg',
                )
            )
        )
    ) );

    // Single post sidebar layout
    $wp_customize->add_setting( 'single_post_sidebar_layout',
        array(
            'default'           => 'right',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control( new WPKites_Plus_Image_Radio_Button_Custom_Control( $wp_customize, 'single_post_sidebar_layout',
        array(
            'label' => esc_html__( 'Single Post', 'wpkites-plus'  ),
            'section' => 'sidebar_layout_setting_section',
            'priority'  => 2,
            'choices' => array(
                'right' => array( 
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/right.jpg',
                ),
                'left' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/left.jpg',
                ),
                'full' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/full.jpg',
                ),
                'stretched' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/stretched.jpg',
                )
            )
        )
    ) );

    // Single post sidebar layout
    $wp_customize->add_setting( 'page_sidebar_layout',
        array(
            'default'           => 'right',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_plus_sanitize_select'
        )
    );
    $wp_customize->add_control( new WPKites_Plus_Image_Radio_Button_Custom_Control( $wp_customize, 'page_sidebar_layout',
        array(
            'label' => esc_html__( 'Page', 'wpkites-plus'  ),
            'section' => 'sidebar_layout_setting_section',
            'priority'  => 3,
            'choices' => array(
                'right' => array( 
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/right.jpg',
                ),
                'left' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/left.jpg',
                ),
                'full' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/full.jpg',
                ),
                'stretched' => array(
                    'image' => trailingslashit( WPKITESP_PLUGIN_URL ) . 'inc/images/sidebar-layout/stretched.jpg',
                )
            )
        )
    ) );


    // Post Navigation effect
    $wp_customize->add_section(
        'post_navigation_section',
        array(
            'title' =>__('Post Navigation','wpkites-plus'),
            'panel'  => 'wpkites_general_settings',
            'priority'   => 102,
            
            )
    );
    $wp_customize->add_setting('post_nav_style_setting', 
    array(
        'default'           => 'pagination',
        'sanitize_callback' => 'wpkites_plus_sanitize_select'
        )
    );

    $wp_customize->add_control('post_nav_style_setting', 
    array(      
        'label'     => esc_html__('Choose Style', 'wpkites-plus'),      
        'section'   => 'post_navigation_section',
        'type'      => 'radio',
        'choices'   =>  array(
            'pagination'    => esc_html__('Pagination', 'wpkites-plus'),
            'load_more'     => esc_html__('Load More', 'wpkites-plus'),
            'infinite'  => esc_html__('Infinite Scroll', 'wpkites-plus'),
            )
        )
    );

    // add section to manage scroll_to_top icon settings
    $wp_customize->add_section(
            'scrolltotop_setting_section',
            array(
                'title' => __('Scroll to Top Settings', 'wpkites-plus'),
                'panel' => 'wpkites_general_settings',
                'priority' => 102,
            )
    );

    // enable/disable scroll_to_top icon
     $wp_customize->add_setting('scrolltotop_setting_enable',
        array(
            'default' => true,
            'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize, 'scrolltotop_setting_enable',
        array(
            'label'    => __( 'Enable/Disable Scroll to Top', 'wpkites-plus' ),
            'section'  => 'scrolltotop_setting_section',
            'type'     => 'toggle',
            'priority'              => 1,
        )
    ));


    $wp_customize->add_setting('scroll_position_setting',
            array(
                'default' => 'right',
                'sanitize_callback' => 'wpkites_plus_sanitize_select'
            )
    );

    $wp_customize->add_control('scroll_position_setting',
            array(
                'label' => esc_html__('Choose Position', 'wpkites-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'type' => 'radio',
                'choices' => array(
                    'left' => esc_html__('Left', 'wpkites-plus'),
                    'right' => esc_html__('Right', 'wpkites-plus'),
                )
            )
    );


    $wp_customize->add_setting('wpkites_plus_scroll_icon_class',
            array(
                'default' => 'fa fa-arrow-up',
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control('wpkites_plus_scroll_icon_class',
            array(
                'label' => esc_html__('Icon Class Name', 'wpkites-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'type' => 'text',
            )
    );

    $wp_customize->add_setting('wpkites_plus_scroll_border_radius',
            array(
                'default' => 3,
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'wpkites_plus_sanitize_number_range',
            )
    );
    $wp_customize->add_control('wpkites_plus_scroll_border_radius',
            array(
                'label' => esc_html__('Border Radius', 'wpkites-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'type' => 'number',
                'input_attrs' => array('min' => 0, 'max' => 50, 'step' => 1, 'style' => 'width: 100%;'),
            )
    );

    // enable/disable scrolltotop color settings
    $wp_customize->add_setting(
            'apply_scrll_top_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_scrll_top_clr_enable',
            array(
                'label' => esc_html__('Click here to apply the below color settings', 'wpkites-plus'),
                'type' => 'checkbox',
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                
            )
    );

    $wp_customize->add_setting(
            'wpkites__scroll_bg_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#926AA6'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'wpkites__scroll_bg_color',
                    array(
                'label' => esc_html__('Background Color', 'wpkites-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'settings' => 'wpkites__scroll_bg_color',
    )));


    $wp_customize->add_setting(
            'wpkites__scroll_icon_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#fff'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'wpkites__scroll_icon_color',
                    array(
                'label' => esc_html__('Icon Color', 'wpkites-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'settings' => 'wpkites__scroll_icon_color',
    )));


    $wp_customize->add_setting(
            'wpkites__scroll_bghover_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#fff'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'wpkites__scroll_bghover_color',
                    array(
                'label' => esc_html__('Background Hover Color', 'wpkites-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'settings' => 'wpkites__scroll_bghover_color',
    )));


    $wp_customize->add_setting(
            'wpkites__scroll_iconhover_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#926AA6'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'wpkites__scroll_iconhover_color',
                    array(
                'label' => esc_html__('Icon Hover Color', 'wpkites-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'settings' => 'wpkites__scroll_iconhover_color',
    )));

   
}

add_action('customize_register', 'wpkites_plus_general_settings_customizer');
?>