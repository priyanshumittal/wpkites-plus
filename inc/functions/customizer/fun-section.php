<?php

/* funfact section */
$wp_customize->add_setting('funfact_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_sanitize_checkbox'
));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'funfact_section_enabled',
                array(
            'label' => __('Enable/Disable Funfact Section', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'funfact_section',
                )
));

$wp_customize->add_section('funfact_section', array(
    'title' => __('Funfact Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
    'priority' => 17,
));

if (class_exists('WPKites_Plus_Repeater')) {
    $wp_customize->add_setting('wpkites_funfact_content', array());

    $wp_customize->add_control(new WPKites_Plus_Repeater($wp_customize, 'wpkites_funfact_content', array(
                'label' => esc_html__('Funfact Content', 'wpkites-plus'),
                'section' => 'funfact_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Funfact', 'wpkites-plus'),
                'item_name' => esc_html__('Funfact', 'wpkites-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'active_callback' => 'wpkites_plus_funfact_callback'
    )));
}


//FunFact Background Image
$wp_customize->add_setting('funfact_callout_background', array(
    'default' => WPKITESP_PLUGIN_URL .'inc/images/bg/funfact-bg.png',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'funfact_callout_background', array(
            'label' => esc_html__('Background Image', 'wpkites-plus'),
            'section' => 'funfact_section',
            'settings' => 'funfact_callout_background',
            'active_callback' => 'wpkites_plus_funfact_callback'
        )));

// Image overlay
$wp_customize->add_setting('funfact_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('funfact_image_overlay', array(
    'label' => esc_html__('Enable Funfact image overlay', 'wpkites-plus'),
    'section' => 'funfact_section',
    'type' => 'checkbox',
    'active_callback' => 'wpkites_plus_funfact_callback'
));

/**
 * Add selective refresh for Front page funfact section controls.
 */
$wp_customize->selective_refresh->add_partial('wpkites_funfact_content', array(
    'selector' => '.funfact .wpkites-fun-container',
    'settings' => 'wpkites_funfact_content',
    'render_callback' => 'wpkites_funfact_content_render_callback'
));

function wpkites_funfact_content_render_callback() {
    return get_theme_mod('wpkites_funfact_content');
}
$wp_customize->selective_refresh->add_partial('funfact_callout_background', array(
    'selector' => '.funfact.bg-default',
    'settings' => 'funfact_callout_background',    
    'render_callback' => 'funfact_callout_background_render_callback'
));

function funfact_callout_background_render_callback() {
    return get_theme_mod('funfact_callout_background');
}

?>