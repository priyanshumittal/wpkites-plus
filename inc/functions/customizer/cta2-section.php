<?php

//Callout Section
$wp_customize->add_section('home_cta2_page_section', array(
    'title' => esc_html__('CTA 2 Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
    'priority' => 12,
));

// Enable call to action section
$wp_customize->add_setting('cta2_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_sanitize_checkbox'
    ));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'cta2_section_enable',
                array(
            'label' => esc_html__('Enable/Disable CTA 2 Section', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'home_cta2_page_section',
                )
));

//cta2 Background Image
$wp_customize->add_setting('callout_cta2_background', array(
    'default'=> WPKITESP_PLUGIN_URL.'/inc/images/bg/cta-3.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'callout_cta2_background', array(
            'label' => esc_html__('Background Image', 'wpkites-plus'),
            'section' => 'home_cta2_page_section',
            'settings' => 'callout_cta2_background',
            'active_callback' => 'wpkites_plus_cta_callback'
        )));

// Image overlay
$wp_customize->add_setting('cta2_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('cta2_image_overlay', array(
    'label' => esc_html__('Enable CTA 2 image overlay', 'wpkites-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'checkbox',
    'active_callback' => 'wpkites_plus_cta_callback'
));


//callout Background Overlay Color
$wp_customize->add_setting('cta2_overlay_section_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'rgba( 146,106,166,0.80)',
));

$wp_customize->add_control(new WPKites_Plus_Customize_Alpha_Color_Control($wp_customize, 'cta2_overlay_section_color', array(
            'label' => esc_html__('CTA 2 Image Overlay Color', 'wpkites-plus'),
            'palette' => true,
            'active_callback' => 'wpkites_plus_cta_callback',
            'section' => 'home_cta2_page_section')
));

$wp_customize->add_setting(
        'home_cta2_title',
        array(
            'default' => esc_html__('Stay Connected With Us', 'wpkites-plus'),
            
        )
);
$wp_customize->add_control('home_cta2_title', array(
    'label' => esc_html__('Title', 'wpkites-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_cta_callback'
    ));

$wp_customize->add_setting(
        'home_cta2_desc',
        array(
            'default' => __('We Work With You To Address Your Most <br> Critical Business Priorities', 'wpkites-plus'),
           
        )
);
$wp_customize->add_control('home_cta2_desc', array(
    'label' => esc_html__('Description', 'wpkites-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'textarea',
    'active_callback' => 'wpkites_plus_cta_callback'
    ));

//Button link 
$wp_customize->add_setting(
        'home_cta2_btn1_link',
        array(
            'default' => '<iframe width="560" height="315" src="https://www.youtube.com/embed/8OBfr46Y0cQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        
));


$wp_customize->add_control(
        'home_cta2_btn1_link',
        array(
            'label' => esc_html__('Embed Video Code', 'wpkites-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'textarea',
            'active_callback' => 'wpkites_plus_cta_callback'
));

/**
 * Add selective refresh for Front page pricing section controls.
 */
$wp_customize->selective_refresh->add_partial('home_cta2_title', array(
    'selector' => '.cta-2 .cta-block p',
    'settings' => 'home_cta2_title',
    'render_callback' => 'home_cta2_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_cta2_desc', array(
    'selector' => '.cta-2 .cta-block h2',
    'settings' => 'home_cta2_desc',
    'render_callback' => 'home_cta2_desc_render_callback',
));

function home_cta2_title_render_callback() {
    return get_theme_mod('home_cta2_title');
}

function home_cta2_desc_render_callback() {
    return get_theme_mod('home_cta2_desc');
}
?>