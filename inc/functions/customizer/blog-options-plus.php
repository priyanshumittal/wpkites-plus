<?php

/**
 * Single Blog Options Customizer
 *
 * @package wpkites
 */
function wpkites_plus_single_blog_customizer($wp_customize) {

    /************************* Blog Button Title*********************************/
$wp_customize->add_setting( 'wpkites_blog_button_title',
    array(
        'default'           => esc_html__('Read More','wpkites-plus'),
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wpkites_sanitize_text',    
        )
    );
$wp_customize->add_control( 'wpkites_blog_button_title',
    array(
        'label'    => esc_html__( 'Read More Button Text', 'wpkites-plus' ),
        'section'  => 'wpkites_blog_section',
        'type'     => 'text',   
        'priority' =>4,
        )
    );

/************************* Enable Author*********************************/
$wp_customize->add_setting( 'wpkites_enable_blog_author',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wpkites_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize,  'wpkites_enable_blog_author',
    array(
        'label'    => esc_html__( 'Hide/Show Author', 'wpkites-plus' ),
        'section'  => 'wpkites_blog_section',
        'type'     => 'toggle', 
        'priority' => 5,
        )
    ));

/************************* Enable Date*********************************/
$wp_customize->add_setting( 'wpkites_enable_blog_date',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wpkites_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize,  'wpkites_enable_blog_date',
    array(
        'label'    => esc_html__( 'Hide/Show Date', 'wpkites-plus' ),
        'section'  => 'wpkites_blog_section',
        'type'     => 'toggle', 
        'priority' => 6,
        )
    ));

/************************* Enable Category*********************************/
$wp_customize->add_setting( 'wpkites_enable_blog_category',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wpkites_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize,   'wpkites_enable_blog_category',
    array(
        'label'    => esc_html__( 'Hide/Show Category', 'wpkites-plus' ),
        'section'  => 'wpkites_blog_section',
        'type'     => 'toggle', 
        'priority' => 7,
        )
    ));

/************************* Enable blog*********************************/
$wp_customize->add_setting('wpkites_enable_blog_comnt',
            array(
                'default' => true,
                'sanitize_callback' => 'wpkites_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'wpkites_enable_blog_comnt',
            array(
                'label' => esc_html__('Hide/Show Comments', 'wpkites-plus'),
                'type' => 'toggle',
                'section' => 'wpkites_blog_section',
                'priority' => 8,
            )
    ));
/************************* Enable Continue Reading Button*********************************/
$wp_customize->add_setting( 'wpkites_enable_blog_read_button',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wpkites_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new WPKites_Toggle_Control( $wp_customize, 'wpkites_enable_blog_read_button',
    array(
        'label'    => esc_html__( 'Hide/Show Read More Button', 'wpkites-plus' ),
        'section'  => 'wpkites_blog_section',
        'type'     => 'toggle', 
        'priority' => 9,
        )
    ));

    /*     * *********************** Related Post  ******************************** */

    $wp_customize->add_setting('wpkites_enable_related_post',
            array(
                'default' => true,
                'sanitize_callback' => 'wpkites_plus_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'wpkites_enable_related_post',
                    array(
                'label' => esc_html__('Enable/Disable Related Posts', 'wpkites-plus'),
                'type' => 'toggle',
                'section' => 'wpkites_single_blog_section',
                'priority' => 1,
                    )
    ));

    /*     * *********************** Related Post Title  ******************************** */

    $wp_customize->add_setting('wpkites_related_post_title',
            array(
                'default' => esc_html__('Related Posts', 'wpkites-plus'),
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control('wpkites_related_post_title',
            array(
                'label' => esc_html__('Related Posts Title', 'wpkites-plus'),
                'type' => 'text',
                'section' => 'wpkites_single_blog_section',
                'priority' => 2,
                'active_callback' => 'wpkites_plus_rt_post_callback',
            )
    );

    /*     * *********************** Related Post Option ******************************** */

    $wp_customize->add_setting('wpkites_related_post_option',
            array(
                'default' => 'categories',
                'sanitize_callback' => 'wpkites_sanitize_select'
            )
    );

    $wp_customize->add_control('wpkites_related_post_option',
            array(
                'label' => esc_html__('Related Posts Option', 'wpkites-plus'),
                'section' => 'wpkites_single_blog_section',
                'settings' => 'wpkites_related_post_option',
                'active_callback' => 'wpkites_plus_rt_post_callback',
                'priority' => 3,
                'type' => 'select',
                'choices' => array(
                    'categories' => esc_html__('All', 'wpkites-plus'),
                    'tags' => esc_html__('Related by tags', 'wpkites-plus'),
                )
            )
    );
}

add_action('customize_register', 'wpkites_plus_single_blog_customizer');