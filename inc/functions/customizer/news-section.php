<?php

//Latest News Section
$wp_customize->add_section('wpkites_latest_news_section', array(
    'title' => __('Latest News Settings', 'wpkites-plus'),
    'panel' => 'section_settings',
    'priority' =>16,
));


// Enable news section
$wp_customize->add_setting('latest_news_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wpkites_plus_sanitize_checkbox'
));

$wp_customize->add_control(new WPKites_Toggle_Control($wp_customize, 'latest_news_section_enable',
                array(
            'label' => __('Enable/Disable Latest News Section', 'wpkites-plus'),
            'type' => 'toggle',
            'section' => 'wpkites_latest_news_section',
                )
));

// News section title
$wp_customize->add_setting('home_news_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('All News & Articles', 'wpkites-plus'),
    'sanitize_callback' => 'wpkites_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_title', array(
    'label' => __('Title', 'wpkites-plus'),
    'section' => 'wpkites_latest_news_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_news_callback'
));



//News section subtitle
$wp_customize->add_setting('home_news_section_discription', array(
    'default' => __('Our Blogs', 'wpkites-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_discription', array(
    'label' => __('Sub Title', 'wpkites-plus'),
    'section' => 'wpkites_latest_news_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_news_callback'
));

$wp_customize->add_setting('home_news_design_layout', array('default' => 1));
$wp_customize->add_control('home_news_design_layout',
        array(
            'label' => __('Design Style', 'wpkites-plus'),
            'section' => 'wpkites_latest_news_section',
            'active_callback' => 'wpkites_plus_news_callback',
            'type' => 'select',
            'choices' => array(                
                1 => __('Carousel Style', 'wpkites-plus'),
                2 => __('List Style', 'wpkites-plus'),
                3 => __('Masonry Style', 'wpkites-plus'),
                4 => __('Grid Style', 'wpkites-plus'),
                5 => __('Switcher Style', 'wpkites-plus'),
            )
));

/* * ****************** Blog Content ****************************** */

$wp_customize->add_setting('wpkites_homeblog_layout',
        array(
            'default' => 4,
            'sanitize_callback' => 'wpkites_sanitize_select'
        )
);

$wp_customize->add_control('wpkites_homeblog_layout',
        array(
            'label' => esc_html__('Column Layout', 'wpkites-plus'),
            'section' => 'wpkites_latest_news_section',
            'type' => 'select',
            'active_callback' => 'wpkites_column_callback',
            'active_callback' => function($control) {
                return (
                        wpkites_plus_news_callback($control) &&
                        wpkites_column_callback($control)
                        );
            },
            'choices' => array(
                6 => '2 ' . esc_html__('Column', 'wpkites-plus'),
                4 => '3 ' . esc_html__('Column', 'wpkites-plus'),
            )
        )
);

$wp_customize->add_setting('wpkites_homeblog_counts',
        array(
            'default' => 4,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'wpkites_sanitize_number_range',
        )
);
$wp_customize->add_control('wpkites_homeblog_counts',
        array(
            'label' => esc_html__('Number of Posts', 'wpkites-plus'),
            'section' => 'wpkites_latest_news_section',
            'type' => 'number',
            'input_attrs' => array('min' => 2, 'max' => 20, 'step' => 1, 'style' => 'width: 100%;'),
            'active_callback' => 'wpkites_plus_news_callback'
        )
);


// Read More Button
$wp_customize->add_setting('home_news_button_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Read More', 'wpkites-plus'),
    'sanitize_callback' => 'wpkites_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_button_title', array(
    'label' => __('Read More Text', 'wpkites-plus'),
    'section' => 'wpkites_latest_news_section',
    'type' => 'text',
    'active_callback' => 'wpkites_plus_news_callback'
));

// enable/disable meta section 
$wp_customize->add_setting(
        'home_meta_section_settings',
        array('capability' => 'edit_theme_options',
            'default' => true,
));
$wp_customize->add_control(
        'home_meta_section_settings',
        array(
            'type' => 'checkbox',
            'label' => __('Enable post meta in blog section', 'wpkites-plus'),
            'section' => 'wpkites_latest_news_section',
            'active_callback' => 'wpkites_plus_news_callback'
        )
);

//Navigation Type
$wp_customize->add_setting('news_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('news_nav_style', array(
    'label' => __('Navigation Style', 'wpkites-plus'),
    'section' => 'wpkites_latest_news_section',
    'type' => 'radio',
    'choices' => array(
        'bullets' => __('Bullets', 'wpkites-plus'),
        'navigation' => __('Navigation', 'wpkites-plus'),
        'both' => __('Both', 'wpkites-plus'),
    ),
    'active_callback' => 'wpkites_plus_news_callback'
));

// animation speed
$wp_customize->add_setting('newz_animation_speed', array('default' => 3000));
$wp_customize->add_control('newz_animation_speed',
        array(
            'label' => __('Animation Speed', 'wpkites-plus'),
            'section' => 'wpkites_latest_news_section',
            'type' => 'select',
            'choices' => array(
                2000 => '2.0',
                3000 => '3.0',
                4000 => '4.0',
                5000 => '5.0',
                6000 => '6.0',
            ),
            'active_callback' => 'wpkites_plus_news_callback'
));

// smooth speed
$wp_customize->add_setting('newz_smooth_speed', array('default' => 1000));
$wp_customize->add_control('newz_smooth_speed',
        array(
            'label' => __('Smooth Speed', 'wpkites-plus'),
            'section' => 'wpkites_latest_news_section',
            'type' => 'select',
            'active_callback' => 'wpkites_plus_news_callback',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0')
));

/**
 * Add selective refresh for Front page news section controls.
 */
$wp_customize->selective_refresh->add_partial('home_news_section_title', array(
    'selector' => '.home-blog .section-header h2',
    'settings' => 'home_news_section_title',
    'render_callback' => 'wpkites_plus_home_news_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_section_discription', array(
    'selector' => '.home-blog .section-header h5',
    'settings' => 'home_news_section_discription',
    'render_callback' => 'wpkites_plus_home_news_section_discription_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_blog_more_btn', array(
    'selector' => '.home-blog .business-view-more-post',
    'settings' => 'home_blog_more_btn',
    'render_callback' => 'wpkites_plus_home_blog_more_btn_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_button_title', array(
    'selector' => '.home-blog a.more-link',
    'settings' => 'home_news_button_title',
    'render_callback' => 'wpkites_plus_home_news_button_title_render_callback',
));

function wpkites_plus_home_news_section_title_render_callback() {
    return get_theme_mod('home_news_section_title');
}

function wpkites_plus_home_news_section_discription_render_callback() {
    return get_theme_mod('home_news_section_discription');
}

function wpkites_plus_home_blog_more_btn_render_callback() {
    return get_theme_mod('home_blog_more_btn');
}

function wpkites_plus_home_news_button_title_render_callback() {
    return get_theme_mod('home_news_button_title');
}

function wpkites_plus_column_callback($control) {
    if ($control->manager->get_setting('home_news_design_layout')->value() == '2') {
        return false;
    }
    return true;
}