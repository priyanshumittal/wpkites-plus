<?php

class wpkites_plus_pagination {

    function wpkites_plus_page() {
        global $post;
        global $wp_query, $wp_rewrite, $loop, $template;
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        if(basename($template)!='search.php') {                    
            if ($wp_query->max_num_pages == 0) {
               $wp_query = $loop;
            }
        }
        $faPrevious = (!is_rtl()) ? "fa fa-long-arrow-left" : "fa fa-long-arrow-right";
        $faNext = (!is_rtl()) ? "fa fa-long-arrow-right" : "fa fa-long-arrow-left";


        if (!is_rtl()) {
            the_posts_pagination(array(
                'prev_text' => '<i class="fa fa-long-arrow-left"></i>',
                'next_text' => '<i class="fa fa-long-arrow-right"></i>',
           ));
        } else {
            the_posts_pagination(array(
                'prev_text' => '<i class="fa fa-long-arrow-right"></i>',
                'next_text' => '<i class="fa fa-long-arrow-left"></i>',
            ));
        }
    }

}

class WPKites_Portfolio_Page {

    function WPKites_Port_Page($curpage, $post_type_data, $total, $posts_per_page) {
        $faPrevious = (!is_rtl()) ? "fa fa-long-arrow-left" : "fa fa-long-arrow-right";
        $faNext = (!is_rtl()) ? "fa fa-long-arrow-right" : "fa fa-long-arrow-left";
        $count = $total - $posts_per_page;
        if ($count <= 0) {
            return;
        } else {
            ?>
            <div class="pagination">
                 <?php if ($curpage != 1) {
                        echo '<a class="previous" alt="'.esc_attr__('Previous','wpkites-plus').'" href="' . get_pagenum_link(($curpage - 1 > 0 ? $curpage - 1 : 1)) . '" ><i class="'.$faPrevious.'"></i></a>';
                    }
                    ?>
                    <?php
                    for ($i = 1; $i <= $post_type_data->max_num_pages; $i++) {
                        echo '<a class="' . ($i == $curpage ? 'active ' : '') . '" href="' . get_pagenum_link($i) . '" alt="'.$i.'">' . $i . '</a>';
                    }
                    if ($i - 1 != $curpage) {
                        echo '<a class="next" alt="'.esc_attr__('next','wpkites-plus').'" href="' . get_pagenum_link(($curpage + 1 <= $post_type_data->max_num_pages ? $curpage + 1 : $post_type_data->max_num_pages)) . '"><i class="'.$faNext.'"></i></a>';
                    }
                    ?>
               
            </div>
        <?php
        }
    }

}