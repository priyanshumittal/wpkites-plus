<?php
// Typography
$enable_top_widget_typography = get_theme_mod('enable_top_widget_typography', false);
$enable_header_typography = get_theme_mod('enable_header_typography', false);
$enable_banner_typography = get_theme_mod('enable_banner_typography', false);
$enable_section_title_typography = get_theme_mod('enable_section_title_typography', false);
$enable_slider_title_typography = get_theme_mod('enable_slider_title_typography', false);
$enable_content_typography = get_theme_mod('enable_content_typography', false);
$enable_post_typography = get_theme_mod('enable_post_typography', false);
$enable_meta_typography = get_theme_mod('enable_meta_typography', false);
$enable_shop_page_typography = get_theme_mod('enable_shop_page_typography', false);
$enable_sidebar_typography = get_theme_mod('enable_sidebar_typography', false);
$enable_footer_bar_typography = get_theme_mod('enable_footer_bar_typography', false);
$enable_footer_widget_typography = get_theme_mod('enable_footer_widget_typography', false);
$enable_after_btn_typography= get_theme_mod('enable_after_btn_typography',false);


/* Top Widget Area */
if ($enable_top_widget_typography == true) {
    ?>
    <style>
        body .header-sidebar .widgettitle, body .header-sidebar .widget h1, body .header-sidebar .widget h2, body .header-sidebar .widget h3, body .header-sidebar .widget h4, body .header-sidebar .widget h5, body .header-sidebar .widget h6 { 
            font-size:<?php echo get_theme_mod('topbar_widget_title_fontsize', '30') . 'px'; ?>;
            font-weight:<?php echo get_theme_mod('topbar_widget_title_fontweight', '700'); ?>;
            font-family:<?php echo get_theme_mod('topbar_widget_title_fontfamily', 'Open Sans'); ?>;
            font-style:<?php echo get_theme_mod('topbar_widget_title_fontstyle', 'normal'); ?>;
            text-transform:<?php echo get_theme_mod('topbar_widget_title_text_transform', 'default'); ?>;
            margin-bottom :0px;
            line-height:<?php echo get_theme_mod('topbar_widget_title_line_height','45').'px'; ?>;
        }
        body .head-contact-info li, body .head-contact-info li a, body .header-sidebar .custom-social-icons li > a, body .header-sidebar p, body .header-sidebar a, body .header-sidebar em { 
            font-size:<?php echo get_theme_mod('top_widget_typography_fontsize', '15') . 'px'; ?>;
            font-weight:<?php echo get_theme_mod('top_widget_typography_fontweight', '400'); ?>;
            font-family:<?php echo get_theme_mod('top_widget_typography_fontfamily', 'Open Sans'); ?>;
            font-style:<?php echo get_theme_mod('top_widget_typography_fontstyle', 'normal'); ?>;
            text-transform:<?php echo get_theme_mod('top_widget_typography_text_transform', 'default'); ?>;
            line-height:<?php echo get_theme_mod('topbar_widget_content_line_height','30').'px'; ?>;
        }

        body .header-sidebar .custom-social-icons li > a {
            width: <?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?>;
            height: <?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?>;
            line-height:<?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?>;
        }
    </style>
    <?php
}


/* Site title and tagline */
if ($enable_header_typography == true) {
    ?>
    <style>
        body .site-title {
            font-family:<?php echo get_theme_mod('site_title_fontfamily', 'Open Sans'); ?>;
            font-size:<?php echo get_theme_mod('site_title_fontsize', '36') . 'px'; ?>;
            font-weight:<?php echo get_theme_mod('site_title_fontweight', '700'); ?>;
            font-style:<?php echo get_theme_mod('site_title_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('site_title_text_transform', 'default'); ?> ;
            line-height:<?php echo get_theme_mod('site_title_line_height','39').'px'; ?> ;
        }

        body .site-description {
            font-family:<?php echo get_theme_mod('site_tagline_fontfamily', 'Open Sans'); ?> ;
            font-size:<?php echo get_theme_mod('site_tagline_fontsize', '20') . 'px'; ?>;
            font-weight:<?php echo get_theme_mod('site_tagline_fontweight', '400'); ?>;
            font-style:<?php echo get_theme_mod('site_tagline_fontstyle', 'normal'); ?>;
            text-transform:<?php echo get_theme_mod('site_tagline_text_transform', 'default'); ?>;
            line-height:<?php echo get_theme_mod('site_tagline_line_height','30').'px'; ?> ;
        }

        body .navbar .nav > li > a:not(.wpkites_header_btn), body .navbar .nav .nav-item .nav-link {
            font-family:<?php echo get_theme_mod('menu_title_fontfamily', 'Open Sans'); ?> ;
            font-size:<?php echo get_theme_mod('menu_title_fontsize', '15') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('menu_title_fontweight', '600'); ?> ;
            font-style:<?php echo get_theme_mod('menu_title_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('menu_title_text_transform', 'default'); ?> ;
            line-height:<?php echo get_theme_mod('menu_line_height','30').'px'; ?> ;
        }

        body .dropdown-menu .dropdown-item, body .navbar .nav .nav-item .dropdown-item {
            font-family:<?php echo get_theme_mod('submenu_title_fontfamily', 'Open Sans'); ?>;
            font-size:<?php echo get_theme_mod('submenu_title_fontsize', '15') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('submenu_title_fontweight', '400'); ?> ;
            font-style:<?php echo get_theme_mod('submenu_title_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('submenu_title_text_transform', 'default'); ?> ;
            line-height:<?php echo get_theme_mod('submenu_line_height','30').'px'; ?> ;
        }
    </style>
    <?php
}

/* After Title */
if($enable_after_btn_typography == true)
{
?>
<style>
body .btn-style-one span {
    font-family:<?php echo get_theme_mod('after_btn_fontfamily','Open Sans'); ?>;
    font-size:<?php echo get_theme_mod('after_btn_fontsize','15').'px'; ?> ;
    line-height:<?php echo get_theme_mod('after_btn_lheight','1').'px'; ?> ;
    font-weight:<?php echo get_theme_mod('after_btn_fontweight','600'); ?> ;
    font-style:<?php echo get_theme_mod('after_btn_fontstyle','normal'); ?> ;
    text-transform:<?php echo get_theme_mod('after_btn_text_transform','default'); ?> ;
}
</style>
<?php } 


/* Banner Title */
if ($enable_banner_typography == true) {
    ?>
    <style>
        body .page-title-section .page-title :is(span,h1,h2,h3,h4,h5,h6,p,div):not(.page-title h3.theme-dtl) {
            font-family:<?php echo get_theme_mod('banner_title_fontfamily', 'Open Sans'); ?> ;
            font-size:<?php echo get_theme_mod('banner_title_fontsize', '32') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('banner_title_fontweight', '700'); ?> ;
            font-style:<?php echo get_theme_mod('banner_title_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('banner_title_text_transform', 'default'); ?> ;
            line-height:<?php echo get_theme_mod('banner_titl_line_height','41').'px'; ?> ;
        }

        /* Breadcrumb Title */
        body .page-breadcrumb a, body .page-breadcrumb span, body .page-breadcrumb, .rank-math-breadcrumb span, .rank-math-breadcrumb p, .navxt-breadcrumb a , .navxt-breadcrumb span,.navxt-breadcrumb {
            font-family:<?php echo get_theme_mod('breadcrumb_title_fontfamily', 'Open Sans'); ?> ;
            font-size:<?php echo get_theme_mod('breadcrumb_title_fontsize', '15') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('breadcrumb_title_fontweight', '400'); ?> ;
            font-style:<?php echo get_theme_mod('breadcrumb_title_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('breadcrumb_title_text_transform', 'default'); ?> ;
            line-height:<?php echo get_theme_mod('breadcrumb_line_height','30').'px'; ?> ;
        }
    </style>
    <?php
}


/* Section Title */
if ($enable_section_title_typography == true) {
    ?>
    <style>
        body .section-header  h2:not(.cta .title), body .contact .section-header h2, body .funfact h2.funfact-title {
            font-size:<?php echo get_theme_mod('section_title_fontsize', '36') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('section_title_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('section_title_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('section_title_fontstyle', 'Normal'); ?> ;
            text-transform:<?php echo get_theme_mod('section_title_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('section_title_line_height','54')).'px'; ?> ;
        }

        body .section-header .section-subtitle, body .testimonial .section-header p {
            font-size:<?php echo get_theme_mod('section_subtitle_fontsize', '36') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('section_subtitle_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('section_subtitle_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('section_subtitle_fontstyle', 'Normal'); ?> ;
            text-transform:<?php echo get_theme_mod('section_subtitle_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('section_description_line_height','30')).'px'; ?> ;
    }
    </style>
    <?php
}


/* Slider Title */
if ($enable_slider_title_typography == true) {
    ?>
    <style>
        body .slider-caption h2.title  {
            font-size:<?php echo get_theme_mod('slider_title_fontsize', '50') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('slider_title_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('slider_title_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('slider_title_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('slider_title_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('slider_line_height','85')).'px'; ?> ;
        }
    </style>
    <?php
}


/* Content */
if ($enable_content_typography == true) {
    ?>
    <style>
        /* Heading H1 */
        body .about-section h1, body:not(.woocommerce-page) .entry-content h1, body .services h1, body .contact h1, body .error-page h1, body .cta_main h1, body .page-stretched-template h1 {
            font-size:<?php echo get_theme_mod('h1_typography_fontsize', '36') . 'px'; ?> ;
            line-height:<?php echo get_theme_mod('h1_typography_fontsize', '36') + 5 . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('h1_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('h1_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('h1_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('h1_typography_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('h1_line_height','54')).'px'; ?> ;
        }

        /* Heading H2 */
        body:not(.woocommerce-page) .entry-content h2, body .about h2, body .contact h2, body .cta-2 h2, body .section-space.abou-section h2, body .section-header h2.counter-value, body.about-header h2, body .cta_main h2, body .page-stretched-template h2 {
            font-size:<?php echo get_theme_mod('h2_typography_fontsize', '30') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('h2_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('h2_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('h2_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('h2_typography_text_transform', 'default'); ?> ;
           line-height:<?php echo esc_html(get_theme_mod('h2_line_height','45')).'px'; ?> ;
    
        }
        body .error-page h2 {
            font-size:<?php echo get_theme_mod('h2_typography_fontsize', '30') . 'px'; ?> ;
            line-height:<?php echo get_theme_mod('h2_typography_fontsize', '30') + 45 . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('h2_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('h2_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('h2_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('h2_typography_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('h2_line_height','45')).'px'; ?> ;    
        }

        /* Heading H3 */
        body:not(.woocommerce-page) .entry-content h3, body .related-posts h3, body .about-section h3, body .services h3, body .contact h3, body .contact-form-map .title h3, body .section-space .about-section h3, body .comment-form .comment-respond h3, body .home-blog .entry-header h3.entry-title a, body .page-stretched-template h3 {
            font-size:<?php echo get_theme_mod('h3_typography_fontsize', '24') . 'px'; ?> ;
            line-height:<?php echo get_theme_mod('h3_typography_fontsize', '24') + 5 . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('h3_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('h3_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('h3_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('h3_typography_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('h3_line_height','36')).'px'; ?> ;
    
        }
        body .comment-title h3{
            font-size:<?php echo get_theme_mod('h3_typography_fontsize', '24') + 4 . 'px'; ?> ;
            line-height:<?php echo get_theme_mod('h3_typography_fontsize', '24') + 4 + 5 . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('h3_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('h3_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('h3_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('h3_typography_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('h3_line_height','36')).'px'; ?> ;
    
        }

        /* Heading H4 */
        body h4:not(.blog h4.blog-title,.blog h4.entry-title, .footer-sidebar .widget.widget_block h4, .sidebar .widget.widget_block h4), body .blog h4 a.home-blog-title, body .entry-content h4, body .about-header h4:not(.blog-title), body .team-grid h4, body .entry-header h4:not(.entry-title) a:not(.blog-title), body .contact-widget h4, body .about-section h4, body .testimonial .testmonial-block .name, body .services h4, body .contact h4, body .portfolio h4, body .section-space .about-sections h4, body #related-posts-carousel .entry-header h4 a:not(.blog-title), body .blog-author h4.name, body .error-page h4, body .team .team-grid h4.name, body .page-stretched-template h4 {

            font-size:<?php echo get_theme_mod('h4_typography_fontsize', '20') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('h4_line_height','30')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('h4_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('h4_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('h4_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('h4_typography_text_transform', 'default'); ?> ;
        }

        /* Heading H5 */
        body .product-price h5, body .blog-author h5, body .comment-detail h5, body .entry-content h5, body .about h5, body .contact h5, body .section-space .about-sections h5, body .contact-info h5, body .about-section h5, body .portfolio .portfolio-thumbnail h5.post_cats, body .page-stretched-template h5 {
            font-size:<?php echo get_theme_mod('h5_typography_fontsize', '18') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('h5_line_height','24')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('h5_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('h5_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('h5_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('h5_typography_text_transform', 'default'); ?> ;
        }

        /* Heading H6 */
        body h6:not(.footer-sidebar .widget.widget_block h6, .sidebar .widget.widget_block h6), body .entry-content h6, body .about-sections h6, body .services h6, body .contact h6, body .section-space .about-section h6, body .page-stretched-template h6 {
            font-size:<?php echo get_theme_mod('h6_typography_fontsize', '14') . 'px'; ?> ;
             line-height:<?php echo esc_html(get_theme_mod('h6_line_height','21')).'px'; ?> ;
           font-weight:<?php echo get_theme_mod('h6_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('h6_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('h6_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('h6_typography_text_transform', 'default'); ?> ;
        }

        /* Paragraph */
        body .entry-content p:not(.testimonial .testmonial-block .designation), body .about-content p, body .funfact p, body .woocommerce-product-details__short-description p, body .wpcf7 .wpcf7-form p label, body .about-section p:not(.about-section p.section-subtitle), body .entry-content li, body .contact address, body .contact p, body .services p, body .contact p, body .sponsors p, body .cta-2 p, body .funfact p.description.font-weight-normal, body .testimonial.testimonial-1 .testimonial-block .inner-box .content-column .entry-content p, body .page-stretched-template p {
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400'); ?> ;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('p_typography_text_transform', 'default'); ?> ;
        }
        body .funfact p.description.font-weight-normal {
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('p_typography_text_transform', 'default'); ?> !important;
        }
        body .slider-caption p, body p:not(.site-description,.footer-sidebar p,.sidebar p,.testimonial p, .about-section p.section-subtitle, .site-info p.copyright-section){
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400'); ?> ;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('_typography_text_transform', 'default'); ?> ;
        }
        body .portfolio .tab a, body .portfolio li a {
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400') + 200; ?> ;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('p_typography_text_transform', 'default'); ?> ;
        }


        /* Button Text */
        body .btn-combo a, body .mx-auto a, body .pt-3 a, body .wpcf7-form .wpcf7-submit,  body .woocommerce .button, body .btn-default, body .btn-light, body .sidebar .woocommerce button[type="submit"], body .site-footer .woocommerce button[type="submit"], body .sidebar .widget .search-submit, body #commentform input[type="submit"], body .search-submit, body .wp-block-button__link, body .more-link, body .woocommerce .added_to_cart, body .search-form input[type="submit"], body .wp-block-search .wp-block-search__button, body .woocommerce ul.products li.product .button, body .woocommerce div.product form.cart .button, body.woocommerce-cart .wc-proceed-to-checkout a.checkout-button { 
            font-size:<?php echo get_theme_mod('button_text_typography_fontsize', '15') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('button_line_height','30')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('button_text_typography_fontweight', '600'); ?> ;
            font-family:<?php echo get_theme_mod('button_text_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('button_text_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('button_text_typography_text_transform', 'default'); ?> ;
        }
    </style>
    <?php
}

/* Blog / Archive / Single Post */
if ($enable_post_typography == true) {
    ?>
    <style>
        body .entry-header h4.blog-title, body .entry-header h4 a.blog-title, body #related-posts-carousel .entry-header h4 a.blog-title, body .entry-header h2 a, body .entry-header h3.entry-title a:not(.home-blog-title), body .blog h4.entry-title a:not(.home-blog-title){
            font-size:<?php echo get_theme_mod('post-title_fontsize', '36') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('post-title_line_height','54')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('post-title_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('post-title_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('post-title_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('post-title_text_transform', 'default'); ?> ;
        }
    </style>
    <?php
}

/* Post Meta */
if ($enable_meta_typography == true) {
    ?>
    <style>
        body .blog .entry-meta a, body .entry-date a {
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?>;
            line-height:<?php echo esc_html(get_theme_mod('meta_line_height','28')).'px'; ?>;
            font-weight:<?php echo get_theme_mod('meta_fontweight', '500'); ?>;
            font-family:<?php echo get_theme_mod('meta_fontfamily', 'Open Sans'); ?>;
            font-style:<?php echo get_theme_mod('meta_fontstyle', 'normal'); ?>;
            text-transform:<?php echo get_theme_mod('meta_text_transform', 'default'); ?>;
        }
        body .blog .entry-meta a .author
        {
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?>;
        }
        body .blog .entry-meta i {
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?>;
            line-height:<?php echo esc_html(get_theme_mod('meta_line_height','28')).'px'; ?>;
            
        }

        body .blog .date{
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?>;
            line-height:<?php echo esc_html(get_theme_mod('meta_line_height','28')).'px'; ?>;
            font-family:<?php echo get_theme_mod('meta_fontfamily', 'Open Sans'); ?>;
            font-weight:<?php echo get_theme_mod('meta_fontweight', '500'); ?>;
            font-style:<?php echo get_theme_mod('meta_fontstyle', 'normal'); ?>;
            text-transform:<?php echo get_theme_mod('meta_text_transform', 'default'); ?>;
        }
        
    </style>
    <?php
}


/* Shop Page */
if ($enable_shop_page_typography == true) {
    ?>
    <style>
        /* Heading H2 */
        body.woocommerce .products h2, body .woocommerce .cart_totals h2, body .woocommerce-Tabs-panel h2, body.woocommerce .cross-sells h2, body.woocommerce div.product h2.product_title, body.woocommerce h2:NOT(.site-title, .sidebar h2), body.woocommerce ul.products li.product .woocommerce-loop-product__title, body .woocommerce ul.products li.product h2.woocommerce-loop-product__title{
            font-size:<?php echo get_theme_mod('shop_h2_typography_fontsize', '18') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('shop_h2_line_height','30')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('shop_h2_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('shop_h2_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('shop_h2_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('shop_h2_typography_text_transform', 'default'); ?> ;
        }

        /* Heading H3 */
        body .woocommerce .checkout h3:not(footer h3,.sidebar h3), body.woocommerce h3:not(footer h3,.sidebar h3,.page-title-section .page-title h3.theme-dtl) {
            font-size:<?php echo get_theme_mod('shop_h3_typography_fontsize', '24') . 'px'; ?> ;
            line-height:<?php echo esc_html(get_theme_mod('shop_h3_line_height','36')).'px'; ?> ;
            font-weight:<?php echo get_theme_mod('shop_h3_typography_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('shop_h3_typography_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('shop_h3_typography_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('shop_h3_typography_text_transform', 'default'); ?> ;
        }
    </style>
    <?php
}


/* Sidebar widgets */
if ($enable_sidebar_typography == true) {
    ?>
    <style>
        body .sidebar .widget-title, body .sidebar .widget.widget_block :is(h1,h2,h3,h4,h5,h6), body .sidebar .wp-block-search .wp-block-search__label{
            font-size:<?php echo get_theme_mod('sidebar_fontsize', '24') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('sidebar_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('sidebar_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('sidebar_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('sidebar_text_transform', 'default'); ?> ;
        line-height:<?php echo esc_html(get_theme_mod('sidebar_line_height','36')).'px'; ?> ;
        }
        /* Sidebar Widget Content */
        body .sidebar .widget_recent_entries a, body .sidebar a, body .sidebar p, body .sidebar .wp-block-latest-posts__post-excerpt, body .sidebar .wp-block-latest-posts__post-full-content, body .sidebar .wp-block-latest-posts__post-author, body .sidebar .wp-block-latest-posts__post-date, body .woocommerce ul.product_list_widget li a, body .sidebar ul li a {
            font-size:<?php echo get_theme_mod('sidebar_widget_content_fontsize', '15') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('sidebar_widget_content_fontweight', '600'); ?> ;
            font-family:<?php echo get_theme_mod('sidebar_widget_content_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('sidebar_widget_content_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('sidebar_widget_content_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('sidebar_widget_content_line_height','30')).'px'; ?> ;
        }
        body .sidebar .widget_block.widget a.tag-cloud-link {
            font-size:<?php echo get_theme_mod('sidebar_widget_content_fontsize', '15') . 'px'; ?> !important;
        }
    </style>
    <?php
}


/* Footer Bar */
if ($enable_footer_bar_typography == true) {
    ?>
    <style>
        body .site-footer .site-info * {
            font-size:<?php echo get_theme_mod('footer_bar_fontsize', '15') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('footer_bar_fontweight', '400'); ?> ;
            font-family:<?php echo get_theme_mod('footer_bar_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('footer_bar_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('footer_bar_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('fbar_line_height','28')).'px'; ?> ;
        }
    </style>
    <?php
}


/* Footer Widget */
if ($enable_footer_widget_typography == true) {
    ?>
    <style>
        /* Footer Widget Title */
        body .site-footer .footer-typo .widget-title, body .site-footer .footer-typo .wp-block-search__label, body .footer-sidebar .widget.widget_block :is(h1,h2,h3,h4,h5,h6){
            font-size:<?php echo get_theme_mod('footer_widget_title_fontsize', '24') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('footer_widget_title_fontweight', '700'); ?> ;
            font-family:<?php echo get_theme_mod('footer_widget_title_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('footer_widget_title_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('footer_widget_title_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('footer_widget_title_line_height','36')).'px'; ?> ;
        }
        /* Footer Widget Content */
        body .footer-sidebar .widget_recent_entries a, body .footer-sidebar.footer-typo a, body .footer-sidebar.footer-typo p, body .footer-sidebar.footer-typo .textwidget, body .footer-sidebar  .head-contact-info li, body .footer-sidebar .head-contact-info li a, body .footer-sidebar em, body .footer-sidebar address, body .footer-sidebar .wp-block-latest-posts__post-excerpt, body .footer-sidebar .wp-block-latest-posts__post-full-content, body .footer-sidebar .wp-block-latest-posts__post-author, body .footer-sidebar .wp-block-latest-posts__post-date {
            font-size:<?php echo get_theme_mod('footer_widget_content_fontsize', '15') . 'px'; ?> ;
            font-weight:<?php echo get_theme_mod('footer_widget_content_fontweight', '600'); ?> ;
            font-family:<?php echo get_theme_mod('footer_widget_content_fontfamily', 'Open Sans'); ?> ;
            font-style:<?php echo get_theme_mod('footer_widget_content_fontstyle', 'normal'); ?> ;
            text-transform:<?php echo get_theme_mod('footer_widget_content_text_transform', 'default'); ?> ;
            line-height:<?php echo esc_html(get_theme_mod('footer_widget_content_line_height','30')).'px'; ?> ;
        }
        body .site-footer .widget_block.widget a.tag-cloud-link {
            font-size:<?php echo get_theme_mod('footer_widget_content_fontsize', '15') . 'px'; ?> !important;
        }
    </style>
<?php } ?>



<?php
// -----------------Colors & Background----------------------
?>



<style>
    <?php 
    if (get_theme_mod('header_clr_enable', false) == true) : ?>
        /* Header Background Color*/
        
        body .five .layout5.header-logo, body .six .navbar-header.index6, body .seven .layout3.header-sidebar, body .layout3.header-sidebar .bottom-header {
            background: <?php echo get_theme_mod('header_background_color', '#000000'); ?>;  
        }
        /* Site Title & Tagline */
        body .site-title a.site-title-name, body .navbar4 .custom-logo-link-url .site-title a, body .navbar-header.index6 .custom-logo-link-url .site-title-name {
            color: <?php echo get_theme_mod('site_title_link_color', '#ffffff'); ?>;
        }
        body .site-title a.site-title-name:hover, body .navbar4 .custom-logo-link-url .site-title a:hover, body .navbar-header.index6 .custom-logo-link-url .site-title-name:hover {
            color: <?php echo get_theme_mod('site_title_link_hover_color', '#926AA6'); ?>;
        }
        body p.site-description, body .navbar-header.index6 .site-description {
            color: <?php echo get_theme_mod('site_tagline_text_color', '#acacac'); ?> ;
        }
    <?php endif; ?>

    /* Sticky Header Color shceme */
    <?php if (get_theme_mod('sticky_header_clr_enable', false) == true) : ?>
        body .header-sticky.stickymenu1, body .header-sticky.stickymenu, body .header-sticky.shrink1, body .five.stickymenu .navbar2, body .six.stickymenu .index5, body .five.shrink1 .header-logo.layout5, body .six.shrink1 .navbar-header.index6, body .five.stickymenu .header-logo.layout5, body .six.stickymenu .navbar-header.index6 {
            background-color: <?php echo get_theme_mod('sticky_header_bg_color', '#000000'); ?>;
        }
        body .five.stickymenu1 .layout5.header-logo
        {
            background: <?php echo get_theme_mod('sticky_header_bg_color', '#000000'); ?>;
        }
        body .six.stickymenu1 .navbar-header.index6, body .seven.stickymenu .header-sidebar.layout3 .bottom-header, body .seven.shrink1 .header-sidebar.layout3 .bottom-header, body .seven.stickymenu .header-sidebar.layout3 .bottom-header {
            background: <?php echo get_theme_mod('sticky_header_bg_color', '#000000'); ?>;
        }
        body .header-sticky.stickymenu1 .site-title a, body .header-sticky.stickymenu .site-title a, body .header-sticky.shrink1 .site-title a {
            color: <?php echo get_theme_mod('sticky_header_title_color', '#ffffff'); ?> ;
        }
        body .header-sticky.stickymenu .navbar.navbar6 .nav .nav-item:hover .nav-link, body .header-sticky.stickymenu .navbar.navbar6 .nav .nav-item .nav-link:focus {
             color: <?php echo get_theme_mod('sticky_header_menus_link_active_color', '#926AA6'); ?> ;
        }
        body .header-sticky.stickymenu .navbar-nav .current_page_ancestor .nav-link, 
        body .header-sticky.stickymenu .navbar-nav .sm-nowrap > li.current_page_parent > .dropdown-item, 
        body .header-sticky.stickymenu .navbar .nav.navbar-nav .sm-nowrap > li.current_page_parent > .dropdown-item, 
        body .header-sticky.stickymenu .navbar.navbar6 .nav .nav-item.active .nav-link {
            color: <?php echo get_theme_mod('sticky_header_menus_link_active_color', '#926AA6'); ?> !important;
        }
        body .header-sticky.stickymenu1 .site-description, body .header-sticky.stickymenu .site-description, body .header-sticky.shrink1 .site-description {
            color: <?php echo get_theme_mod('sticky_header_subtitle_color', '#acacac'); ?> ;
        }
        body .header-sticky.stickymenu1 .nav .nav-item .nav-link, body .header-sticky.stickymenu .nav .nav-item .nav-link, body .header-sticky.shrink1 .nav .nav-item .nav-link, body .navbar.custom.header-sticky.stickymenu1 .nav .nav-item .nav-link, body .navbar.custom.header-sticky.stickymenu .nav .nav-item .nav-link, body .navbar.custom.header-sticky.shrink1 .nav .nav-item .nav-link, body .header-sticky.stickymenu .navbar5.navbar .nav .nav-item .nav-link, body .header-sticky.stickymenu1 .navbar5.navbar .nav .nav-item .nav-link, body .header-sticky.shrink1 .navbar5.navbar .nav .nav-item .nav-link, body .header-sticky.stickymenu .navbar6.navbar .nav .nav-item .nav-link, body .header-sticky.stickymenu1 .navbar6.navbar .nav .nav-item .nav-link, body .header-sticky.shrink1 .navbar6.navbar .nav .nav-item .nav-link, 
            body .header-sticky.stickymenu .layout3.navbar .nav .nav-item .nav-link, body .header-sticky.stickymenu1 .layout3.navbar .nav .nav-item .nav-link, body .header-sticky.shrink1 .layout3.navbar .nav .nav-item .nav-link {
            color: <?php echo get_theme_mod('sticky_header_menus_link_color', '#ffffff'); ?> ;
        }
        body .header-sticky.stickymenu1 .nav .nav-item:hover .nav-link, body .header-sticky.stickymenu1 .navbar.custom .nav .nav-item.active .nav-link:hover, body .header-sticky.stickymenu .nav .nav-item:hover .nav-link, body .header-sticky.stickymenu .navbar.custom .nav .nav-item.active .nav-link:hover, body .header-sticky.shrink1 .nav .nav-item:hover .nav-link, body .header-sticky.shrink1 .navbar.custom .nav .nav-item.active .nav-link:hover, body .navbar.header-sticky.stickymenu .nav .nav-item:hover .nav-link, body .header-sticky.stickymenu .navbar .nav .nav-item:hover .nav-link, body .header-sticky.stickymenu .navbar .nav .nav-item:hover .nav-link, body .header-sticky.stickymenu .navbar .nav .nav-item:hover .nav-link{
                color: <?php echo get_theme_mod('sticky_header_menus_link_hover_color', '#926AA6'); ?>;
        }
        body .header-sticky.stickymenu.navbar ul li.menu-item a .menu-text:hover:after, body .header-sticky.stickymenu.navbar ul li.menu-item a .menu-text:hover:after, body .header-sticky.stickymenu.navbar .nav li.active .nav-link .menu-text:after, body .header-sticky.stickymenu ul li.menu-item a .menu-text:hover:after, body .header-sticky.stickymenu .navbar ul li.menu-item a .menu-text:hover:after, body .header-sticky.stickymenu  .navbar.custom .nav .nav-item.active .nav-link .menu-text:hover:after, body .header-sticky.stickymenu  .navbar.custom .nav .nav-item.active .nav-link .menu-text:after {
            background-color:<?php echo get_theme_mod('sticky_header_menus_link_hover_color', '#926AA6'); ?> ;
        }

        body .header-sticky.stickymenu .nav .nav-item.active .nav-link, body .header-sticky.stickymenu .navbar-nav .show .dropdown-menu > .active > .menu-text:after, body .navbar.custom.header-sticky.stickymenu1 .nav .nav-item.active .nav-link, body .navbar.custom.header-sticky.stickymenu .nav .nav-item.active .nav-link, body .navbar.custom.header-sticky.shrink1 .nav .nav-item.active .nav-link,body .header-sticky.stickymenu .nav .nav-item.active .nav-link, body .header-sticky.stickymenu  .navbar.custom .nav .nav-item.active .nav-link:hover, body .header-sticky.stickymenu .navbar .nav .nav-item.active .nav-link, body .header-sticky.stickymenu .navbar .nav .nav-item.current_page_ancestor.current_page_parent.nav-item .nav-link, body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active a.dropdown-item, body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu li.active a.dropdown-item, body .header-sticky.stickymenu.navbar .nav .nav-item .dropdown.active > a {
            color: <?php echo get_theme_mod('sticky_header_menus_link_active_color', '#926AA6'); ?>;
        }
        body .header-sticky.stickymenu.navbar .nav li.active .nav-link .menu-text:after, body .header-sticky.stickymenu .navbar .nav li.active .nav-link .menu-text:after {
            background-color: <?php echo get_theme_mod('sticky_header_menus_link_active_color', '#926AA6'); ?>;
        }

        /* Sticky Header Submenus */
        body .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item, body .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-menu, body .header-sticky.stickymenu .nav.navbar-nav .dropdown-item, body .header-sticky.stickymenu .nav.navbar-nav .dropdown-menu, body .header-sticky.shrink1 .nav.navbar-nav .dropdown-item, body .header-sticky.shrink1 .nav.navbar-nav .dropdown-menu, body .header-sticky.stickymenu .layout3.navbar .nav .nav-item .dropdown-menu {
            background-color: <?php echo get_theme_mod('sticky_header_submenus_background_color', '#000000'); ?>;
        }
        body .header-sticky.stickymenu .nav.navbar-nav a.dropdown-item, body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu a.dropdown-item {
            color: <?php echo get_theme_mod('sticky_header_submenus_link_color', '#ffffff'); ?> ;
        }
        body .header-sticky.stickymenu .nav.navbar-nav a.dropdown-item:hover, body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .current_page_item.active a.dropdown-item, body .header-sticky.stickymenu .nav.navbar-nav a.bg-light.dropdown-item, body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active a.dropdown-item, body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu li.active a.dropdown-item, body .header-sticky.stickymenu.navbar .nav .nav-item .dropdown.active > a, body .navbar .nav .nav-item .dropdown:hover > a, body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu a.dropdown-item:hover, body .header-sticky.stickymenu .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark:hover, body .header-sticky.stickymenu .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark { 
            color: <?php echo get_theme_mod('sticky_header_submenus_link_hover_color', '#926AA6'); ?>;
        }
        body .header-sticky.stickymenu.navbar ul.dropdown-menu li.menu-item a .menu-text:hover:after, body .header-sticky.stickymenu ul.dropdown-menu li.menu-item a .menu-text:hover:after {
            background-color: <?php echo get_theme_mod('sticky_header_submenus_link_hover_color', '#926AA6'); ?>;
        }
        body .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item:focus, body .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item:hover, body .header-sticky.stickymenu .nav.navbar-nav .dropdown-item:focus, body .header-sticky.stickymenu .nav.navbar-nav .dropdown-item:hover, body .header-sticky.shrink1 .nav.navbar-nav .dropdown-item:focus, body .header-sticky.shrink1 .nav.navbar-nav .dropdown-item:hover, body .navbar .nav li.active .nav-link .menu-text:after {
            background-color: transparent;
        }
    <?php endif; ?>

    /* Primary Menu */
    <?php if (get_theme_mod('apply_menu_clr_enable', false) == true) : ?>
        body .navbar.custom .nav .nav-item .nav-link, body .navbar .nav .nav-item .nav-link, body .navbar.navbar1 .nav .nav-item .nav-link, body .navbar.navbar4 .nav .nav-item .nav-link, body .navbar5.navbar .nav .nav-item .nav-link, body .navbar.navbar6 .nav .nav-item .nav-link, body .layout3.navbar .nav .nav-item .nav-link {
            color: <?php echo get_theme_mod('menus_link_color', '#ffffff'); ?>;
        }
        body .navbar.navbar6 .nav .nav-item:hover .nav-link, body .navbar.navbar6 .nav .nav-item .nav-link:focus, body .navbar.seven .nav .nav-item.active:hover .nav-link, body .navbar.seven .nav .nav-item.active .nav-link:focus, body .navbar.custom .nav .nav-item.active .nav-link {
             color: <?php echo get_theme_mod('menus_link_active_color', '#926AA6'); ?>;
        }
        body .navbar.custom .nav .nav-item:hover .nav-link, body .navbar .nav .nav-item:hover .nav-link, body .navbar .nav .nav-item.active .nav-link:hover, body .navbar .nav .nav-item:hover .nav-link {
            color: <?php echo get_theme_mod('menus_link_hover_color', '#926AA6'); ?>;
        }
        body .navbar.navbar6 .nav .nav-item .nav-link:hover{
            color: <?php echo get_theme_mod('menus_link_hover_color', '#926AA6'); ?> !important;
        }
        body .nav.navbar-nav a.dropdown-item:hover {
            color: <?php echo get_theme_mod('menus_link_hover_color', '#926AA6'); ?>;
        }
        body .navbar ul li.menu-item a .menu-text:hover:after {
            background: <?php echo get_theme_mod('menus_link_hover_color', '#926AA6'); ?>;
        }
        body .navbar .nav li.active .nav-link .menu-text:after {
            background:<?php echo get_theme_mod('menus_link_hover_color', '#926AA6'); ?>; 
            width: 100%;
        }
        body .navbar.custom .nav .nav-item.active .nav-link, body .navbar .nav .nav-item.active .nav-link, body .navbar .nav .nav-item.current_page_ancestor.current_page_parent.nav-item .nav-link  {
            color: <?php echo get_theme_mod('menus_link_active_color', '#926AA6'); ?>;
        }
        body .navbar-nav .current_page_ancestor .nav-link, body .navbar-nav .sm-nowrap > li.current_page_parent > .dropdown-item, body .navbar .nav.navbar-nav .sm-nowrap > li.current_page_parent > .dropdown-item, body .navbar.navbar6 .nav .nav-item.active .nav-link {
            color: <?php echo get_theme_mod('menus_link_active_color', '#926AA6'); ?> !important;
        }
        
        /* Submenus */
        body .nav.navbar-nav .dropdown-item, body .nav.navbar-nav .dropdown-menu, body .layout3.navbar .nav .nav-item .dropdown-menu {
            background-color: <?php echo get_theme_mod('submenus_background_color', '#000000'); ?>;
        }
        body .nav.navbar-nav a.dropdown-item, body .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu a.dropdown-item, body .layout3.navbar .nav.navbar-nav a.dropdown-item, body .navbar .nav .nav-item .dropdown-item {
            color: <?php echo get_theme_mod('submenus_link_color', '#ffffff'); ?>;
        }
        body a.text-dark.bg-light, body .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark:hover, body .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark {
            color: <?php echo get_theme_mod('submenus_link_color', '#ffffff'); ?> !important;
        }
        body .navbar ul.dropdown-menu li.menu-item a .menu-text:hover:after {
            background: <?php echo get_theme_mod('submenus_link_hover_color', '#926AA6'); ?>;
        }
        body .navbar .nav .nav-item.active li.active a.dropdown-item .nav-link:hover, body .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active a.dropdown-item, body .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu li.active a.dropdown-item, body .navbar .nav .nav-item .dropdown.active > a, body .navbar .nav .dropdown-menu > li.active > a {
            color: <?php echo get_theme_mod('menus_link_active_color', '#926AA6'); ?>;
        }
        body .nav.navbar-nav a.dropdown-item:hover, body .nav.navbar-nav a.bg-light.dropdown-item, body .navbar .nav .nav-item .dropdown:hover > a, body .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu a.dropdown-item:hover, body .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark:hover, body .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark, body .layout3.navbar .nav.navbar-nav a.dropdown-item:hover, body .navbar .nav .nav-item .dropdown-item:hover {
            color: <?php echo get_theme_mod('submenus_link_hover_color', '#926AA6'); ?>;
        }
        body a.text-dark:hover {
            color: <?php echo get_theme_mod('submenus_link_hover_color', '#926AA6'); ?> !important;
        }
        body .nav.navbar-nav .dropdown-item:focus, body .nav.navbar-nav .dropdown-item:hover {
            background-color: transparent;
        }
    <?php endif; ?>

    /* Banner */
    body .page-title-section .page-title :is(span,h1,h2,h3,h4,h5,h6,p,div):not(.page-title h3.theme-dtl), body .page-section-space .page-title.content-area-title :is(span,h1,h2,h3,h4,h5,h6,p,div), body .page-stretched-template .page-title.content-area-title :is(span,h1,h2,h3,h4,h5,h6,p,div), body .section-space .page-title.content-area-title :is(span,h1,h2,h3,h4,h5,h6,p,div) {
        color: <?php echo get_theme_mod('banner_text_color', '#fff'); ?>;
    }

    /* Banner */
    body .page-title-section .page-title h3.theme-dtl, body .page-section-space .page-title h3.theme-dtl, body .page-stretched-template .page-title h3.theme-dtl, body .section-space .page-title h3.theme-dtl {
        color: <?php echo get_theme_mod('home_banner_subtitle_color', '#fff'); ?>;
    }


    /* Breadcrumb */
    <?php
    $enable_brd_link_clr_setting = get_theme_mod('enable_brd_link_clr_setting', false);
    if ($enable_brd_link_clr_setting == true):
        ?>
        body .page-breadcrumb span a, nav.rank-math-breadcrumb a, .navxt-breadcrumb span a {
            color: <?php echo get_theme_mod('breadcrumb_title_link_color', '#ffffff'); ?> ;
        }
        body .page-breadcrumb span a:hover, body nav.rank-math-breadcrumb a:hover, body .navxt-breadcrumb span a:hover {
            color: <?php echo get_theme_mod('breadcrumb_title_link_hover_color', '#926AA6'); ?> ;
        }
    <?php endif; ?>

    /* After Menu Button */
     <?php
    $enable_after_menu_btn_clr_setting=get_theme_mod('enable_after_menu_btn_clr_setting',false);
    if($enable_after_menu_btn_clr_setting==true): ?>
        body #wrapper .main-header-btn a{
            background-color: <?php echo get_theme_mod('after_menu_btn_bg_clr','#000');?>
        }   
        body #wrapper .main-header-btn a {
            color: <?php echo get_theme_mod('after_menu_btn_txt_clr','#926AA6');?>
        }
        body #wrapper .main-header-btn a:hover {
            background-color: <?php echo get_theme_mod('after_menu_btn_hover_clr','#926AA6');?>
        }
        body #wrapper .main-header-btn a:hover {
            color: <?php echo get_theme_mod('after_menu_btn_text_hover_clr','#fff');?>
        }   

    <?php endif;?>  

    /* Content */
    <?php
    $enable_content_link_clr_setting = get_theme_mod('content_clr_enable', false);
    if ($enable_content_link_clr_setting == true) {
        ?>
        body h1:not(.sidebar h1, .header-sidebar .widget.widget_block h1, .site-footer .footer-sidebar h1, .page-title-section .page-title h1), body h1 a {
            color: <?php echo get_theme_mod('h1_color', '#ffffff'); ?> ;
        }
        body .section-header h2:not(.cta_main h2, .testimonial h2, .funfact h2, .sidebar h2), body h2:not(.site-footer h2, .cta_main h2,.testimonial h2, .funfact h2, .sidebar h2, .slider-caption .title, .cta-2 .title, .header-sidebar .widget.widget_block h2, .page-title-section .page-title h2), body h2:not(.testimonial h2, .funfact h2, .sidebar h2, .custom-logo-link-url h2.site-title) a, body .contact-section .contact-info .title, body .contact-section .contact-form .title {
            color: <?php echo get_theme_mod('h2_color', '#ffffff'); ?>;
        }
        body h3:not(.sidebar h3, .header-sidebar .widget.widget_block h3, .site-footer .footer-sidebar h3, .page-title-section .page-title h3.theme-dtl, .page-title h3), body h3 a {
            color: <?php echo get_theme_mod('h3_color', '#ffffff'); ?>;
        }
        body .entry-header h4 > a:not(.rm-h4, body .entry-header h4 > a:hover), body h4:not(.sidebar h4,.funfact-title,.single .blog-title .entry-title,.blog.bg-default .entry-header .entry-title, .header-sidebar .widget.widget_block h4, .page-title-section .page-title h4, .footer-sidebar .widget.widget_block h4), body h4 a:not(.blog.bg-default .entry-header .entry-title a, body h4 > a:hover), body .team .team-grid .name, body .section-space.contact-detail .contact-area h4, body .services h4.entry-title a, body .portfolio .tab-content .portfolio-thumbnail .entry-title a, body .blog .entry-header a.home-blog-title, body .team .name, body .contact-info .title, body .contact .contact-widget .title, body .team3 h4, body .team4 h4{
            color: <?php echo get_theme_mod('h4_color', '#ffffff'); ?>;
        }
        body .blog-author h5, body .comment-detail h5, body h5:not(.sidebar h5, .testimonial .section-header h5.section-subtitle, .header-sidebar .widget.widget_block h5, .page-title-section .page-title h5, .footer-sidebar .widget.widget_block h5), body h5 a, body .blog-author .post-by, body .contact-section .contact-info .subtitle, body .portfolio .portfolio-thumbnail h5 a, body .section-header h5.section-subtitle:not(.testimonial .section-header h5.section-subtitle) {
            color: <?php echo get_theme_mod('h5_color', '#ffffff'); ?>;
        }

        body .product-price h5 > a,
        body .contact-section .contact-info .subtitle,
        body .contact-section .contact-info .contact-widget .title,
        body .contact-section .contact-form .subtitle {
            color: <?php echo get_theme_mod('h5_color', '#ffffff'); ?>;
        }

        body h6:not(.sidebar h6, .header-sidebar .widget.widget_block h6, .page-title-section .page-title h6, .footer-sidebar .widget.widget_block h6), body .section-space.contact-detail .contact-area h6, body h6 a {
            color: <?php echo get_theme_mod('h6_color', '#ffffff'); ?>;
        }
        body p:not(.cta-2 .section-subtitle, .woocommerce-mini-cart__total, .slider-caption .description, .site-description, .testimonial p, .funfact p,.sidebar p,.footer-sidebar.footer-typo p,.site-info  p), body .services3 .post .entry-content p, body .services4 .post .entry-content p, body .contact-section .contact-info .details, body .wpcf7 .wpcf7-form p, body .wpcf7 .wpcf7-form p label {
            color: <?php echo get_theme_mod('p_color', '#acacac'); ?>;
        }
        body .team .team-grid .card-body p { color: <?php echo get_theme_mod('p_color', '#acacac'); ?>; }
    <?php } ?>

    <?php if (get_theme_mod('apply_slider_clr_enable', false) == true): ?> 
        /* Slider Section */
        body .bcslider-section .heading span{
            color: <?php echo get_theme_mod('home_slider_subtitle_color', '#ffffff'); ?>;
        }
        body .bcslider-section .slider-caption h2{
            color: <?php echo get_theme_mod('home_slider_title_color', '#ffffff'); ?>;
        }
        body .bcslider-section .slider-caption .description
        {
            color: <?php echo get_theme_mod('home_slider_description_color', '#ffffff'); ?>;
        }
        body .bcslider-section .btn-small.btn-default {
            border: 1px solid <?php echo get_theme_mod('slider_btn1_color', '#926AA6'); ?>;
            background: <?php echo get_theme_mod('slider_btn1_color', '#926AA6'); ?>;
        }
        body .bcslider-section #slider-carousel .btn-small.btn-default:hover {
            border: 1px solid <?php echo get_theme_mod('slider_btn1_hover_color', '#ffffff'); ?>;
            background: <?php echo get_theme_mod('slider_btn1_hover_color', '#ffffff'); ?>;
        }
    <?php endif;?>
     <?php if (get_theme_mod('slider_btn2_color_enable', false) == true):?> 
    body .bcslider-section .btn-light {
        background: <?php echo get_theme_mod('slider_btn2_color', '#ffffff'); ?>;
        border: 1px solid <?php echo get_theme_mod('slider_btn2_color', '#ffffff'); ?>;
    }

    body .bcslider-section #slider-carousel .btn-light:hover {
        border: 1px solid <?php echo get_theme_mod('slider_btn2_hover_color', '#926AA6'); ?>;
        background: <?php echo get_theme_mod('slider_btn2_hover_color', '#926AA6'); ?>;
    }
    <?php endif; ?>

    /* Testimonial Section */
    <?php if (get_theme_mod('apply_testimonial_clr_enable', false) == true) { ?> 
        body .testimonial .section-title, body .testimonial .section-header .section-title
        {
            color: <?php echo get_theme_mod('home_testi_title_color', '#926AA6'); ?> ;
        }
        body .section-space.testimonial .section-header p, body .testimonial .section-header h5.section-subtitle {
            color: <?php echo get_theme_mod('home_testi_subtitle_color', '#ffffff'); ?> ;
        }
        body .section-space.testimonial .entry-content p, body .testimonial .testimonial-block .inner-box .content-column .entry-content p, body #testimonial-carousel2 .entry-content p, body #testimonial-carousel3 .entry-content p, body #testimonial-carousel4 .entry-content p, body .testimonial.testimonial-1 .testimonial-block .inner-box .content-column .entry-content p {
            color: <?php echo get_theme_mod('testimonial_description_color', '#acacac'); ?> ;
        }
        body .testimonial .testmonial-block .name, body .testimonial .testmonial-block .name a, body .testimonial.testimonial-1 .testimonial-block .inner-box .content-column .author-info .author-name a, body #testimonial-carousel2 .name, body #testimonial-carousel3 .name, body #testimonial-carousel4 .name {
            color: <?php echo get_theme_mod('testi_clients_name_color', '#ffffff'); ?> ;
        }
        body .testimonial .testmonial-block .designation, body .testimonial.testimonial-1 .testimonial-block .inner-box .content-column .author-info .designation, body #testimonial-carousel2 .designation, body #testimonial-carousel3 .designation, body #testimonial-carousel4 .designation {
            color: <?php echo get_theme_mod('testi_clients_designation_color', '#926AA6'); ?> ;
        }
    <?php } else { ?>
        .testimonial .section-title
        {
            color: '#ffffff';
        }

        .testimonial .entry-content .description
        {
            color: '#ffffff';
        }
    <?php } ?>

    /* CTA 1 SECTION */

    <?php if (get_theme_mod('apply_cta1_clr_enable', false) == true): ?>
        body .cta_main .cta_content h2{
            color: <?php echo get_theme_mod('home_cta1_title_color', '#ffffff'); ?>;
        }
        body .cta_main .cta_content .btn-light {
            color: <?php echo get_theme_mod('home_cta1_btn_color', '#ffffff'); ?> ;
        }
        body .cta_main .cta_content .btn-light:hover {
            color: <?php echo get_theme_mod('home_cta1_btn_hover_color', '#926AA6'); ?> ;
        }
        body .cta_content, body .cta_main, body .cta_content .btn-light {
            background-color: <?php echo get_theme_mod('cta1_bg_section_color', '#926AA6'); ?> ;
        }

    <?php endif; ?>

    /* CTA 2 SECTION */
    .cta-2 .title {
        color: #ffffff;
    }
    .cta-2 p {
        color: #ffffff;
    }

    <?php if (get_theme_mod('apply_cta2_clr_enable', false) == true): ?>
        body .cta-2 .title{
            color: <?php echo get_theme_mod('home_cta2_title_color', '#ffffff'); ?>;
        }
        body .cta-2 p.section-subtitle {
            color: <?php echo get_theme_mod('home_cta2_subtitle_color', '#ffffff'); ?>;
        }
        body #cta-video .fa {
            color: <?php echo get_theme_mod('home_cta2_btn1_color', '#fff'); ?>;
        }
    <?php endif; ?>
    
        

    /* Funfact SECTION */
    .funfact .title {
        color: '#ffffff';
    }

    .funfact .funfact-inner .funfact-title, .funfact .funfact-inner .description.text-white{
        color: '#ffffff';
    }
    <?php if (get_theme_mod('apply_funfact_clr_enable', false) == true): ?>

        body .funfact .funfact-inner .funfact-title{
            color: <?php echo get_theme_mod('funfact_count_color', '#ffffff'); ?>;
        }
        body .funfact .description{
            color: <?php echo get_theme_mod('funfact_count_desc_color', '#ffffff'); ?>;
        }
        body .funfact .funfact-icon {
            color: <?php echo get_theme_mod('funfact_count_icon_color', '#ffffff'); ?>;
        }     
    <?php endif; ?>


    /* Blog Page */
    <?php if ((get_theme_mod('apply_blg_clr_enable', false) == true) && (!is_single())): ?> 
        body .standard-view .entry-title a, body .entry-title.template-blog-grid-view a, body .entry-title.template-blog-grid-view-sidebar a, body .entry-title.template-blog-list-view a, body .section-space.blog:NOT(.home-blog) .list-view .entry-title a:not(a.home-blog-title), body .entry-title.blog-masonry-two-col a, body .entry-title.blog-masonry-three-col a, body .entry-title.blog-masonry-four-col a, body .section-space.blog:NOT(.home-blog) .entry-title a:not(a.home-blog-title),body .blog.bg-default .entry-header .entry-title a {
            color: <?php echo get_theme_mod('blog_post_page_title_color', '#ffffff'); ?>;
        }
        body .blog .entry-header .entry-title a:not(a.home-blog-title):hover, body .blog .entry-header .entry-title a:not(a.home-blog-title):hover, body .blog .entry-header .entry-title.template-blog-list-view a:hover {
            color: <?php echo get_theme_mod('blog_post_page_title_hover_color', '#926AA6'); ?>;
        }
        body .blog .entry-header .entry-meta a, body .blog .entry-meta > span a, body .blog .entry-meta a, body .blog .entry-meta a span {
            color: <?php echo get_theme_mod('blog_post_page_meta_link_color', '#ffffff'); ?>;
        }
        body .blog .entry-header .entry-meta a:hover, body .blog .entry-meta a:hover, body .blog .entry-meta .author:hover {
            color: <?php echo get_theme_mod('blog_post_page_meta_link_hover_color', '#926AA6'); ?>;
        }
        body .section-module.blog .entry-meta .cat-links a, body .section-module.blog .standard-view .entry-meta .author a, body .section-module.blog .list-view .entry-meta .author a, body .section-module.blog.grid-view .entry-meta .author a, body .section-module.blog .entry-meta .comment-links a::before, body .entry-meta .posted-on a, body .entry-meta .comment-links a, body .section-module.blog .entry-meta .comment-links a::before {
            color: <?php echo get_theme_mod('blog_post_page_meta_link_color', '#ffffff'); ?>;
        }
        body .section-space.blog .entry-meta .cat-links a:hover, body .section-module.blog .standard-view .entry-meta .author a:hover, body .section-module.blog .list-view .entry-meta .author a:hover, body .section-module.blog .entry-meta .comment-links a:hover::before, body .section-module.blog .entry-meta a:hover, body .section-module.blog.grid-view .entry-meta .author a:hover {
            color: <?php echo get_theme_mod('blog_post_page_meta_link_hover_color', '#926AA6'); ?>;
        }
    <?php endif; ?>

    /* Single Post/Page */
    <?php if (get_theme_mod('apply_blg_single_clr_enable', false) == true && (is_single())): ?>
        body .single-post .standard-view .entry-title a, body.single-post .entry-header h4.blog-title, body.single-post .blog .entry-header h4{ 
            color: <?php echo get_theme_mod('single_post_page_title_color', '#ffffff'); ?>;
        }
        body.single-post .entry-meta a, body.single-post .entry-meta span, body .blog-single .entry-meta .cat-links a, body .section-module.blog .standard-view .entry-meta .author a, body .section-module.blog .list-view .entry-meta .author a, body .blog-single .entry-meta .comment-links a::before{
            color: <?php echo get_theme_mod('single_post_page_meta_link_color', '#ffffff'); ?>;
        }
        body.single-post .entry-meta a:hover, body .entry-meta a:hover span, body.single-post .entry-meta .comment-links a:hover, body.single-post .entry-meta .posted-on a:hover, body .blog-single .entry-meta .cat-links a:hover, body .section-module.blog .standard-view .entry-meta .author a:hover, body .section-module.blog .list-view .entry-meta .author a:hover, body .blog-single .entry-meta .comment-links a:hover::before{
            color: <?php echo get_theme_mod('single_post_page_meta_link_hover_color', '#926AA6'); ?>;
        }
    <?php endif; ?>

    /* Sidebar */
    <?php if (get_theme_mod('apply_sibar_link_hover_clr_enable', false) == true): ?>
        body .sidebar .widget .widget-title, body .sidebar .widget.widget_block :is(h1,h2,h3,h4,h5,h6), body .sidebar .widget .wp-block-search__label, body .sidebar .wc-block-product-search .wc-block-product-search__label {
            color: <?php echo get_theme_mod('sidebar_widget_title_color', '#ffffff'); ?>;
        }
        body .sidebar p, body .sidebar .wp-block-latest-posts__post-excerpt, body .sidebar .wp-block-latest-posts__post-full-content, body .sidebar .wp-block-latest-posts__post-author, body .sidebar .wp-block-latest-posts__post-date, body .sidebar address {
            color: <?php echo get_theme_mod('sidebar_widget_text_color', '#acacac'); ?>;
        }
        body .sidebar a:not(.sidebar a.button) {
            color: <?php echo get_theme_mod('sidebar_widget_link_color', '#acacac'); ?>;
        }
        body .sidebar .widget .wp-block-tag-cloud a {
            color: <?php echo esc_attr(get_theme_mod('sidebar_widget_link_color', '#ffffff')); ?> !important;
        }
        body .sidebar.s-l-space .sidebar a:hover, body .sidebar .widget a:not(.sidebar a.button):hover, body .sidebar .widget a:focus, body .sidebar .widget .wp-block-tag-cloud a:hover {
            color: <?php echo get_theme_mod('sidebar_widget_link_hover_color', '#926AA6'); ?>;
        }
        body .sidebar .widget .wp-block-tag-cloud a:hover {
            color: <?php echo esc_attr(get_theme_mod('sidebar_widget_link_hover_color', '#ffffff')); ?> !important;
        }
    <?php endif; ?>

    /* Footer Widgets */
    <?php if (get_theme_mod('apply_ftrsibar_link_hover_clr_enable', false) == true) { ?>
        body .site-footer {
            background-color: <?php echo get_theme_mod('footer_widget_background_color', '#202022'); ?>;
        }
        body .footer-sidebar .widget .widget-title, body .footer-sidebar:not(.site-info .footer-sidebar) .widget.widget_block :is(h1,h2,h3,h4,h5,h6), body .footer-sidebar .widget .wp-block-search__label{
            color: <?php echo get_theme_mod('footer_widget_title_color', '#ffffff'); ?>;
        }
        body .footer-sidebar .widget.widget_block h1:after, body .footer-sidebar .widget.widget_block h2:after,
        body .footer-sidebar .widget.widget_block h3:after, body .footer-sidebar .widget.widget_block h4:after,
        body .footer-sidebar .widget.widget_block h5:after, body .footer-sidebar .widget.widget_block h6:after,
        body .footer-sidebar .widget .wp-block-search__label:after {
            background-color: <?php echo esc_attr(get_theme_mod('footer_widget_title_color', '#ffffff')); ?>;
        }
        body .footer-sidebar:not(.site-info .footer-sidebar) p, body .footer-sidebar .widget, body .footer-sidebar .widget_text p, body .footer-sidebar .widget_text p.description, body .footer-sidebar .widget_text address p, body .footer-sidebar.footer-typo .wp-block-latest-posts__post-author, body .footer-sidebar.footer-typo .wp-block-latest-posts__post-date {
            color: <?php echo get_theme_mod('footer_widget_text_color', '#ffffff'); ?>;
        }
        body .footer-sidebar:not(.site-info .footer-sidebar) .widget a, body .footer-sidebar .widget_recent_entries .post-date, body .footer-sidebar .widget_text address a, body .footer-sidebar .widget address i {
            color: <?php echo get_theme_mod('footer_widget_link_color', '#ffffff'); ?>;
        }
        body .footer-sidebar:not(.site-info .footer-sidebar) .widget li:before {
            color: <?php echo get_theme_mod('footer_widget_link_color', '#ffffff'); ?>;
        }
        body .footer-sidebar .widget a:hover, body .footer-sidebar .widget.widget_block a:hover, body .footer-sidebar .widget_text address a:hover {
            color: <?php echo get_theme_mod('footer_widget_link_hover_color', '#926AA6'); ?>;
        }
        body .footer-sidebar .widget .wp-block-tag-cloud a {
            color: <?php echo get_theme_mod('footer_widget_link_color', '#ffffff');?> !important
        }
        body .footer-sidebar .widget .wp-block-tag-cloud a:hover, body .site-footer .footer-sidebar .widget .wp-block-tag-cloud a.tag-cloud-link:hover {
            color: <?php echo get_theme_mod('footer_widget_link_hover_color', '#ffffff');?> !important;
        }
        
    <?php } else { ?>
        .site-footer p {
            color: #fff;
        }
    <?php } ?>

    /* Footer Bar */
        body .site-info {
        border-top: <?php echo get_theme_mod('footer_bar_border',0);?>px <?php echo get_theme_mod('footer_border_style','solid');?> <?php echo get_theme_mod('wpkites_footer_border_clr','#fff');?>
        }
        <?php if (get_theme_mod('apply_foot_hover_anchor_enable', false) == true):
        ?>
        body .site-info .footer-sidebar .widget-title, body .site-info .footer-sidebar .widget-title, body .site-info .footer-sidebar .widget.widget_block h1, body .site-info .footer-sidebar .widget.widget_block h2, body .site-info .footer-sidebar .widget.widget_block h3, body .site-info .footer-sidebar .widget.widget_block h4, body .site-info .footer-sidebar .widget.widget_block h5, body .site-info .footer-sidebar .widget.widget_block h6 {
        color: <?php echo get_theme_mod('advance_footer_bar_title_color','#fff'); ?>; }       
        
        body .site-info {
            background-color: <?php echo get_theme_mod('footer_bar_background_color', '#926AA6'); ?>;
        }
        body .site-info p, body .site-info span, .footer-sidebar .widget_text p {
            color: <?php echo get_theme_mod('footer_bar_text_color', '#ffffff'); ?>;
        }
        body .site-info a,body .site-info .widget a, body .site-info span a,.site-info .footer-sidebar .widget.widget_block li:before {
            color: <?php echo get_theme_mod('footer_bar_link_color', '#fff'); ?> ;
        }
        body .site-info a:hover, body .site-info .widget_recent_entries li a:hover, body .site-info .footer-sidebar li:hover a, body .site-info .footer-sidebar .widget:not(.widget_calendar) a:hover, body .site-info span a:hover, body .site-info p.copyright-section a:hover {
            color: <?php echo get_theme_mod('footer_bar_link_hover_color', '#000000'); ?>;
        }
    <?php endif; 
    if (get_theme_mod('search_btn_enable', true) == true ){ if(!is_rtl()) { ?>
    .cart-header {
        border-left: 1px solid #747474;
        padding: 0 0 0 0.5rem;
    } 
    <?php } else{?>
        .cart-header {
        border-right: 1px solid #747474;
        padding: 0 0 0 0.5rem;
    } 
    <?php }
    }?>
    .header-sticky.stickymenu1:not(.header-sticky.five.stickymenu1, .header-sticky.six.stickymenu1, .header-sticky.seven.stickymenu1), 
    .header-sticky.stickymenu:not(.header-sticky.five.stickymenu, .header-sticky.six.stickymenu, .header-sticky.seven.stickymenu), 
    .header-sticky.shrink1:not(.header-sticky.five.shrink1, .header-sticky.six.shrink1, .header-sticky.seven.shrink1), 
    .header-sticky.five.stickymenu1 .layout5.header-logo, .header-sticky.five.stickymenu .layout5.header-logo,
    .header-sticky.six.stickymenu1 .index6, .header-sticky.six.stickymenu .index6,
    .header-sticky.seven.stickymenu1 .header-sidebar.layout3, .header-sticky.seven.stickymenu .header-sidebar.layout3
    {
        opacity: <?php echo get_theme_mod('sticky_header_opacity', '1.0'); ?>;
        <?php if (get_theme_mod('sticky_header_height', 0) > 0): ?>;
            padding-top: <?php echo get_theme_mod('sticky_header_height', 0); ?>px;
            padding-bottom: <?php echo get_theme_mod('sticky_header_height', 0); ?>px;
        <?php endif; ?>
    }
body .header-sticky.six.stickymenu, body .header-sticky.five.stickymenu{
    padding-top: 0;
    padding-bottom: 0;
}
body .header-sticky.six.stickymenu .header-logo.index5,body .header-sticky.stickymenu .header-logo.index2 {
padding-top: <?php echo esc_html(get_theme_mod('sticky_header_height', 0)); ?>px;
}
body .header-sticky.six.stickymenu nav.navbar1,body .header-sticky.stickymenu nav.navbar2 {
    padding-bottom: <?php echo esc_html(get_theme_mod('sticky_header_height', 0)); ?>px;
}  
.custom-logo{width: <?php echo esc_html(get_theme_mod('wpkites_logo_length',154));?>px; height: auto;}
body .navbar-brand.sticky-logo img{width: <?php echo esc_html(get_theme_mod('wpkites_logo_length',154));?>px; height: auto !important;}
body .navbar-brand.sticky-logo-mbl img{width: <?php echo esc_html(get_theme_mod('wpkites_logo_length',154));?>px; height: auto !important;}
.wpkites_header_btn{ -webkit-border-radius: <?php echo esc_html(get_theme_mod('after_menu_btn_border',0));?>px;border-radius: <?php echo esc_html(get_theme_mod('after_menu_btn_border',0));?>px;}

</style>