<?php 
add_action( 'add_meta_boxes', 'wpkites_plus_add_meta_box' );

if ( ! function_exists( 'wpkites_plus_add_meta_box' ) ) {
	function wpkites_plus_add_meta_box(){
		add_meta_box( 'wpkites-plus-header-page-metabox-options', esc_html__('Breadcrumbs Setting', 'wpkites-plus' ), 'wpkites_plus_header_meta_box', '','normal');
	}
}

add_action( 'admin_enqueue_scripts', 'wpkites_plus_backend_scripts');

if ( ! function_exists( 'wpkites_plus_backend_scripts' ) ){
	function wpkites_plus_backend_scripts( $hook ) {
		wp_enqueue_style( 'wp-color-picker');
		wp_enqueue_script( 'wp-color-picker');
	}
}

if ( ! function_exists( 'wpkites_plus_header_meta_box' ) ) {
	function wpkites_plus_header_meta_box( $post ) {
		
		$wpkites_show_breadcrumb = get_post_meta( get_the_ID(), 'wpkites_show_breadcrumb', true );

		$custom_text = get_post_custom( $post->ID );
		$text_color = ( isset( $custom_text['text_color'][0] ) ) ? $custom_text['text_color'][0] : '';

		$custom_separator = get_post_custom( $post->ID );
		$separator_color = ( isset( $custom_separator['separator_color'][0] ) ) ? $custom_separator['separator_color'][0] : '';

		$custom_link = get_post_custom( $post->ID );
		$link_color = ( isset( $custom_link['link_color'][0] ) ) ? $custom_link['link_color'][0] : '';

		$custom_link_hover = get_post_custom( $post->ID );
		$link_hover_color = ( isset( $custom_link_hover['link_hover_color'][0] ) ) ? $custom_link_hover['link_hover_color'][0] : '';

		wp_nonce_field( 'wpkites_plus_text_meta_box', 'wpkites_plus_text_meta_box_nonce' );
		wp_nonce_field( 'wpkites_plus_separator_meta_box', 'wpkites_plus_separator_meta_box_nonce' );
		wp_nonce_field( 'wpkites_plus_link_meta_box', 'wpkites_plus_link_meta_box_nonce' );
		wp_nonce_field( 'wpkites_plus_link_hover_meta_box', 'wpkites_plus_link_hover_meta_box_nonce' );
		
		$wpkites_breadcrumbs_choices = apply_filters(
								'wpkites_breadcrumbs_choices',
								array(
			
									'wpkites_breadcrumbs_enable' => array(
										'label' => '',
										'url'   => WPKITESP_PLUGIN_URL . 'inc/images/meta-box/enable.png',
									),
									'wpkites_breadcrumbs_disable' => array(
										'label' => '',
										'url'   => WPKITESP_PLUGIN_URL . 'inc/images/meta-box/disable.png',
									),
								)
							);
		$wpkites_breadcrumbs_choices = array(
								'' => array(
									'label' => '',
									'url'   => WPKITESP_PLUGIN_URL . 'inc/images/meta-box/default.png',
								),
							) + $wpkites_breadcrumbs_choices; ?>
		<script>
		jQuery(document).ready(function($){
		    $('.color_field').each(function(){
        		$(this).wpColorPicker();
    		    });		    
			$('.wpkites_show_breadcrumb').click(function() {
			   if($('#breadcrumb0').is(':checked')) { $('.color').show(); }
			   if($('#breadcrumb1').is(':checked')) { $('.color').show(); }
			   if($('#breadcrumb2').is(':checked')) { $('.color').hide(); }
			});
		});
		</script>
		<style type="text/css">
			label.tg-label > input.wpkites_show_breadcrumb:checked {
			    display: none;
			}
		</style>
		
		<table class="form-table">

			<tr>
	        <th><label for="wpkites_show_breadcrumb"><?php echo esc_html__('Display Breadcrumbs','wpkites-plus'); ?></label></th>
			<td><?php $i=0;foreach ( $wpkites_breadcrumbs_choices as $breadcrumbs_id => $value ) : ?>
			<label class="tg-label breadcrumbs">
				<input type="radio" id="breadcrumb<?php echo $i;?>" class="wpkites_show_breadcrumb" name="wpkites_show_breadcrumb" value="<?php echo esc_attr( $breadcrumbs_id ); ?>" <?php checked( $wpkites_show_breadcrumb, $breadcrumbs_id ); ?> />
				<img src="<?php echo esc_url( $value['url'] ); ?>"/>
			</label>
			<?php $i++;endforeach;?>
			</td>	
	      </tr>
	      
	      <tr class="color">
	        <th><p><?php echo esc_html('Link Color', 'wpkites-plus' ); ?></p></th>
	        <td>
	          <input class="color_field" type="hidden" name="link_color" value="<?php esc_attr( $link_color ); ?>"/>
	      </tr>

	      <tr class="color">
	        <th><p><?php echo esc_html('Link Hover Color', 'wpkites-plus' ); ?></p></th>
	        <td>
	          <input class="color_field" type="hidden" name="link_hover_color" value="<?php esc_attr( $link_hover_color ); ?>"/>
	      </tr>

	    </table>
		
		<?php
	}
}
if ( ! function_exists( 'wpkites_plus_save_header_meta_box' ) ) {
	function wpkites_plus_save_header_meta_box( $post_id ) {
		 // Check if our nonce is set.
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		if( !current_user_can( 'edit_pages' ) ) {
			return;
		}
		if ( !isset( $_POST['link_color'] ) || !wp_verify_nonce( $_POST['wpkites_plus_link_meta_box_nonce'], 'wpkites_plus_link_meta_box' ) ) {
			return;
		}
		if ( !isset( $_POST['link_hover_color'] ) || !wp_verify_nonce( $_POST['wpkites_plus_link_hover_meta_box_nonce'], 'wpkites_plus_link_hover_meta_box' ) ) {
			return;
		}

		 // Check the user's permissions.
        if ( !current_user_can( 'edit_post', $post_id ) ) {
                return;
        }

		$text_color = (isset($_POST['text_color']) && $_POST['text_color']!='') ? $_POST['text_color'] : '';
		update_post_meta($post_id, 'text_color', $text_color);

		$separator_color = (isset($_POST['separator_color']) && $_POST['separator_color']!='') ? $_POST['separator_color'] : '';
		update_post_meta($post_id, 'separator_color', $separator_color);

		$link_color = (isset($_POST['link_color']) && $_POST['link_color']!='') ? $_POST['link_color'] : '';
		update_post_meta($post_id, 'link_color', $link_color);

		$link_hover_color = (isset($_POST['link_hover_color']) && $_POST['link_hover_color']!='') ? $_POST['link_hover_color'] : '';
		update_post_meta($post_id, 'link_hover_color', $link_hover_color);

		if(isset( $_POST['post_ID']))
        {   
          $post_ID = absint($_POST['post_ID']);
            update_post_meta($post_ID, 'wpkites_show_breadcrumb', sanitize_text_field($_POST['wpkites_show_breadcrumb']));
        }   

	}
}

add_action( 'save_post', 'wpkites_plus_save_header_meta_box' );

function wpkites_plus_meta_box() {
    global $post;
    if(!empty($post)) {
        $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

        if($pageTemplate == 'your-page-template-here.php' ) {
            add_meta_box( $id, $title, $callback, 'page', $context, $priority, $callback_args );
        }
    }
}
add_action( 'add_meta_boxes', 'wpkites_plus_meta_box' );