<?php
//call the action for the portfolio section
add_action('wpkites_plus_portfolio_action','wpkites_plus_portfolio_section');
//function for the portfolio section
function wpkites_plus_portfolio_section(){
$wpkites_plus_home_project_enabled = get_theme_mod('portfolio_section_enable', true);
if ($wpkites_plus_home_project_enabled != false) {
    $wpkites_plus_project_section_title = get_theme_mod('home_portfolio_section_title', __('Our Latest Case Studies', 'wpkites-plus'));
    $home_portfolio_section_subtitle = get_theme_mod('home_portfolio_section_subtitle', __('Recent Projects', 'wpkites-plus'));
    $portfolio_col = get_theme_mod('home_portfolio_slide_item', 4);
    $no_portfolio = get_theme_mod('home_portfolio_numbers_options', 3);
    $post_type = 'wpkites_portfolio';
    $tax = 'portfolio_categories';
    $term_args = array('hide_empty' => true, 'orderby' => 'id');
    $posts_per_page = get_theme_mod('home_portfolio_numbers', 3);
    $tax_terms = get_terms($tax, $term_args);
    $defualt_tex_id = get_option('wpkites_default_term_id');
    $j = 1;
    $k = 1;
    ?>
    <section class="section-space portfolio <?php if($portfolio_col==3):echo 'portfolio4';endif;?> bg-default-color" id="portfolio">
        <div class="wpkites-portfolio-container container">
            <?php if (!empty($wpkites_plus_project_section_title) || !empty($home_portfolio_section_subtitle)) {?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="section-header">                           
                            <?php if (!empty($wpkites_plus_project_section_title)): ?><h2 class="section-title home_project_title "><?php echo $wpkites_plus_project_section_title; ?></h2><div class="section-separator border-center"></div><?php endif;
                            if (!empty($home_portfolio_section_subtitle)): ?><h5 class="section-subtitle"><?php echo $home_portfolio_section_subtitle; ?></h5><?php endif; ?>
                             <div class="separator"><i class="fa fa-crosshairs"></i></div>
                        </div>
                    </div>
                </div>
            <?php }
            if ($tax_terms) {
                $args = array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',
                    // 'portfolio_categories' => $tax_term->slug,
                    'posts_per_page' => $no_portfolio,
                    'orderby' => 'menu_order',
                );
                $portfolio_query = null;
                $portfolio_query = new WP_Query($args);
                if ($portfolio_query->have_posts()):?>
                    <!-- Grid row -->
                    <div class="row">
                    <?php
                    while ($portfolio_query->have_posts()) : 
                        $portfolio_query->the_post();
                        $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                        if (get_post_meta(get_the_ID(), 'portfolio_link', true)) {
                                $portfolio_link = get_post_meta(get_the_ID(), 'portfolio_link', true);
                        } else {
                            $portfolio_link = '';
                        }
                        $portfolio_description = get_the_content();
                        ?>
                        <!-- Grid column -->
                        <div class="col-lg-<?php echo $portfolio_col;?> col-md-<?php echo $portfolio_col;?>">
                        <!-- Card -->
                            <figure class="portfolio-thumbnail">
                                <?php
                                the_post_thumbnail('full', array('class' => 'img-fluid'));
                                if (has_post_thumbnail()) {
                                    $post_thumbnail_id = get_post_thumbnail_id();
                                    $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
                                }

                                if (!empty($portfolio_link)) {
                                    $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';

                                    $portlink = '<a href=' . ($portfolio_link) . ' title=' . esc_html(get_the_title()) . ' ' . $tagt . ' >' . esc_html(get_the_title()) . '</a>';
                                } else {
                                    $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';
                                    $portlink ='<a href="#" title=' . esc_html(get_the_title()) . ' ' . $tagt . ' >' . esc_html(get_the_title()) . '</a>';
                                }
                                foreach ($tax_terms as $tax_term) {
                                $modelId = get_the_ID() . '_model'.rawurldecode($tax_term->slug);
                                }?>
                                <div class="portfolio-item-description">
                                    <div class="see-more">
                                        <a data-toggle="modal" data-target="#<?php echo $modelId;?>" ><i class="fa fa-plus"></i></a>
                                    </div>
                                    <div class="portfolio_description">
                                        <div class="portfolio-item-meta">
                                            <?php 
                                            $tax_string=implode(" ",get_the_taxonomies());
                                            $tax_cat=str_replace( array( 'Categories:'), ' ', $tax_string);?>
                                            <h5 class="post_cats"><?php echo $tax_cat;?></h5>
                                        </div>
                                        <?php if(!empty($portlink)):?>
                                            <div class="portfolio-item-title">
                                                <h4 class="title"><?php echo $portlink; ?></h4>
                                            </div>
                                      <?php endif;?>
                                    </div>
                                </div>
                                <div class="overlay"></div>
                            </figure>
                            <!-- Card -->                         

                            <!-- Modal -->
                            <div class="modal fade" id="<?php echo $modelId;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body p-0">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            
                                            <!-- Grid row -->
                                            <div class="row">
                                            
                                                <!-- Grid column -->
                                                <div class="col-md-6 py-5 pl-5">                                                 
                                                    <article class="post text-center">
                                                        <div class="entry-header">
                                                            <h2 class="entry-title"><?php echo $portlink; ?></h2>
                                                        </div>                                                        
                                                        <div class="entry-content">
                                                                <p><?php echo get_the_content(); ?></p>
                                                        </div>                                                        
                                                    </article>                                                    
                                                </div>
                                                <!-- Grid column -->
                                            
                                                <!-- Grid column -->
                                                <div class="col-md-6">
                                                    <div class="view rounded-right">
                                                        <img class="img-fluid" src="<?php echo esc_url($post_thumbnail_url); ?>" alt="<?php the_title();?>">
                                                    </div>
                                                </div>
                                                <!-- Grid column --> 
                                            </div>
                                            <!-- Grid row -->                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal -->
                        </div>
                        <!-- /Grid column -->
                        <?php
                        endwhile;
                        wp_reset_query();?>
                    </div>
                    <?php
                    endif;                         
            }
            else{
                 do_action('wpkites_plus_dummy_portfolio_layout', $portfolio_col);
            }?>
        </div>
    </section>
<?php } 
}