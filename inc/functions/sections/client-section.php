<?php
//call the action for the client section
add_action('wpkites_plus_client_action','wpkites_plus_client_section');
//function for the client section
function wpkites_plus_client_section()
{
$client_section_enable = get_theme_mod('client_section_enable', true);
    if($client_section_enable != false)
    {
        // Client Callback
            $client_items=get_theme_mod('client_items',5);
            $atts=array(
                      'items' => $client_items,
                    );

            $client_section=wpkites_client_callback($atts);
            echo $client_section;
    }
}