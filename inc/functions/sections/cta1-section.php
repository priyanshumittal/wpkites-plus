<?php
//call the action for the cta1 section
add_action('wpkites_plus_cta1_action','wpkites_plus_cta1_section');
//function for the cta1 section
function wpkites_plus_cta1_section()
{
$cta1_section_enable  = get_theme_mod('cta1_section_enable', true);
	if($cta1_section_enable != false){ 
		// CTA1 Callback
	        $cta1_section=wpkites_cta1_callback();
	        echo $cta1_section; 
	} 
}