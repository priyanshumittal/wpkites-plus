<?php
//call the action for the about section
add_action('wpkites_plus_about_action','wpkites_plus_about_section');
//function for the about section
function wpkites_plus_about_section()
{
$about_section_enable  = get_theme_mod('about_section_enable', true);
	if($about_section_enable != false){
		// About Callback
	        $about_section=wpkites_about_callback();
	        echo $about_section; 
	} 
}