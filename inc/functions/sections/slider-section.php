<?php
//call the action for the slider section
add_action('wpkites_plus_slider_action','wpkites_plus_slider_section');
//function for the slider section
function wpkites_plus_slider_section()
{
$slider_data = get_theme_mod('wpkites_plus_slider_content');
if (empty($slider_data)) {
    if (get_theme_mod('home_slider_title') != '' || get_theme_mod('home_slider_discription') != '' || get_theme_mod('home_slider_image') != '') {
        $home_slider_title = get_theme_mod('home_slider_title');
        $home_slider_discription = get_theme_mod('home_slider_discription');
        $home_slider_btn_target = get_theme_mod('home_slider_btn_target');
        $home_slider_btn_txt = get_theme_mod('home_slider_btn_txt');
        $home_slider_btn_link = get_theme_mod('home_slider_btn_link');
        $home_slider_btn_target2= get_theme_mod('home_slider_btn_target2');
        $home_slider_btn_txt2 = get_theme_mod('home_slider_btn_txt2');
        $home_slider_btn_link2 = get_theme_mod('home_slider_btn_link2');
        $home_slider_image = get_theme_mod('home_slider_image');
        $home_slider_align_split = get_theme_mod('slider_content_alignment');
        $home_slider_caption='customizer_repeater_slide_caption_'.$home_slider_align_split;
        $slider_data = json_encode( array(
                            array(
                                'title'      => !empty($home_slider_title)? $home_slider_title:__('We provide solutions to','wpkites-plus') . '<br>' . __('grow your business','wpkites-plus'),
                                'text'       => !empty($home_slider_discription)? $home_slider_discription :'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.',
                                'abtsliderbutton_text' => !empty($home_slider_btn_txt)? $home_slider_btn_txt : __('Learn More','wpkites-plus'),
                                'abtsliderlink' => !empty($home_slider_btn_link)? $home_slider_btn_link : '#',
                                'image_url'  => !empty($home_slider_image)? $home_slider_image :  WPKITESP_PLUGIN_URL .'/inc/images/slider/slide-1.jpg',
                                'abtslider_open_new_tab' => !empty($home_slider_btn_target)? $home_slider_btn_target : false,
                                'abtbutton_text' => !empty($home_slider_btn_txt2)? $home_slider_btn_txt2 :__('About Us', 'wpkites-plus'),
                                'abtlink' => !empty($home_slider_btn_link2)? $home_slider_btn_link2 :'#',
                                'abtopen_new_tab' => !empty($home_slider_btn_target2)? $home_slider_btn_target2 : false,
                                'home_slider_caption' => !empty($home_slider_align_split)? $home_slider_caption : 'customizer_repeater_slide_caption_center',
                                'id'         => 'customizer_repeater_56d7ea7f40b50',
                                ),
                        ) );
    } 
    else {
        $slider_data = json_encode(array(
            array(
                'title' => __('We provide solutions to','wpkites-plus') . '<br><span>' . __('grow your business', 'wpkites-plus') . '</span>',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.',
                'abtsliderbutton_text' => __('Learn More', 'wpkites-plus'),
                'abtsliderlink' => '#',
                'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/slider/slide-1.jpg',
                'abtslider_open_new_tab' => 'yes',
                'abtbutton_text' => __('About Us', 'wpkites-plus'),
                'abtlink' => '#',
                'abtopen_new_tab' => 'yes',
                'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                'sidebar_check' => 'no',
                'id' => 'customizer_repeater_56d7ea7f40b96',
            ),
            array(
                'title' =>  __('We create stunning') . '<br><span>' . __('WordPress themes','wpkites-plus') . '</span>',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.',
                'abtsliderbutton_text' => __('Learn More', 'wpkites-plus'),
                'abtsliderlink' => '#',
                'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/slider/slide-2.jpg',
                'abtslider_open_new_tab' => 'yes',
                'abtbutton_text' => __('About Us', 'wpkites-plus'),
                'abtlink' => '#',
                'abtopen_new_tab' => 'yes',
                'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                'sidebar_check' => 'no',
                'id' => 'customizer_repeater_56d7ea7f40b97',
            ),
            array(
               'title' => __('We provide solutions to','wpkites-plus') . '<br><span>' . __('grow your business', 'wpkites-plus') . '</span>',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.',
                'abtsliderbutton_text' => __('Learn More', 'wpkites-plus'),
                'abtsliderlink' => '#',
                'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/slider/slide-3.jpg',
                'abtslider_open_new_tab' => 'yes',
                'abtbutton_text' => __('About Us', 'wpkites-plus'),
                'abtlink' => '#',
                'abtopen_new_tab' => 'yes',
                'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                'sidebar_check' => 'no',
                'id' => 'customizer_repeater_56d7ea7f40b98',
            ),
        ));
    }
}
$home_page_slider_enabled = get_theme_mod('home_page_slider_enabled', true);
if ($home_page_slider_enabled != false) {
$video_upload = get_theme_mod('slide_video_upload');
$video_upload = wp_get_attachment_url( $video_upload);
$video_youtub = get_theme_mod('slide_video_url');   
// Below Script will run for only video slide       
if((!empty($video_upload) || !empty($video_youtub) ) && (get_theme_mod('slide_variation','slide')=='video')){ ?>
    <section class="video-slider home-section home-full-height bcslider-section" id="totop" data-background="assets/images/section-5.jpg">
    <?php if(!empty($video_youtub)){?>
        <div class="video-player" data-property="{videoURL:'<?php echo $video_youtub;?>', containment:'.home-section', mute:false, autoPlay:true, loop:true, opacity:1, showControls:false, showYTLogo:false, vol:25}"></div>
    <?php } else if(!empty($video_upload)){?>
        <video playsinline autoplay="" muted="" loop="" id="video_slider">
            <source src="<?php echo $video_upload; ?>" type="video/mp4">
         </video>
     <?php }?>
        <div id="slider-carousel" class="owl-carousel owl-theme" id="home">
        <?php $slider_data = json_decode($slider_data);
        if($slider_data!='')
        {
           foreach ($slider_data as $slide_iteam) {
                $slider_image = !empty($slide_iteam->image_url) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->image_url, 'Slider section') : '';
                $slider_text = !empty($slide_iteam->text) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->text, 'Slider section') : '';
                $slider_title = !empty($slide_iteam->title) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->title, 'Slider section') : '';
                $slider_subtitle = !empty($slide_iteam->subtitle) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->subtitle, 'Slider section') : '';
                $slider_link = !empty($slide_iteam->abtsliderlink) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->abtsliderlink, 'Slider section') : '';
                $slider_link_two = !empty($slide_iteam->abtlink) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->abtlink, 'Slider section') : '';
                $slider_button_text = !empty($slide_iteam->abtsliderbutton_text) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->abtsliderbutton_text, 'Slider section') : '';
                $slider_button_two_text = !empty($slide_iteam->abtbutton_text) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->abtbutton_text, 'Slider section') : '';
                $slider_align_split = explode('_', $slide_iteam->home_slider_caption);
                ?> 
                <div class="item <?php echo $slide_iteam->sidebar_check;?>">
                    <div class="container slider-caption <?php if($slide_iteam->sidebar_check=='yes'){echo 'widget-slide';}?>">
                    <?php if(($slide_iteam->sidebar_check=='yes') && ($slider_align_split[4]!='center')){?>
                        <div class="row">
                        <?php if($slider_align_split[4]=='left'):?>
                            <div class="col-md-7">
                                <div class="caption-content <?php echo 'text-'.$slider_align_split[4];?>">
                                <?php
                                if($slider_subtitle!=''){ ?>
                                    <p class="heading"><span><?php echo $slider_subtitle; ?></span></p>
                                <?php }
                                 if($slider_title!=''){ ?>
                                    <div class="inner-title">
                                    <h2 class="title"><?php echo $slider_title; ?></h2>
                                </div>
                                <?php } 
                                if($slider_text!=''){ ?>
                                    <p class="description"><?php echo $slider_text; ?></p>
                                <?php }
                                if(($slider_button_text !=null) || ($slider_button_two_text !=null)) { ?>
                                    <div class="btn-combo">
                                    <?php if($slider_button_text !=null): ?>
                                        <a href="<?php echo $slider_link; ?>"  <?php if($slide_iteam->abtslider_open_new_tab == 'yes'){ echo "target='_blank'"; } ?> class="btn-small btn-default"> <?php echo $slider_button_text; ?></a>
                                    <?php endif;
                                    if($slider_button_two_text !=null): ?>
                                        <a href="<?php echo $slider_link_two; ?>" <?php if($slide_iteam->abtopen_new_tab == 'yes'){ echo "target='_blank'";  } ?> class="btn-small btn-light"><?php echo $slider_button_two_text; ?></a>
                                    <?php endif;?>
                                    </div>
                                <?php } ?>                      
                                </div>
                            </div>
                            <div class="col-md-5">
                                <?php dynamic_sidebar('slider-widget-area');?>
                            </div>
                        <?php endif;

                        if($slider_align_split[4]=='right'):?>
                            <div class="col-md-5">
                                <?php dynamic_sidebar('slider-widget-area');?>
                            </div>
                            <div class="col-md-7">
                                <div class="caption-content <?php echo 'text-'.$slider_align_split[4];?>">
                                <?php
                                if($slider_subtitle!=''){ ?>
                                    <p class="heading"><span><?php echo $slider_subtitle; ?></span></p>
                                <?php }
                                if($slider_title!=''){ ?>
                                    <div class="inner-title">
                                    <h2 class="title"><?php echo $slider_title; ?></h2>
                                </div>
                                <?php } 
                                if($slider_text!=''){ ?>
                                    <p class="description"><?php echo $slider_text; ?></p>
                                <?php }
                                if(($slider_button_text !=null) || ($slider_button_two_text !=null)) { ?>
                                    <div class="btn-combo">
                                    <?php if($slider_button_text !=null): ?>
                                        <a href="<?php echo $slider_link; ?>"  <?php if($slide_iteam->abtslider_open_new_tab == 'yes'){ echo "target='_blank'"; } ?> class="btn-small btn-default"> <?php echo $slider_button_text; ?></a>
                                    <?php endif;
                                    if($slider_button_two_text !=null): ?>
                                        <a href="<?php echo $slider_link_two; ?>" <?php if($slide_iteam->abtopen_new_tab == 'yes'){ echo "target='_blank'";  } ?> class="btn-small btn-light"><?php echo $slider_button_two_text; ?></a>
                                        <?php endif;?>
                                    </div>
                                <?php } ?>                      
                                </div>
                            </div>
                        <?php endif;?>                                    
                        </div>
                    <?php
                    }
                    else
                    {?>
                        <div class="caption-content <?php echo 'text-'.$slider_align_split[4];?>">
                        <?php
                        if($slider_subtitle!=''){ ?>
                                    <p class="heading"><span><?php echo $slider_subtitle; ?></span></p>
                                <?php }
                                 if($slider_title!=''){ ?>
                            <div class="inner-title">
                            <h2 class="title"><?php echo $slider_title; ?></h2>
                            </div>
                        <?php } 
                        if($slider_text!=''){ ?>
                            <p class="description"><?php echo $slider_text; ?></p>
                        <?php }
                        if(($slider_button_text !=null) || ($slider_button_two_text !=null)) { ?>
                            <div class="btn-combo">
                            <?php if($slider_button_text !=null): ?>
                                <a href="<?php echo $slider_link; ?>"  <?php if($slide_iteam->abtslider_open_new_tab == 'yes'){ echo "target='_blank'"; } ?> class="btn-small btn-default"> <?php echo $slider_button_text; ?></a>
                            <?php endif;                                            
                            if($slider_button_two_text !=null): ?>
                                <a href="<?php echo $slider_link_two; ?>" <?php if($slide_iteam->abtopen_new_tab == 'yes'){ echo "target='_blank'";  } ?> class="btn-small btn-light"><?php echo $slider_button_two_text; ?></a>
                            <?php endif;?>
                            </div>
                        <?php } ?>                      
                        </div>
                                
                    <?php
                    }?>      
                    </div>
                    <?php $slider_image_overlay = get_theme_mod('slider_image_overlay',true);
                    $slider_overlay_section_color = get_theme_mod('slider_overlay_section_color','rgba(0,0,0,0.6)');
                    if($slider_image_overlay != false) { ?>
                        <div class="overlay" style="background-color:<?php echo $slider_overlay_section_color;?>"></div>
                    <?php } ?>
                </div>
                <?php 
                }
            } ?>    
        </div>
    </section>    
<!-- /Video Slider Section -->
<?php } 
else {?>
    <!-- Slider Section --> 
    <section class="bcslider-section">
        <div id="slider-carousel" class="owl-carousel owl-theme">
            <?php
            $slider_data = json_decode($slider_data);
            if ($slider_data != '') {
                foreach ($slider_data as $slide_iteam) {
                    $slider_image = !empty($slide_iteam->image_url) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->image_url, 'Slider section') : '';
                    $slider_text = !empty($slide_iteam->text) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->text, 'Slider section') : '';
                    $slider_title = !empty($slide_iteam->title) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->title, 'Slider section') : '';
                    $slider_subtitle = !empty($slide_iteam->subtitle) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->subtitle, 'Slider section') : '';
                    $slider_link = !empty($slide_iteam->abtsliderlink) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->abtsliderlink, 'Slider section') : '';
                    $slider_link_two = !empty($slide_iteam->abtlink) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->abtlink, 'Slider section') : '';
                    $slider_button_text = !empty($slide_iteam->abtsliderbutton_text) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->abtsliderbutton_text, 'Slider section') : '';
                    $slider_button_two_text = !empty($slide_iteam->abtbutton_text) ? apply_filters('wpkites_plus_translate_single_string', $slide_iteam->abtbutton_text, 'Slider section') : '';
                    $slider_align_split = explode('_', $slide_iteam->home_slider_caption);
                    ?> 
                    <div class="item <?php echo $slide_iteam->sidebar_check;?> home-section home-full-height" <?php if ($slider_image != '') { ?>style="background-image:url( <?php echo $slider_image; ?> );" <?php } ?>>
                        <div class="container slider-caption <?php if($slide_iteam->sidebar_check=='yes'){echo 'widget-slide';}?>">
                        <?php if(($slide_iteam->sidebar_check=='yes') && ($slider_align_split[4]!='center')){?>      <div class="row">
                            <?php if($slider_align_split[4]=='left'){?>
                                <div class="col-md-7">
                                    <div class="caption-content <?php echo 'text-'.esc_attr($slider_align_split[4]);?>">
                                    <?php if($slider_title!=''){ ?>
                                        <h2 class="title"><?php echo wp_kses_post($slider_title); ?></h2>                                            
                                        <?php }

                                        if($slider_text!=''){ ?>
                                            <p class="description"><?php echo wp_kses_post($slider_text); ?></p>
                                        <?php } 
                                        if(($slider_button_text !=null) || ($slider_button_two_text !=null)) { ?>
                                            <div class="btn-combo">
                                            <?php if($slider_button_text !=null): ?>
                                                <a href="<?php echo esc_url($slider_link); ?>"  <?php if($slide_iteam->abtslider_open_new_tab == 'yes'){ echo "target='_blank'"; } ?> class="btn-small btn-default"> <?php echo esc_html($slider_button_text); ?></a>
                                            <?php endif; ?>

                                            <?php if($slider_button_two_text !=null): ?>
                                                <a href="<?php echo esc_url($slider_link_two); ?>" <?php if($slide_iteam->abtopen_new_tab == 'yes'){ echo "target='_blank'";  } ?> class="btn-small btn-light"><?php echo esc_html($slider_button_two_text); ?></a>
                                            <?php endif;?>
                                            </div>
                                        <?php } ?>                      
                                        </div>
                                    </div>
                                <div class="col-md-5">
                                    <?php dynamic_sidebar('slider-widget-area');?>
                                </div>
                            <?php }
                            if($slider_align_split[4]=='right'){?>
                                <div class="col-md-5">
                                    <?php dynamic_sidebar('slider-widget-area');?>
                                </div>
                                <div class="col-md-7">
                                    <div class="container slider-caption <?php if($slide_iteam->sidebar_check=='yes'){echo 'widget-slide';}?>">
                                        <div class="caption-content <?php echo 'text-'.esc_attr($slider_align_split[4]);?>">
                                        <?php if($slider_title!=''){ ?>
                                            <h2 class="title"><?php echo wp_kses_post($slider_title); ?></h2>                                          
                                        <?php } 

                                        if($slider_text!=''){ ?>
                                            <p class="description"><?php echo wp_kses_post($slider_text); ?></p>
                                        <?php }

                                        if(($slider_button_text !=null) || ($slider_button_two_text !=null)) { ?>
                                            <div class="btn-combo">

                                            <?php if($slider_button_text !=null): ?>
                                                <a href="<?php echo esc_url($slider_link); ?>"  <?php if($slide_iteam->abtslider_open_new_tab == 'yes'){ echo "target='_blank'"; } ?> class="btn-small btn-default"> <?php echo esc_html($slider_button_text); ?></a>
                                            <?php endif; 

                                            if($slider_button_two_text !=null): ?>
                                                    <a href="<?php echo esc_url($slider_link_two); ?>" <?php if($slide_iteam->abtopen_new_tab == 'yes'){ echo "target='_blank'";  } ?> class="btn-small btn-light"><?php echo esc_html($slider_button_two_text); ?></a>
                                            <?php endif;?>
                                        </div>
                                        <?php } ?>                      
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                            </div>
                    <?php
                    }
                    else
                    {?>
                    <div class="caption-content <?php echo 'text-' . $slider_align_split[4]; ?>">
                        <?php if ($slider_title != '') { ?>
                            <h2 class="title"><?php echo $slider_title; ?></h2>
                        <?php } 

                        if ($slider_text != '') { ?>
                            <p class="description"><?php echo $slider_text; ?></p>
                        <?php } 
                        
                        if (($slider_button_text != null) || ($slider_button_two_text != null)) { ?>
                            <div class="btn-combo">
                                <?php if ($slider_button_text != null): ?>
                                    <a href="<?php echo $slider_link; ?>"  <?php
                                    if ($slide_iteam->abtslider_open_new_tab == 'yes') {
                                        echo "target='_blank'";
                                    }
                                    ?> class="btn-small btn-default"> <?php echo $slider_button_text; ?> </a>
                                   <?php endif; ?>                                          
                                   <?php if ($slider_button_two_text != null): ?>
                                    <a href="<?php echo $slider_link_two; ?>" <?php
                                    if ($slide_iteam->abtopen_new_tab == 'yes') {
                                        echo "target='_blank'";
                                    }
                                    ?> class="btn-small btn-light"><?php echo $slider_button_two_text; ?></a>
                                   <?php endif; ?>
                            </div>
                        <?php } ?>
                    </div>
                    <?php 
                    }?>
                </div>
                <?php
                $slider_image_overlay = get_theme_mod('slider_image_overlay', true);
                $slider_overlay_section_color = get_theme_mod('slider_overlay_section_color', 'rgba(0,0,0,0.6)');
                if ($slider_image_overlay != false) {
                    ?>
                    <div class="overlay" style="background-color:<?php echo $slider_overlay_section_color; ?>"></div>
                <?php } ?>
            </div>
            <?php
                }
            }
            ?>  
        </div>      
    </section>
    <div class="clearfix"></div>
    <?php
}
}
}