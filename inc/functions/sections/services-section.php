<?php
//call the action for the services section
add_action('wpkites_plus_services_action','wpkites_plus_services_section');
//function for the services section
function wpkites_plus_services_section()
{
$wpkites_home_service_enabled = get_theme_mod('home_service_section_enabled', true);
    if($wpkites_home_service_enabled != false)
    {
        // Service Callback
            $atts=array(
                      'style' => get_theme_mod('home_serive_design_layout',1),
                      'col' => get_theme_mod('home_service_col',3),
                );

            $service_section=wpkites_service_callback($atts);
            echo $service_section; 
    }
}