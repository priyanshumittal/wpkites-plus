<?php
//call the action for the cta2 section
add_action('wpkites_plus_cta2_action','wpkites_plus_cta2_section');
//function for the cta2 section
function wpkites_plus_cta2_section()
{
$cta2_section_enable  = get_theme_mod('cta2_section_enable', true);
	if($cta2_section_enable != false) 
	{ 
		// CTA2 Callback
	        $cta2_section=wpkites_cta2_callback();
	        echo $cta2_section;
	}
}