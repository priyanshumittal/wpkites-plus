<?php
if (!function_exists('starter_team_json')) {

    function starter_team_json() {
        return json_encode(array(
                array(
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/team/team-1.jpg',
                    'image_url2' => WPKITESP_PLUGIN_URL . '/inc/images/portfolio/project-20.jpg',
                    'membername' => 'Danial Wilson',
                    'designation' => esc_html__('Senior Manager', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_26d7ea7f40c56',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-37fb908374e06',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-47fb9144530fc',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9750e1e09',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-67fb0150e1e256',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/team/team-2.jpg',
                    'image_url2' => WPKITESP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Amanda Smith',
                    'designation' => esc_html__('Founder & CEO', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d1ea2f40c66',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9133a7772',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9160rt683',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916zzooc9',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916qqwwc784',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/team/team-3.jpg',
                    'image_url2' => WPKITESP_PLUGIN_URL . '/inc/images/portfolio/project-11.jpg',
                    'membername' => 'Victoria Wills',
                    'designation' => esc_html__('Web Master', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c76',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb917e4c69e',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb91830825c',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e8',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPKITESP_PLUGIN_URL . '/inc/images/team/team-4.jpg',
                    'image_url2' => WPKITESP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Travis Marcus',
                    'designation' => esc_html__('UI Developer', 'wpkites-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c86',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb925cedcb2',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb92615f030',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
            ));
    }

}
if (!function_exists('starter_service_json')) {

    function starter_service_json() {
        return json_encode(array(
            array(
                'icon_value' => 'fa-headphones',
                'title' => esc_html__('Unlimited Support', 'wpkites-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b56',
            ),
            array(
                'icon_value' => 'fa-mobile',
                'title' => esc_html__('Pixel Perfect Design', 'wpkites-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b66',
            ),
            array(
                'icon_value' => 'fa fa-cogs',
                'title' => esc_html__('Powerful Options', 'wpkites-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b86',
            ),
            array(
                'icon_value' => 'fa-desktop',
                'title' => esc_html__('Powerful Options', 'wpkites-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_52d7ea8f40b86',
            ),
            array(
                'icon_value' => 'fa-headphones',
                'title' => esc_html__('Powerful Options', 'wpkites-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_53d7ea9f40b86',
            ),
            array(
                'icon_value' => 'fa-desktop',
                'title' => esc_html__('Powerful Options', 'wpkites-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_59d7ea4f40b86',
            ),
        ));
    }

}
if (!function_exists('starter_funfact_json')) {

    function starter_funfact_json() {
        return json_encode(array(
            array(
                'icon_value' => 'fa-pie-chart',
                'title' => '2531',
                'text' => esc_html__('Registered Members', 'wpkites-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b56',
            ),
            array(
                'icon_value' => 'fa-regular fa-face-smile',
                'title' => '3564',
                'text' => esc_html__('Happy Customer', 'wpkites-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b66',
            ),
            array(
                'icon_value' => 'fa-shield',
                'title' => '231',
                'text' => esc_html__('Win Awards', 'wpkites-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b86',
            ),
            array(
                'icon_value' => 'fa-solid fa-hand-peace',
                'title' => '1070',
                'text' => esc_html__('Satisfied Customer', 'wpkites-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b87',
            ),
        ));
    }

}

if (!function_exists('wpkites_plus_dummy_portfolio_fn')) {

    function wpkites_plus_dummy_portfolio_fn($portfolio_col) {
        $portfolio_cat=array(__('Business','wpkites-plus'), __('Portfolio','wpkites-plus'), __('finance','wpkites-plus'), __('Business','wpkites-plus'), __('Portfolio','wpkites-plus'), __('finance','wpkites-plus'));
        $portfolio_title=array(__('Business Planing','wpkites-plus'), __('Portfolio Planing','wpkites-plus'), __('finance Planing','wpkites-plus'), __('Business Planing','wpkites-plus'), __('Portfolio Planing','wpkites-plus'), __('finance Planing','wpkites-plus'));
        $portfolio_discreption='<p>sellus facilisis, nunc in lacinia auctor, eeros lacus aliquet velit, quis lobortis risus nunc nec nisi maecans et turpis vitae velit.volutpat.</p><p>sellus facilisis, nunc in lacinia auctor, eeros lacus aliquet velit, quis lobortis risus nunc nec nisi maecans et turpis vitae velit.volutpat.</p>';
        $no_portfolio = get_theme_mod('home_portfolio_numbers_options', 3);?>
        <!-- Modal -->
        <?php for ($i = 1; $i<= $no_portfolio; $i++) {?>
        <div class="modal fade" id="basicExampleModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        
                        <!-- Grid row -->
                        <div class="row">
                        
                            <!-- Grid column -->
                            <div class="col-md-6 py-5 pl-5">
                                
                            <article class="post text-center">
                                <div class="entry-header">
                                    <h2 class="entry-title"><a href="#" alt="<?php esc_attr_e('Multi-purpose','wpkites-plus'); ?>">Pop-in  effect</a></h2>
                                </div>
                                <div class="entry-content">
                                    <p><?php echo $portfolio_discreption;?></p>
                                </div>
                            </article>
                                
                            </div>
                            <!-- Grid column -->
                        
                            <!-- Grid column -->
                            <div class="col-md-6">
                                <div class="view rounded-right">
                                    <img class="img-fluid" src="<?php echo WPKITESP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" alt="<?php esc_attr_e('Sample image','wpkites-plus'); ?>">
                                </div>
                            </div>
                            <!-- Grid column -->
                        
                        </div>
                        <!-- Grid row -->
                        
                    </div>
                </div>
            </div>
        </div>
        <?php
    }  ?>
    <!-- Grid row -->
    <div class="row">
       <?php 
       for ($i = 1; $i<=$no_portfolio; $i++) {
            $portfolio_col = get_theme_mod('home_portfolio_slide_item', 4);?>
            <!-- Card -->
            <div class="col-lg-<?php echo $portfolio_col;?> col-md-<?php echo $portfolio_col;?>">
                <figure class="portfolio-thumbnail">
                    <img class="img-fluid" src="<?php echo WPKITESP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" alt="<?php esc_attr_e('Finance Planing','wpkites-plus')?>">
                    <div class="portfolio-item-description">
                       <div class="see-more">
                            <a data-toggle="modal" data-target="#basicExampleModal<?php echo $i; ?>" ><i class="fa fa-plus"></i></a>
                       </div>
                       <div class="portfolio_description">
                           <div class="portfolio-item-meta">
                                <h5 class="post_cats"><a href="#" class="portfolio-category"><?php echo $portfolio_cat[$i-1];?></a></h5>
                           </div>
                           <div class="portfolio-item-title">
                                <h4 class="title"><a href="#"><?php echo $portfolio_title[$i-1];?></a></h4>
                           </div>
                       </div>
                    </div>
                    <div class="overlay"></div>
                </figure>
                <!-- Card -->
           </div>
       <?php } ?>
   </div>
   <?php
    }
}

add_action('wpkites_plus_dummy_portfolio_layout', 'wpkites_plus_dummy_portfolio_fn');

if (!function_exists('wpkites_plus_starter_contact_social_json')) {

    function wpkites_plus_starter_contact_social_json() {
        return json_encode(array(
        array(
                        'icon_value' => 'fa-brands fa-facebook-f',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b76',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-x-twitter',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b77',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-linkedin',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b78',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-instagram',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-youtube',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
    ));
    }
}