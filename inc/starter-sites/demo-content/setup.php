<?php
/**
 * @package Starter Sites
 * @since 1.0
 */


/**
 * Set import files
 */
if ( !function_exists( 'wpkites_plus_starter_sites_import_files' ) ) {

    function wpkites_plus_starter_sites_import_files() {

        // Demos url
        $demo_url = 'https://spicethemes.com/startersites/';

         return array(
            array(
                'import_file_name'              =>  esc_html__('Default', 'wpkites-plus'),
                'import_file_url'               =>  $demo_url . 'default/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'default/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'default/customize-export.dat',
                'preview_url'                   =>  'https://wpkites-pro.spicethemes.com/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/default/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Photography', 'wpkites-plus'),
                'import_file_url'               =>  $demo_url . 'photography/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'photography/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'photography/customize-export.dat',
                'preview_url'                   =>  'http://photography-wpkites.spicethemes.com/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/photography/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Job Portal', 'wpkites-plus'),
                'import_file_url'               =>  $demo_url . 'job-portal/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'job-portal/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'job-portal/customize-export.dat',
                'preview_url'                   =>  'http://job-portal-wpkites.spicethemes.com/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/job-portal/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Restaurant', 'wpkites-plus'),
                'import_file_url'               =>  $demo_url . 'restaurant/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'restaurant/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'restaurant/customize-export.dat',
                'preview_url'                   =>  'http://food-restaurant-wpkites.spicethemes.com/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/restaurant/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Corporate', 'wpkites-plus'),
                'import_file_url'               =>  $demo_url . 'corporate/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'corporate/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'corporate/customize-export.dat',
                'preview_url'                   =>  'https://corporate-wpkites.spicethemes.com/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/corporate/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Business', 'wpkites-plus'),
                'import_file_url'               =>  $demo_url . 'business/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'business/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'business/customize-export.dat',
                'preview_url'                   =>  'https://business-wpkites.spicethemes.com/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/business/thumb.png',
            )
        );
    }

}
add_filter( 'pt-ocdi/import_files', 'wpkites_plus_starter_sites_import_files' );

/**
 * Define actions that happen after import
 */
if ( !function_exists( 'wpkites_plus_starter_sites_after_import_mods' ) ) {

    function wpkites_plus_starter_sites_after_import_mods() {

        //Assign the menu
        $main_menu = get_term_by( 'name', 'Primary', 'nav_menu' );
        set_theme_mod( 'nav_menu_locations', array(
                'wpkites-primary' => $main_menu->term_id,
            )
        );

        //Asign the static front page and the blog page
        $front_page = get_page_by_title( 'Home' );
        $blog_page  = get_page_by_title( 'Blog' );

        update_option( 'show_on_front', 'page' );
        update_option( 'page_on_front', $front_page -> ID );
        update_option( 'page_for_posts', $blog_page -> ID );

        $args = array(
        'post_type' => 'wpkites_portfolio',
        'numberposts' => -1
        );
        $wpkites_portfolio_posts = get_posts($args);
        foreach ($wpkites_portfolio_posts as $wpkites_portfolio_post){
            $wpkites_portfolio_post->post_title = $wpkites_portfolio_post->post_title.'';
            wp_update_post( $wpkites_portfolio_post );
        }

    }

}
add_action( 'pt-ocdi/after_import', 'wpkites_plus_starter_sites_after_import_mods' );

if ( !function_exists( 'wpkites_plus_starter_sites_ocdi_css' ) ) {
    // Custom CSS for OCDI plugin
    function wpkites_plus_starter_sites_ocdi_css() { ?>
        <style>
            .ocdi .ocdi__theme-about, .ocdi__intro-text {
                display: none;
            }
        </style>
    <?php
    }
}
add_action('admin_enqueue_scripts', 'wpkites_plus_starter_sites_ocdi_css');

// Change the "One Click Demo Import" name from "Starter Sites" in Appearance menu
function wpkites_plus_starter_sites_ocdi_plugin_page_setup( $default_settings ) {
    $default_settings['parent_slug'] = 'admin.php';
    $default_settings['page_title']  = esc_html__( 'One Click Demo Import' , 'wpkites-plus' );
    $default_settings['menu_title']  = esc_html__( 'Starter Sites' , 'wpkites-plus' );
    $default_settings['capability']  = 'import';
    $default_settings['menu_slug']   = 'one-click-demo-import';
    return $default_settings;

}
add_filter( 'ocdi/plugin_page_setup', 'wpkites_plus_starter_sites_ocdi_plugin_page_setup' );

// Register required plugins for the demo's
function wpkites_plus_starter_sites_register_plugins( $plugins ) {

    // List of plugins used by all theme demos.
    $theme_plugins = [
        [ 
            'name'     =>   'Contact Form 7', 
            'slug'     =>   'contact-form-7',
            'required' =>   true,     
        ],
        [ 
            'name'     =>   'Yoast SEO',
            'slug'     =>   'wordpress-seo',
            'required' =>   true,
        ],
        [ 
            'name'     =>   'Unique Headers',
            'slug'     =>   'unique-headers',
            'required' =>   true,
        ],
        [
            'name'     => 'WP Go Maps (formerly WP Google Maps)',
            'slug'     => 'wp-google-maps',
            'required' => true,
        ]
    ];

    // Check if user is on the theme recommeneded plugins step and a demo was selected.
    if (isset( $_GET['step'] ) && $_GET['step'] === 'import' &&  isset( $_GET['import'] )) {
        if ( $_GET['import'] === '0' ) {
            
            $theme_plugins[] = [
                'name'     =>   'WooCommerce',
                'slug'     =>   'woocommerce',
                'required' =>   true,
            ]; 
        }
    }

    if (isset( $_GET['step'] ) && $_GET['step'] === 'import' &&  isset( $_GET['import'] )) {
        if ( $_GET['import'] === '1' || $_GET['import'] === '2' || $_GET['import'] === '3' || $_GET['import'] === '4' || $_GET['import'] === '5' ) {
            
            $theme_plugins[] = [
                'name'     =>   'Elementor',
                'slug'     =>   'elementor',
                'required' =>   true,
            ];

            $theme_plugins[] = [
                'name'     => 'ElementsKit Lite',
                'slug'     => 'elementskit-lite',
                'required' => true,
            ]; 
        }
    }

    return array_merge( $plugins, $theme_plugins );

}
add_filter( 'ocdi/register_plugins', 'wpkites_plus_starter_sites_register_plugins' );

/**
* Remove branding
*/
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );